# Notifications

## Schedule

The src/notification.ts file regularly (once every hour, configured within
src/main.ts) reads a file named `notifications.json`, if it exists. All
notifications -- identified by their `id` property -- are sent to all
connected clients and to new connections as well. When removed from the file --
after re-scan of the file --, the notification stops being sent to new
connections.

Notifications are not checked for any changes if any prior notification of the
same `id` is still cached.

You may also send the `USR2` signal to the node process to trigger an immediate
scan of the file.

## File format

The `notifications.json` file may contain a single notification or an Array of
notifications.

Every notification must fulfill the following interface:

```ts
interface Notification {
  /*
   * The ID of the notification, as explained above.
   */
  id: string;
  /*
   * Optional title of the notification.
   */
  title?: string;
  /*
   * The body message of the notification.
   */
  message: string;

  /*
   * The notification type. Defaults to "info".
   */
  type?: "success" | "info" | "warning" | "error";
  /*
   * The authoring date to show in the notification. If not provided, the
   * current date will be used. Set to null to not indicate any date. Any number
   * or string value is passed to the Date constructor.
   */
  date?: number | string | null;
  /*
   * Duration in milliseconds for the notification message to show. By default
   * it is shown until actively closed by the client. If set to true, a default
   * duration will be used depending on the notification type.
   */
  timeout?: number | true;
}
```

### Minimal example

```json
{
  "id": "welcome-message",
  "message": "Welcome on raining.cards!"
}
```

### Multiple notifications

```json
[
  {
    "id": "welcome-message",
    "message": "Welcome on raining.cards!",
    "date": null,
    "timeout": true
  },
  {
    "id": "update-scheduled-01",
    "title": "Maintenance scheduled",
    "type": "warning",
    "message": "Between 3:00 and 3:15 PM a downtime of a few seconds to minutes is expected. Unfortunately this will result in cancel of ongoing games."
  }
]
```
