import { computed, WatchSource, ComputedRef } from "vue";

export function useMergedType<
  Type,
  T extends { type: Type; customMeta?: Partial<Type> }
>(src: WatchSource<T | undefined>): ComputedRef<Type | undefined> {
  return computed(() => {
    const it = typeof src === "function" ? src() : src.value;
    if (it === undefined) {
      return undefined;
    }
    const { type, customMeta } = it;
    if (customMeta === undefined) {
      return type;
    }
    const result = { ...type };
    for (const key in customMeta) {
      const value = customMeta[key];
      if (value !== undefined) {
        result[key] = value!;
      }
    }
    return result;
  });
}
