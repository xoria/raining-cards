<template>
  <div
    class="zone"
    :id="`zone:${zone.id}`"
    ref="zoneRef"
    :style="mergedZoneType.uiMeta?.styles"
    :class="[
      mergedZoneType.uiMeta?.classNames,
      { self: isSelfRelated, foreign: isForeignRelated },
    ]"
  >
    <div
      class="title"
      v-if="mergedZoneType.label || mergedZoneType.relatedTo?.length"
    >
      <span class="label" :title="mergedZoneType.description">{{
        mergedZoneType.label
      }}</span>
      <template v-if="mergedZoneType.relatedTo?.length">
        <template v-if="mergedZoneType.label">: </template>
        <span
          class="related-player"
          v-for="player in mergedZoneType.relatedTo"
          :key="player.id"
          >{{ player.name }}</span
        >
      </template>
    </div>
    <div class="chunks" :key="chunksRev">
      <ZoneChunk
        class="chunk"
        v-for="chunk in chunks"
        :key="chunk"
        :game-state="gameState"
        :zone="zone"
        :chunk="chunk"
        :materialActions="materialActions"
      />
    </div>
    <div
      class="zones"
      v-if="zone.children?.length > 0"
      :style="{
        'justify-content':
          zone.children.length > 1 ? 'space-between' : 'center',
      }"
    >
      <RecursiveZone
        v-for="it in zone.children"
        :key="it.id"
        :lobby-state="lobbyState"
        :game-state="gameState"
        :zone="it"
      />
    </div>
    <div
      class="actions--global"
      v-if="zoneActions && zoneActions[ElementType.GLOBAL].length"
    >
      <button
        class="action"
        type="button"
        v-for="action in zoneActions[ElementType.GLOBAL]"
        :key="action"
        v-tooltip:bottom.html="action.description"
        @click="gameState.perform(action, zone)"
      >
        {{ action.name }}
      </button>
    </div>
    <div class="loose-chunks">
      <MoveAnimation
        v-for="it in looseChunks"
        :key="it"
        :start="it.start"
        :end="it.end"
        @finish="removeLooseChunk(it)"
      >
        <ZoneChunk class="loose-chunk" :zone="zone" :chunk="it.chunk" />
      </MoveAnimation>
    </div>
  </div>
</template>

<script lang="ts">
import {
  computed,
  defineComponent,
  PropType,
  ref,
  watchEffect,
  toRaw,
} from "vue";
import ZoneChunk from "@/modules/game/ZoneChunk.vue";
import {
  UIActionSchemaTargetLookup,
  UIGameState,
  UILobbyState,
  UIMaterialChunk,
  UIZone,
  UIZoneEvtBeforeDetach,
  UIZoneEvtCreated,
  UIZoneEvtDestroy,
} from "@raining.cards/client";
import { ElementType, GameIds } from "@raining.cards/common";
import { mapValues, withoutUnique } from "@raining.cards/util";
import { useAllowedActions } from "@/modules/game/game.state";
import MoveAnimation from "@/modules/game/MoveAnimation.vue";
import { animationFrame } from "@/util/dom";
import { useMergedType } from "@/util/use-merged-type";

type OffsetPosition = {
  left: number;
  top: number;
};

interface LooseChunk {
  chunk: UIMaterialChunk;
  start?: Partial<CSSStyleDeclaration>;
  end?: Partial<CSSStyleDeclaration>;
}

export default defineComponent({
  name: "RecursiveZone",
  components: { MoveAnimation, ZoneChunk },
  props: {
    lobbyState: { type: Object as PropType<UILobbyState>, required: true },
    gameState: { type: Object as PropType<UIGameState>, required: true },
    zone: { type: Object as PropType<UIZone>, required: true },
  },
  setup(props) {
    const zoneRef = ref<HTMLDivElement>();
    const mergedZoneType = useMergedType(() => props.zone);

    const actions = useAllowedActions(props.gameState);
    const zoneActions = computed(
      (): Record<
        ElementType,
        UIActionSchemaTargetLookup<GameIds, ElementType.ZONE>[]
      > =>
        mapValues(actions.value[ElementType.ZONE], (list) =>
          list.filter((action) => {
            return action.check() && action.checkOrigin(toRaw(props.zone));
          })
        )
    );
    const materialActions = computed(
      (): Record<
        ElementType,
        UIActionSchemaTargetLookup<GameIds, ElementType.MATERIAL>[]
      > =>
        mapValues(actions.value[ElementType.MATERIAL], (list) =>
          list.filter((action) => action.check())
        )
    );

    const chunks = ref(props.zone.materialChunks);
    // since chunks are subject to in-place updates, this is a lightweight
    // alternative to deep clones
    const chunksRev = ref(0);
    watchEffect(() => {
      return props.zone.on("updated", () => {
        chunks.value = props.zone.materialChunks;
        chunksRev.value++;
      });
    });

    const looseChunkResolve = new WeakMap<LooseChunk, () => void>();
    const looseChunks = ref<LooseChunk[]>([]);
    props.zone.on("beforeDetach", async (evt: UIZoneEvtBeforeDetach) => {
      const fromElement = zoneRef.value!.querySelector(
        `.chunks > .chunk[data-offset="${evt.from.chunk.offset}"]`
      )! as HTMLElement;
      const startPosition = offsetPosition(fromElement);
      await animationFrame();
      const toElement = document
        .getElementById(`zone:${evt.toZone.zone.id}`)!
        .querySelector(
          `.chunks > .chunk[data-offset="${evt.toChunk.chunk.offset}"]`
        )! as HTMLElement;
      const endPosition = offsetPosition(toElement);
      const materialOrChunk = evt.delta.newMaterialView ?? evt.from.chunk;
      const looseChunk: LooseChunk = {
        chunk: { ...materialOrChunk, offset: 0, count: 1 },
        start: posToStyle(startPosition),
        end: posToStyle(endPosition),
      };
      looseChunks.value = looseChunks.value.concat([looseChunk]);
      await new Promise<void>((resolve) =>
        looseChunkResolve.set(looseChunk, resolve)
      );
    });

    props.zone.on("created", async (evt: UIZoneEvtCreated) => {
      await animationFrame();
      const toElement = zoneRef.value!.querySelector(
        `.chunks > .chunk[data-offset="${evt.position.chunk.offset}"]`
      )! as HTMLElement;
      const toPosition = offsetPosition(toElement);
      const material = evt.delta.materialView;
      const looseChunk: LooseChunk = {
        chunk: { offset: 0, count: 1, ...material },
        start: {
          ...posToStyle({
            left: toPosition.left - 20,
            top: toPosition.top - 35,
          }),
          opacity: "0",
          transform: "scale(1.5)",
        },
        end: { ...posToStyle(toPosition), opacity: "1", transform: "scale(1)" },
      };
      looseChunks.value = looseChunks.value.concat([looseChunk]);
      await new Promise<void>((resolve) =>
        looseChunkResolve.set(looseChunk, resolve)
      );
    });

    props.zone.on("beforeDestroy", async (evt: UIZoneEvtDestroy) => {
      const fromElement = zoneRef.value!.querySelector(
        `.chunks > .chunk[data-offset="${evt.position.chunk.offset}"]`
      )! as HTMLElement;
      const fromPosition = offsetPosition(fromElement);
      const chunk = evt.position.chunk;
      const looseChunk: LooseChunk = {
        chunk: { ...chunk, offset: 0, count: 1 },
        start: {
          ...posToStyle(fromPosition),
          opacity: "1",
          transform: "scale(1)",
        },
        end: {
          ...posToStyle({
            left: fromPosition.left + 20,
            top: fromPosition.top + 35,
          }),
          opacity: "0",
          transform: "scale(0.5)",
        },
      };
      await animationFrame();
      looseChunks.value = looseChunks.value.concat([looseChunk]);
      await new Promise<void>((resolve) =>
        looseChunkResolve.set(looseChunk, resolve)
      );
    });

    // todo move to core
    const isSelfRelated = computed(() =>
      mergedZoneType.value!.relatedTo?.some(
        (it) => it.id === props.lobbyState.self.id
      )
    );
    // todo move to core
    const isForeignRelated = computed(() =>
      mergedZoneType.value!.relatedTo?.some(
        (it) => it.id !== props.lobbyState.self.id
      )
    );

    return {
      ElementType,
      mergedZoneType,
      zoneRef,
      zoneActions,
      materialActions,
      chunks,
      chunksRev,
      looseChunks,
      isSelfRelated,
      isForeignRelated,
      removeLooseChunk,
    };

    function removeLooseChunk(item: LooseChunk) {
      looseChunks.value = withoutUnique(looseChunks.value, item);
      looseChunkResolve.get(toRaw(item))?.();
    }
  },
});

function offsetPosition(element: HTMLElement): OffsetPosition {
  return {
    left: element.offsetLeft,
    top: element.offsetTop,
  };
}

function posToStyle({
  left,
  top,
}: OffsetPosition): Pick<CSSStyleDeclaration, "left" | "top"> {
  return {
    left: `${left}px`,
    top: `${top}px`,
  };
}
</script>

<style lang="scss" scoped>
@import "!/styles/variables";
@import "!/styles/mixins";

.zone {
  margin: $spacing-xs $spacing-sm;
  font-size: 0.85rem;
  min-height: 48px;
  padding: $spacing-xs;

  &:not(.zone *) {
    padding: $spacing-sm $spacing-md;
    background-color: $white;
  }

  &.foreign {
    background-color: $foreign-900;
  }
  &.self {
    background-color: $primary-900;
  }
}

.zones {
  display: flex;
  flex-wrap: wrap;
}

.title {
  text-align: center;
  font-weight: 700;
  font-size: 0.8em;
  margin-bottom: $spacing-sm;

  .related-player {
    color: #444444;
  }
}

.chunks {
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  margin: -$spacing-sm / 2;

  > .chunk {
    flex-grow: 0;
    margin: $spacing-sm / 2;
  }
}

.loose-chunks {
  display: contents;
}

.actions--global {
  display: flex;
  justify-content: center;
  margin: (-$spacing-sm / 2) (-$spacing-md / 2);
  margin-top: (-$spacing-sm / 2) + $spacing-sm;

  > * {
    margin: ($spacing-sm / 2) ($spacing-md / 2);
  }
}

button.action {
  @include action-button;
  font-size: 0.75rem;
  padding: $spacing-xs $spacing-sm;
}
</style>
