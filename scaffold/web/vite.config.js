import vue from "@vitejs/plugin-vue";
import * as path from "path";

const STYLE_DEPENDENCIES = [
  "tailwindcss",
  "tippy.js",
  "vue-toastification",
].map((it) => [it, path.dirname(require.resolve(`${it}/package.json`))]);

/**
 * https://vitejs.dev/config/
 * @type {import('vite').UserConfig}
 */
export default {
  plugins: [vue()],
  resolve: {
    alias: {
      "@shared": path.join(__dirname, "../shared/src"),
      "@": path.join(__dirname, "src"),
      // well... IntelliJ does not like '@' alias within SCSS. A character that
      // IntelliJ seems to like is '!'. :-/
      "!": path.join(__dirname, "src"),
      // and... IntelliJ does not like to resolve relative imports from within
      // node_modules, so create '~'-prefixed aliases
      ...Object.fromEntries(
        STYLE_DEPENDENCIES.map(([it, dir]) => [`~${it}`, dir])
      ),
    },
  },
};
