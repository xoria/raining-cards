[Base Repository](https://gitlab.com/xoria/raining-cards/)

# A Raining Cards' Companion: Who Knows Whom

- Participants: 2+

A simple companion app to party questioning sessions. Any player can choose
yes/no/other for any other players and themselves. When all players are ready,
the answers are revealed (who chose what for whom).
