import {
  defineAction,
  IdFactory,
  Lobby,
  Transaction,
} from "@raining.cards/server";
import { ElementType, GameIds } from "@raining.cards/common";
import { Zones } from "@/zones";
import { State, StateValue } from "@/state";

export function useClearAction(
  idf: IdFactory<GameIds["actionSchema"]>,
  lobby: Lobby,
  transaction: Transaction,
  zones: Zones,
  state: State
) {
  return defineAction(
    {
      id: idf("clear"),
      name: "Clear All",
      origin: { type: ElementType.GLOBAL },
      target: { type: ElementType.GLOBAL },
    },
    () => {
      for (const zone of zones.board.asList) {
        transaction.destroyAll(zone);
      }
      state.value = StateValue.ANSWERS;
    }
  );
}
