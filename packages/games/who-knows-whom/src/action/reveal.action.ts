import { defineAction, IdFactory, Transaction } from "@raining.cards/server";
import { ElementType, GameIds } from "@raining.cards/common";
import { State, StateValue } from "@/state";
import { Zones } from "@/zones";

export function useRevealAction(
  idf: IdFactory<GameIds["actionSchema"]>,
  transaction: Transaction,
  zones: Zones,
  state: State
) {
  return defineAction(
    {
      id: idf("reveal"),
      name: "Reveal All",
      origin: { type: ElementType.GLOBAL },
      target: { type: ElementType.GLOBAL },
    },
    () => {
      for (const zone of zones.board.asList) {
        // todo provide more efficient zone reveal functions within Transaction
        for (const material of zone.materials) {
          transaction.revealGlobal({ zone, material });
        }
      }
      state.value = StateValue.REVEALED;
    }
  );
}
