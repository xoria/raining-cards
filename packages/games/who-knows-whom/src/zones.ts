import {
  defineBoard,
  defineZone,
  defineZoneSingleton,
  idFactory,
  Player,
  Zone,
} from "@raining.cards/server";
import { withLookup } from "@raining.cards/util";
import { GameIds } from "@raining.cards/common";

export type Zones = ReturnType<typeof useZones>;

export function useZones(players: Player[]) {
  const idfType = idFactory<GameIds["zoneType"]>("zone-type");
  const idf = idFactory<GameIds["zone"]>("zone");

  const wrapperDef = defineZoneSingleton(idf("domain"));
  const playerDef = defineZone(idfType("player"), {
    uiMeta: {
      styles: {
        // dirty prevent height change on first vote
        minHeight: "116px",
        minWidth: "360px",
        // dirty fix action buttons to bottom of zones
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
      },
    },
  });

  const playerZones = withLookup(
    players.map((player) => {
      const zone = playerDef.factory(idf(`player:${player.id}`), undefined, {
        relatedTo: [player],
      });
      return { id: player.id, zone };
    })
  );
  wrapperDef.instance.children = playerZones.asList.map((it) => it.zone);

  const playerByZone = new Map<Zone, Player>();
  for (const player of players) {
    playerByZone.set(playerZones.asLookup[player.id].zone, player);
  }

  return {
    types: [wrapperDef, playerDef],
    player: playerZones,
    board: defineBoard([wrapperDef.instance]),
    playerByZone,
  };
}
