import { defineGameMeta, Game } from "@raining.cards/server";
import { Zones } from "@/zones";
import { Actions } from "@/action/actions";
import { Materials } from "@/materials";

export type GameMeta = Game["meta"];

export function useGameMeta(
  materials: Materials,
  zones: Zones,
  actions: Actions
): GameMeta {
  return defineGameMeta(
    {
      materialFaceTypes: materials.facesList,
      zoneTypes: zones.types,
    },
    (client, isPlayer) => {
      if (!isPlayer) {
        return {};
      }
      const playerActions = actions.of(client);
      return {
        actionSchemas: playerActions.asList,
        actionSchemaGroups: playerActions.groups,
      };
    }
  );
}
