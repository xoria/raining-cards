import {
  defineMaterial,
  defineMaterialFace,
  defineMaterialFaceSingleton,
  idFactory,
} from "@raining.cards/server";
import { GameIds } from "@raining.cards/common";

export const ANSWERS = ["yes", "no", "other"];

export type Materials = ReturnType<typeof useMaterials>;

export function useMaterials() {
  const idfFaces = idFactory<GameIds["materialFaceType"]>("material-face-type");
  const idf = idFactory<GameIds["material"]>("material");

  const backdropForeignFace = defineMaterialFaceSingleton(
    idfFaces("backdrop:foreign"),
    { uiMeta: { styles: { borderRadius: "12px", backgroundColor: "white" } } }
  );
  const answersForeignFaces = Object.fromEntries(
    ANSWERS.map((label) => [
      label,
      defineMaterialFace(idfFaces(`answer:foreign:${label}`), {
        label,
        uiMeta: { styles: { borderRadius: "12px", backgroundColor: "white" } },
      }),
    ])
  );
  const answersForeign = Object.fromEntries(
    ANSWERS.map((label) => [
      label,
      defineMaterial(idf(`answer:foreign:${label}`), [
        backdropForeignFace,
        answersForeignFaces[label],
      ]),
    ])
  );

  const backdropSelfFace = defineMaterialFaceSingleton(
    idfFaces("backdrop:self"),
    { uiMeta: { styles: { borderRadius: "12px", backgroundColor: "orange" } } }
  );
  const answersSelfFaces = Object.fromEntries(
    ANSWERS.map((label) => [
      label,
      defineMaterialFace(idfFaces(`answer:self:${label}`), {
        label,
        uiMeta: {
          styles: {
            borderRadius: "12px",
            backgroundColor: "orange",
            fontWeight: "600",
          },
        },
      }),
    ])
  );
  const answersSelf = Object.fromEntries(
    ANSWERS.map((label) => [
      label,
      defineMaterial(idf(`answer:self:${label}`), [
        backdropSelfFace,
        answersSelfFaces[label],
      ]),
    ])
  );

  return {
    facesList: [
      backdropForeignFace,
      backdropSelfFace,
      ...Object.values(answersForeignFaces),
      ...Object.values(answersSelfFaces),
    ],
    asList: [...Object.values(answersForeign), ...Object.values(answersSelf)],

    answersForeign,
    answersSelf,
  };
}
