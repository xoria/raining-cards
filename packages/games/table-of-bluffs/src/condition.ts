import { ToBGameIds } from "@/ToB.types";
import { PlayerZonesMap, ToBZones } from "@/zone/zones";
import { ActionCondition, matchCondition, Player } from "@raining.cards/server";
import { matchAll, matchSome } from "@raining.cards/util";
import {
  ToBMaterialFaceType,
  ToBMaterialFaceTypeGroup,
} from "@/material/face-types";
import { ToBZoneType } from "@/zone/zone-types";

export enum ToBActionCondition {
  CREDITS_MIN_1 = "credits:1+",
  CREDITS_MIN_2 = "credits:2+",
  CREDITS_MIN_3 = "credits:3+",
  CREDITS_MIN_7 = "credits:7+",
  CREDITS_MAX_9 = "credits:0-9",

  TREASURE_MIN_1 = "treasure:1+",

  EQUAL_TEAMS = "teams:equal",

  ALIVE_MIN_3 = "alive:3+",

  // ... enemy:${playerId}
  // ... alive:${playerId}
  // ... credits:1+:${playerId}
}

export function conditionsMetaMap(
  zones: ToBZones,
  { zonesSelf, byPlayer }: PlayerZonesMap,
  otherPlayers: Player[]
): ActionCondition<ToBGameIds>[] {
  const allPlayers = Array.from(byPlayer.keys());
  const teamZones = allPlayers.map(
    (player) => byPlayer.get(player)![ToBZoneType.PLAYER_TEAM].id
  );
  return [
    {
      id: ToBActionCondition.CREDITS_MIN_1,
      condition: matchCondition({
        zoneIds: [zonesSelf[ToBZoneType.PLAYER_CREDITS].id],
        materialFaceTypeIds: [ToBMaterialFaceType.CREDIT],
        count: { min: 1 },
      }),
    },
    {
      id: ToBActionCondition.CREDITS_MIN_2,
      condition: matchCondition({
        zoneIds: [zonesSelf[ToBZoneType.PLAYER_CREDITS].id],
        materialFaceTypeIds: [ToBMaterialFaceType.CREDIT],
        count: { min: 2 },
      }),
    },
    {
      id: ToBActionCondition.CREDITS_MIN_3,
      condition: matchCondition({
        zoneIds: [zonesSelf[ToBZoneType.PLAYER_CREDITS].id],
        materialFaceTypeIds: [ToBMaterialFaceType.CREDIT],
        count: { min: 3 },
      }),
    },
    {
      id: ToBActionCondition.CREDITS_MIN_7,
      condition: matchCondition({
        zoneIds: [zonesSelf[ToBZoneType.PLAYER_CREDITS].id],
        materialFaceTypeIds: [ToBMaterialFaceType.CREDIT],
        count: { min: 7 },
      }),
    },
    {
      id: ToBActionCondition.CREDITS_MAX_9,
      condition: matchCondition({
        zoneIds: [zonesSelf[ToBZoneType.PLAYER_CREDITS].id],
        materialFaceTypeIds: [ToBMaterialFaceType.CREDIT],
        count: { max: 9 },
      }),
    },
    {
      id: ToBActionCondition.TREASURE_MIN_1,
      condition: matchCondition({
        zoneIds: [zones.global.treasury.id],
        materialFaceTypeIds: [ToBMaterialFaceType.CREDIT],
        count: { min: 1 },
      }),
    },
    {
      id: ToBActionCondition.EQUAL_TEAMS,
      condition: matchSome([
        matchCondition({
          zoneIds: teamZones,
          materialFaceTypeIds: [ToBMaterialFaceType.TEAM_RED],
          count: 0,
        }),
        matchCondition({
          zoneIds: teamZones,
          materialFaceTypeIds: [ToBMaterialFaceType.TEAM_BLUE],
          count: 0,
        }),
      ]),
    },
    {
      id: ToBActionCondition.ALIVE_MIN_3,
      condition: matchCondition({
        zoneIds: teamZones,
        materialFaceTypeGroupIds: [ToBMaterialFaceTypeGroup.TEAM],
        count: { min: 3 },
      }),
    },
    ...otherPlayers.map(
      (player): ActionCondition<ToBGameIds> => {
        const zoneIds = [
          zonesSelf[ToBZoneType.PLAYER_TEAM].id,
          byPlayer.get(player)![ToBZoneType.PLAYER_TEAM].id,
        ];
        return {
          id: `enemy:${player.id}`,
          condition: matchAll([
            matchCondition({
              zoneIds,
              materialFaceTypeIds: [ToBMaterialFaceType.TEAM_RED],
              count: 1,
            }),
            matchCondition({
              zoneIds,
              materialFaceTypeIds: [ToBMaterialFaceType.TEAM_BLUE],
              count: 1,
            }),
          ]),
        };
      }
    ),
    ...otherPlayers.map(
      (player): ActionCondition<ToBGameIds> => ({
        id: `alive:${player.id}`,
        condition: matchCondition({
          zoneIds: [byPlayer.get(player)![ToBZoneType.PLAYER_ROLES].id],
          materialFaceTypeIds: [ToBMaterialFaceType.ROLE_BACKDROP],
          count: { min: 1 },
        }),
      })
    ),
    ...otherPlayers.map(
      (player): ActionCondition<ToBGameIds> => ({
        id: `credits:1+:${player.id}`,
        condition: matchCondition({
          zoneIds: [byPlayer.get(player)![ToBZoneType.PLAYER_CREDITS].id],
          materialFaceTypeIds: [ToBMaterialFaceType.CREDIT],
          count: { min: 1 },
        }),
      })
    ),
  ];
}
