import { ToBGame } from "@/ToB.game";
import { ToBActionCondition } from "@/condition";
import { ToBActionSchemaGroup, ToBActionSchema } from "@/action/action.types";
import { ToBGameIds } from "@/ToB.types";
import { PlayerZonesMap, ToBZones } from "@/zone/zones";
import {
  ActionSchema,
  ClientAction,
  ClientActorZone,
  last,
  Player,
  Zone,
} from "@raining.cards/server";
import { ElementType } from "@raining.cards/common";
import { matchAll, matchIt, matchSome, times } from "@raining.cards/util";
import { ToBZoneType } from "@/zone/zone-types";
import { doubtActionStackItem } from "@/action/doubt.action";
import { CREDIT_SYMBOL } from "@/material/face-types";

interface LatestSteal {
  credits: number;
  fromZone: Zone<ToBGameIds>;
  toZone: Zone<ToBGameIds>;
}

export const LATEST_STEAL = new WeakMap<ToBGame, LatestSteal>();

export function stealSchema(
  game: ToBGame,
  { byPlayer }: PlayerZonesMap,
  _: ToBZones,
  otherPlayers: Player[]
): ActionSchema<ToBGameIds>[] {
  return otherPlayers.map((player) => {
    const id = `${ToBActionSchema.STEAL}|${player.id}`;
    game.actionSchemaMapping[id] = ToBActionSchema.STEAL;
    return {
      id,
      name: "Steal",
      description: `Role: <em>Captain</em>; can be blocked by <em>Captain</em> and <em>Ambassador</em>.<br/>Steal ${CREDIT_SYMBOL}2 from some player.`,
      origin: {
        type: ElementType.ZONE,
        zone: [byPlayer.get(player)![ToBZoneType.PLAYER_CREDITS].id],
      },
      target: { type: ElementType.GLOBAL },
      condition: matchAll([
        matchIt(ToBActionCondition.CREDITS_MAX_9),
        matchSome([
          matchIt(ToBActionCondition.EQUAL_TEAMS),
          matchIt(`enemy:${player.id}`),
        ]),
        matchIt(`credits:1+:${player.id}`),
      ]),
    };
  });
}

export async function performSteal(
  game: ToBGame,
  action: ClientAction<ToBGameIds>
) {
  // const fromZone = (action.origin as ElementMaterials<ToBGameIds>).materials[0]
  //   .zone;
  // const toZone = (action.target as ElementZone<ToBGameIds>).zone;
  const fromZone = (action.origin as ClientActorZone<ToBGameIds>).zone;
  const toZone = game.zones.byPlayer.get(action.player)![
    ToBZoneType.PLAYER_CREDITS
  ];

  const fromPlayer = game.playerByZone.get(fromZone)!;
  const credits = Math.min(2, fromZone.materials.length);
  game.logs.add(
    `${action.player.name} steals ${CREDIT_SYMBOL}${credits} from ${fromPlayer.name} (captain)`
  );

  LATEST_STEAL.set(game, { credits, fromZone, toZone });
  forwardSteal(game);

  const actionStackItem = game.actionStack.push({
    executors: [
      () => {
        game.transaction.addPlayerAction(
          fromPlayer,
          ToBActionSchemaGroup.RECEIVE_STEAL
        );
      },
      doubtActionStackItem(game, {
        action,
        then: () => backwardSteal(game),
        catch: async (forfeitLife) => {
          await forfeitLife();
          await game.performTask();
        },
      }),
    ],
    condition: () => game.isAlive(fromPlayer),
  });
  await game.performTask();
  game.actionStack.popAssert(actionStackItem);
  game.turn.step();
}

export function backwardSteal(game: ToBGame) {
  const { credits, fromZone, toZone } = LATEST_STEAL.get(game)!;
  times(credits, () => game.transaction.move(last(toZone), last(fromZone)));
}

export function forwardSteal(game: ToBGame) {
  const { credits, fromZone, toZone } = LATEST_STEAL.get(game)!;
  times(credits, () => game.transaction.move(last(fromZone), last(toZone)));
}
