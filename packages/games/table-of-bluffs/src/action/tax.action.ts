import { ToBGame } from "@/ToB.game";
import { ToBActionCondition } from "@/condition";
import { creditMaterialFactory } from "@/material/material-factories";
import { ToBActionSchema } from "@/action/action.types";
import { ToBGameIds } from "@/ToB.types";
import {
  ActionSchema,
  ClientAction,
  ClientActorZone,
  last,
} from "@raining.cards/server";
import { ElementType } from "@raining.cards/common";
import { matchIt, times } from "@raining.cards/util";
import { PlayerZonesMap } from "@/zone/zones";
import { ToBZoneType } from "@/zone/zone-types";
import { doubtActionStackItem } from "@/action/doubt.action";
import { CREDIT_SYMBOL } from "@/material/face-types";

export function taxSchema(
  game: ToBGame,
  { zonesSelf }: PlayerZonesMap
): ActionSchema<ToBGameIds> {
  return {
    id: ToBActionSchema.TAX,
    name: "Tax",
    description: `Role: <em>Duke</em>; cannot be blocked.<br/>Income: ${CREDIT_SYMBOL}3.`,
    origin: {
      type: ElementType.ZONE,
      zone: [zonesSelf[ToBZoneType.PLAYER_CREDITS].id],
    },
    target: { type: ElementType.GLOBAL },
    condition: matchIt(ToBActionCondition.CREDITS_MAX_9),
  };
}

export async function performTax(
  game: ToBGame,
  action: ClientAction<ToBGameIds>
) {
  game.logs.add(`${action.player.name} collects ${CREDIT_SYMBOL}3 tax (duke)`);
  const origin = action.origin as ClientActorZone<ToBGameIds>;
  const position = last(origin.zone);
  times(3, () =>
    game.transaction.create(creditMaterialFactory(game), position)
  );
  game.actionStack.peekAssert().add(
    doubtActionStackItem(game, {
      action,
      then: () => backwardTax(game, action),
    })
  );
  game.turn.step();
}

function backwardTax(game: ToBGame, action: ClientAction<ToBGameIds>) {
  const origin = action.origin as ClientActorZone<ToBGameIds>;
  const position = last(origin.zone);
  times(3, () => game.transaction.destroy(position));
}
