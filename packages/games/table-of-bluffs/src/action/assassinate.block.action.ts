import { ToBGame } from "@/ToB.game";
import { getLives } from "@/zone/zones";
import { ToBActionSchemaGroup, ToBActionSchema } from "@/action/action.types";
import { ToBGameIds } from "@/ToB.types";
import { forcePlayerForfeitLife } from "@/action/forfeit-life.action";
import {
  ActionSchema,
  ClientAction,
  MaterialWithZone,
  Player,
  Zone,
} from "@raining.cards/server";
import { ElementType } from "@raining.cards/common";
import { groupBy, mapObject } from "@raining.cards/util";
import { doubtActionStackItem } from "@/action/doubt.action";
import { ToBZoneType } from "@/zone/zone-types";

export function assassinateBlockSchema(): ActionSchema<ToBGameIds> {
  return {
    id: ToBActionSchema.BLOCK_ASSASSINATE_CONTESSA,
    name: "Block assassination",
    description: `Role: <em>Contessa</em>.<br/>Blocks some assassination.`,
    origin: { type: ElementType.GLOBAL },
    target: { type: ElementType.GLOBAL },
  };
}

export async function performAssassinateBlock(
  game: ToBGame,
  action: ClientAction<ToBGameIds>
) {
  game.logs.add(`${action.player.name} blocks assassination (contessa)`);
  const assassin = game.currentPlayer;
  const actionStackItem = game.actionStack.push({
    executors: [
      () =>
        game.transaction.addPlayerAction(
          assassin,
          ToBActionSchemaGroup.CONTINUE
        ),
      doubtActionStackItem(game, {
        action,
        then: async () => {
          const zone = game.zones.byPlayer.get(action.player)![
            ToBZoneType.PLAYER_ROLES
          ];
          const lives = getLives(zone);
          if (lives.length <= 2) {
            // invalid doubt results in remaining lives lost, so do it
            // automatically
            return loseRemainingLives(game, action.player, zone, lives);
          }
          await forcePlayerForfeitLife(game, action.player);
        },
      }),
    ],
  });
  await game.performTask();
  game.actionStack.popAssert(actionStackItem);
}

function loseRemainingLives(
  game: ToBGame,
  player: Player,
  zone: Zone<ToBGameIds> = game.zones.byPlayer.get(player)![
    ToBZoneType.PLAYER_ROLES
  ],
  lives: MaterialWithZone<ToBGameIds>[] = getLives(zone)
) {
  for (const life of lives) {
    game.transaction.revealGlobal(life);
  }
  const roles = mapObject(
    groupBy(lives, (it) => it.material.faces[1].typeId),
    (materials, type) => {
      const count = materials.length;
      return count === 1 ? type : `${count}x ${type}`;
    }
  );
  game.logs.add(`${player.name} forfeits ${roles.join(", ")} and is defeated`);
  game.transaction.playerDied(player);
}
