import { ToBGame } from "@/ToB.game";
import { ToBActionCondition } from "@/condition";
import { ToBActionSchemaGroup, ToBActionSchema } from "@/action/action.types";
import { ToBGameIds } from "@/ToB.types";
import { PlayerZonesMap, ToBZones } from "@/zone/zones";
import {
  ActionSchema,
  ClientAction,
  ClientActorZone,
  last,
  Player,
} from "@raining.cards/server";
import { ElementType } from "@raining.cards/common";
import { matchAll, matchIt, matchSome, times } from "@raining.cards/util";
import { ToBZoneType } from "@/zone/zone-types";
import { doubtActionStackItem } from "@/action/doubt.action";
import { CREDIT_SYMBOL } from "@/material/face-types";

export function assassinateSchema(
  game: ToBGame,
  { byPlayer }: PlayerZonesMap,
  _: ToBZones,
  otherPlayers: Player[]
): ActionSchema<ToBGameIds>[] {
  return otherPlayers.map((player) => {
    const id = `${ToBActionSchema.ASSASSINATE}|${player.id}`;
    game.actionSchemaMapping[id] = ToBActionSchema.ASSASSINATE;
    return {
      id,
      name: "Assassinate",
      description: `Role: <em>Assassin</em>; can be blocked by <em>Contessa</em>.<br/>Costs: ${CREDIT_SYMBOL}3. Force some player to forfeit a life.`,
      origin: {
        type: ElementType.ZONE,
        zone: [byPlayer.get(player)![ToBZoneType.PLAYER_ROLES].id],
      },
      target: { type: ElementType.GLOBAL },
      condition: matchAll([
        matchIt(ToBActionCondition.CREDITS_MAX_9),
        matchIt(ToBActionCondition.CREDITS_MIN_3),
        matchIt(`alive:${player.id}`),
        matchSome([
          matchIt(ToBActionCondition.EQUAL_TEAMS),
          matchIt(`enemy:${player.id}`),
        ]),
      ]),
    };
  });
}

export const LATEST_ASSASSINATE = new WeakMap<ToBGame, Player>();

export async function performAssassinate(
  game: ToBGame,
  action: ClientAction<ToBGameIds>
) {
  const targetPlayer = game.playerByZone.get(
    (action.origin as ClientActorZone<ToBGameIds>).zone
  )!;
  LATEST_ASSASSINATE.set(game, targetPlayer);
  game.logs.add(
    `${action.player.name} assassinates ${targetPlayer.name} for ${CREDIT_SYMBOL}3 (assassin)`
  );
  const position = last(
    game.zones.byPlayer.get(action.player)![ToBZoneType.PLAYER_CREDITS]
  );
  times(3, () => game.transaction.destroy(position));
  const actionStackItem = game.actionStack.push({
    executors: [
      () => {
        game.transaction.addPlayerAction(
          targetPlayer,
          ToBActionSchemaGroup.RECEIVE_ASSASSINATE
        );
      },
      doubtActionStackItem(game, {
        action,
        catch: async () => await game.performTask(),
      }),
    ],
    condition: () => game.isAlive(targetPlayer),
  });
  await game.performTask();
  game.actionStack.popAssert(actionStackItem);
  game.turn.step();
}
