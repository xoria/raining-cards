import { ToBGame } from "@/ToB.game";
import { ToBActionCondition } from "@/condition";
import { ToBZoneType } from "@/zone/zone-types";
import { ToBActionSchema } from "@/action/action.types";
import { ToBGameIds } from "@/ToB.types";
import {
  ActionSchema,
  ClientAction,
  ClientActorMaterial,
  last,
} from "@raining.cards/server";
import { ElementType } from "@raining.cards/common";
import { matchAll, matchIt } from "@raining.cards/util";
import { PlayerZonesMap } from "@/zone/zones";
import { CREDIT_SYMBOL } from "@/material/face-types";

export function changeTeamSelfSchema(
  game: ToBGame,
  { zonesSelf }: PlayerZonesMap
): ActionSchema<ToBGameIds> {
  return {
    id: ToBActionSchema.CHANGE_TEAM_SELF,
    name: `Flip (${CREDIT_SYMBOL}1)`,
    description: `Costs: ${CREDIT_SYMBOL}1 (put into treasury). Flip your own team.`,
    origin: {
      type: ElementType.MATERIAL,
      position: [{ zone: zonesSelf[ToBZoneType.PLAYER_TEAM].id }],
    },
    target: { type: ElementType.GLOBAL },
    condition: matchAll([
      matchIt(ToBActionCondition.CREDITS_MAX_9),
      matchIt(ToBActionCondition.CREDITS_MIN_1),
      matchIt(ToBActionCondition.ALIVE_MIN_3),
    ]),
  };
}

export async function performChangeTeamSelf(
  game: ToBGame,
  action: ClientAction<ToBGameIds>
) {
  game.logs.add(
    `${action.player.name} flips their own team for ${CREDIT_SYMBOL}1`
  );
  game.transaction.flipGlobal(action.origin as ClientActorMaterial<ToBGameIds>);
  const creditsPosition = last(
    game.zones.byPlayer.get(action.player)![ToBZoneType.PLAYER_CREDITS]
  );
  game.transaction.move(
    creditsPosition,
    last(game.zones.global[ToBZoneType.TREASURY])
  );
  game.turn.step();
}
