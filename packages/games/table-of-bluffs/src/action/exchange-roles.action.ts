import { ToBGame } from "@/ToB.game";
import { ToBActionCondition } from "@/condition";
import { ToBZoneType } from "@/zone/zone-types";
import { ToBActionSchema, ToBActionSchemaGroup } from "@/action/action.types";
import { ToBGameIds } from "@/ToB.types";
import { getLives, PlayerZonesMap, ToBZones } from "@/zone/zones";
import {
  accessOrThrow,
  ActionSchema,
  ClientAction,
  ClientActorMaterial,
  first,
  last,
} from "@raining.cards/server";
import { ElementType } from "@raining.cards/common";
import { matchIt, times } from "@raining.cards/util";
import { ToBMaterialFaceType } from "@/material/face-types";
import { doubtActionStackItem } from "@/action/doubt.action";

export function exchangeRolesSchema(
  game: ToBGame,
  { zonesSelf }: PlayerZonesMap,
  zones: ToBZones
): ActionSchema<ToBGameIds>[] {
  return [
    {
      id: ToBActionSchema.EXCHANGE_ROLES_0,
      name: "Exchange roles",
      description: `Role: <em>Ambassador</em>; cannot be blocked.<br/>Draw 2 roles, then discard 2 roles.`,
      origin: {
        type: ElementType.ZONE,
        zone: [zones.global[ToBZoneType.DECK].id],
      },
      target: { type: ElementType.GLOBAL },
      condition: matchIt(ToBActionCondition.CREDITS_MAX_9),
    },
    {
      id: ToBActionSchema.EXCHANGE_ROLES_1,
      name: "Discard",
      origin: {
        type: ElementType.MATERIAL,
        materialView: { public: [ToBMaterialFaceType.ROLE_BACKDROP] },
        position: [{ zone: zonesSelf[ToBZoneType.PLAYER_ROLES].id }],
      },
      target: { type: ElementType.GLOBAL },
    },
  ];
}

export async function performExchangeRoles0(
  game: ToBGame,
  action: ClientAction<ToBGameIds>
) {
  game.logs.add(`${action.player.name} draws 2 roles (ambassador)`);
  const playerZones = game.zones.byPlayer.get(action.player)!;
  const rolesPosition = last(playerZones[ToBZoneType.PLAYER_ROLES]);
  times(2, () => {
    game.transaction.move(
      last(game.zones.global[ToBZoneType.DECK]),
      rolesPosition
    );
  });
  const actionStackItem = game.actionStack.push({
    executors: [
      () => {
        game.transaction.addPlayerAction(
          action.player,
          ToBActionSchemaGroup.EXCHANGE_ROLES_1
        );
      },
      doubtActionStackItem(game, {
        action,
        then: () => backwardExchangeRoles0(game, action),
        catch: async (forfeitLife) => {
          await forfeitLife();
          await game.performTask();
        },
      }),
    ],
    condition: () => game.turn.size > 1,
  });
  await game.performTask();
  game.actionStack.popAssert(actionStackItem);
  game.turn.step();
}

function backwardExchangeRoles0(
  game: ToBGame,
  { player }: ClientAction<ToBGameIds>
) {
  const playerZones = game.zones.byPlayer.get(player)!;
  const rolesPosition = last(playerZones[ToBZoneType.PLAYER_ROLES]);
  const deckPosition = last(game.zones.global[ToBZoneType.DECK]);
  times(2, () => game.transaction.move(rolesPosition, deckPosition));
  game.transaction.shuffle(game.zones.global[ToBZoneType.DECK]);
}

export async function performExchangeRoles1(
  game: ToBGame,
  action: ClientAction<ToBGameIds>
) {
  const material = action.origin as ClientActorMaterial<ToBGameIds>;
  const deck = game.zones.global[ToBZoneType.DECK];

  const { material: _material } = accessOrThrow(material);
  const lives = getLives(material.zone).map((life) =>
    life.material === _material ? { material: _material, zone: deck } : life
  );
  game.transaction.move(material, last(deck));
  game.transaction.flush();

  if (material.zone.materials.length > 2) {
    const actionStackItem = game.actionStack.push([
      () => {
        game.transaction.addPlayerAction(
          action.player,
          ToBActionSchemaGroup.EXCHANGE_ROLES_1
        );
      },
      doubtActionStackItem(game, {
        action,
        playerLives: lives,
        then: () => backwardExchangeRoles1(game, action),
        catch: async (forfeitLife) => {
          await forfeitLife();
          await game.performTask();
        },
      }),
    ]);
    await game.performTask();
    game.transaction.shuffle(deck);
    game.actionStack.popAssert(actionStackItem);
  }
}

function backwardExchangeRoles1(
  game: ToBGame,
  action: ClientAction<ToBGameIds>
) {
  const playerZones = game.zones.byPlayer.get(action.player)!;
  const deckPosition = last(game.zones.global[ToBZoneType.DECK]);
  const rolesPosition = first(playerZones[ToBZoneType.PLAYER_ROLES]);
  game.transaction.move(deckPosition, rolesPosition);
  backwardExchangeRoles0(game, action);
}
