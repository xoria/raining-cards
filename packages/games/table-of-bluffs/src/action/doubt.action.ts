import { getLives } from "@/zone/zones";
import { ToBActionSchema, ToBActionSchemaGroup } from "@/action/action.types";
import { forcePlayerForfeitLife } from "@/action/forfeit-life.action";
import { ToBGame } from "@/ToB.game";
import { ToBZoneType } from "@/zone/zone-types";
import { ToBGameIds } from "@/ToB.types";
import { assert } from "ts-essentials";
import {
  ActionSchema,
  ClientAction,
  Executor,
  last,
  MaterialWithZone,
  Player,
} from "@raining.cards/server";
import { ElementType } from "@raining.cards/common";
import { once, pickOne, times } from "@raining.cards/util";
import { ToBMaterialFaceType } from "@/material/face-types";

export interface OnDoubtData {
  action: ClientAction<ToBGameIds>;
  playerLives?: MaterialWithZone<ToBGameIds>[];
  then?: OnDoubtHandler;
  catch?: OnDoubtHandler;
}

export type OnDoubtHandler = (
  forfeitLife: () => void,
  doubtAction: ClientAction<ToBGameIds>
) => Promise<void> | void;

export const LATEST_DOUBT_DATA = new WeakMap<ToBGame, OnDoubtData>();

export function doubtSchema(): ActionSchema<ToBGameIds> {
  return {
    id: ToBActionSchema.DOUBT,
    name: "Doubt",
    description: `Doubt the actor of the latest action. If they did bluff, they must forfeit a role; otherwise you must forfeit a role.`,
    origin: { type: ElementType.GLOBAL },
    target: { type: ElementType.GLOBAL },
  };
}

interface VerifyCondition {
  materialType: ToBMaterialFaceType;
  owns: boolean;
}

const VERIFY_CONDITION: Partial<Record<ToBActionSchema, VerifyCondition>> = {
  [ToBActionSchema.ASSASSINATE]: {
    materialType: ToBMaterialFaceType.ASSASSIN,
    owns: true,
  },
  [ToBActionSchema.BLOCK_ASSASSINATE_CONTESSA]: {
    materialType: ToBMaterialFaceType.CONTESSA,
    owns: true,
  },
  [ToBActionSchema.BLOCK_FOREIGN_AID_DUKE]: {
    materialType: ToBMaterialFaceType.DUKE,
    owns: true,
  },
  [ToBActionSchema.BLOCK_STEAL_AMBASSADOR]: {
    materialType: ToBMaterialFaceType.AMBASSADOR,
    owns: true,
  },
  [ToBActionSchema.BLOCK_STEAL_CAPTAIN]: {
    materialType: ToBMaterialFaceType.CAPTAIN,
    owns: true,
  },
  [ToBActionSchema.STEAL]: {
    materialType: ToBMaterialFaceType.CAPTAIN,
    owns: true,
  },
  [ToBActionSchema.TAX]: { materialType: ToBMaterialFaceType.DUKE, owns: true },
  [ToBActionSchema.EXCHANGE_ROLES_0]: {
    materialType: ToBMaterialFaceType.AMBASSADOR,
    owns: true,
  },
  [ToBActionSchema.EXCHANGE_ROLES_1]: {
    materialType: ToBMaterialFaceType.AMBASSADOR,
    owns: true,
  },
  [ToBActionSchema.COLLECT_TREASURE]: {
    materialType: ToBMaterialFaceType.DUKE,
    owns: false,
  },
};

export function doubtActionStackItem(
  game: ToBGame,
  data: OnDoubtData
): Executor {
  return () => {
    LATEST_DOUBT_DATA.set(game, data);
    game.transaction.addEnemyAction(
      data.action.player,
      ToBActionSchemaGroup.DOUBT
    );
    // apply only once
    return false;
  };
}

export async function performDoubt(
  game: ToBGame,
  doubtAction: ClientAction<ToBGameIds>
) {
  const {
    action: doubtedAction,
    playerLives,
    then: onRight,
    catch: onWrong,
  } = LATEST_DOUBT_DATA.get(game)!;

  // determine if doubt is correct
  const roles = getDoubtVerifyRoles(game, doubtedAction, playerLives);
  const verified = roles !== null;
  const accuser = doubtAction.player;
  const accused = doubtedAction.player;

  // add game log
  game.logs.add(
    `${accuser.name} ${verified ? "wrongfully" : "correctly"} doubts ${
      accused.name
    }`
  );

  // on wrong doubts, exchange verified role(s)
  if (verified) {
    roleExchange(game, accused, roles!);
  }

  const forfeitLifeCallback = once(
    async () => await forcePlayerForfeitLife(game, verified ? accuser : accused)
  );

  // trigger appropriate doubt handler
  await performDoubtHandler(
    game,
    forfeitLifeCallback,
    doubtAction,
    verified ? onWrong : onRight
  );

  if (!forfeitLifeCallback.evaluated) {
    await forfeitLifeCallback();
  }
}

async function performDoubtHandler(
  game: ToBGame,
  forfeitLifeCallback: () => void,
  doubtAction: ClientAction<ToBGameIds>,
  handler?: OnDoubtHandler
) {
  if (handler !== undefined) {
    const result = handler(forfeitLifeCallback, doubtAction);
    if (result !== undefined) {
      await result;
    }
  }
}

function getDoubtVerifyRoles(
  game: ToBGame,
  { player, schema }: ClientAction<ToBGameIds>,
  lives = getLives(game.zones.byPlayer.get(player)![ToBZoneType.PLAYER_ROLES])
): MaterialWithZone<ToBGameIds>[] | null {
  const schemaType = game.actionSchemaMapping[schema.id];
  assert(
    Reflect.has(VERIFY_CONDITION, schemaType),
    `Action is not be doubtable: ${schema.id}`
  );
  const { materialType, owns } = VERIFY_CONDITION[schemaType]!;
  const roles = lives.filter(
    (it) => it.material.faces[1].typeId === materialType
  );
  if (owns) {
    // require player to own materialType
    return roles.length === 0 ? null : [pickOne(roles)];
  } else {
    // require player to not own materialType
    return roles.length === 0 ? lives : null;
  }
}

function roleExchange(
  game: ToBGame,
  player: Player,
  materials: MaterialWithZone<ToBGameIds>[]
) {
  const zone = game.zones.byPlayer.get(player)![ToBZoneType.PLAYER_ROLES];
  const deck = game.zones.global[ToBZoneType.DECK];
  // reveal verified roles
  for (const materialWithZone of materials) {
    game.transaction.revealGlobal(materialWithZone);
  }
  // wait for 1600ms delay for players to see roles
  game.transaction.flush(1600);
  // flip materials again and move to deck
  for (const materialWithZone of materials) {
    game.transaction.revealGlobal(materialWithZone, 0);
    game.transaction.move(materialWithZone, last(deck));
  }
  // wait for animation end
  game.transaction.flush();
  // shuffle deck
  game.transaction.shuffle(deck);
  // draw replacement roles
  times(materials.length, () => game.transaction.move(last(deck), last(zone)));
}
