import { IdFactoryType } from "@/id-factory";
import { GlobalZoneType, PlayerZoneType, ToBZoneType } from "@/zone/zone-types";
import {
  creditMaterialFactory,
  roleMaterialFactory,
  teamMaterialFactory,
} from "@/material/material-factories";
import { ToBGameIds } from "@/ToB.types";
import {
  defineBoard,
  GameBoard,
  Material,
  MaterialWithZone,
  Player,
  Zone,
} from "@raining.cards/server";
import { shuffle, times, withoutUnique } from "@raining.cards/util";
import { ToBMaterialFaceType } from "@/material/face-types";
import { ToBGame } from "@/ToB.game";

export type PlayerZones = Record<PlayerZoneType, Zone<ToBGameIds>>;

export interface PlayerZonesMap extends PlayerSpecificZones {
  byPlayer: Map<Player, PlayerZones>;
}

export interface PlayerSpecificZones {
  zonesSelf: PlayerZones;
  zonesOthers: Record<
    Exclude<PlayerZoneType, ToBZoneType.PLAYER_DOMAIN>,
    Zone<ToBGameIds>[]
  >;
}

export interface ToBZones {
  global: Record<GlobalZoneType, Zone<ToBGameIds>>;
  byPlayer: Map<Player, Record<PlayerZoneType, Zone<ToBGameIds>>>;
  ofOtherPlayers: Map<
    Player,
    Record<
      Exclude<PlayerZoneType, ToBZoneType.PLAYER_DOMAIN>,
      Zone<ToBGameIds>[]
    >
  >;
  board: GameBoard<ToBGameIds>;
}

export function getLives(
  zone: Zone<ToBGameIds>
): MaterialWithZone<ToBGameIds>[] {
  return zone.materials
    .slice(0, 2)
    .filter((it) => !it.reveal.global)
    .map((it) => ({ material: it, zone }));
}

export function createZones(game: ToBGame, players: Player[]): ToBZones {
  const deckMaterials = createDeck(game, players.length);
  const byPlayer = createPlayerZoneMap(game, deckMaterials, players);
  const global = createGlobalZones(game, deckMaterials);
  const asTree = [
    global[ToBZoneType.GLOBAL],
    ...[...byPlayer.values()].map((it) => it[ToBZoneType.PLAYER_DOMAIN]),
  ];

  return {
    global,
    byPlayer,
    ofOtherPlayers: getOtherPlayerZonesMap(players, byPlayer),
    board: defineBoard(asTree),
  };
}

function createDeck(
  game: ToBGame,
  playerCount: number
): Material<ToBGameIds>[] {
  const cardsPerRole = playerCount < 7 ? 3 : playerCount < 9 ? 4 : 5;
  return shuffle(
    times(cardsPerRole, (): Material<ToBGameIds>[] => [
      roleMaterialFactory(game, ToBMaterialFaceType.DUKE),
      roleMaterialFactory(game, ToBMaterialFaceType.ASSASSIN),
      roleMaterialFactory(game, ToBMaterialFaceType.AMBASSADOR),
      roleMaterialFactory(game, ToBMaterialFaceType.CAPTAIN),
      roleMaterialFactory(game, ToBMaterialFaceType.CONTESSA),
    ]).flat(1)
  );
}

function createGlobalZones(
  game: ToBGame,
  deckMaterials: Material<ToBGameIds>[]
): ToBZones["global"] {
  const idf = game.idFactories[IdFactoryType.ZONE];
  const deck: Zone<ToBGameIds> = {
    id: idf("deck"),
    typeId: ToBZoneType.DECK,
    reveal: {},
    materials: deckMaterials,
  };
  const treasury: Zone<ToBGameIds> = {
    id: idf("treasury"),
    typeId: ToBZoneType.TREASURY,
    reveal: {},
    materials: [],
  };
  return {
    [ToBZoneType.GLOBAL]: {
      id: idf("domain:global"),
      typeId: ToBZoneType.GLOBAL,
      reveal: {},
      children: [deck, treasury],
      materials: [],
    },
    [ToBZoneType.DECK]: deck,
    [ToBZoneType.TREASURY]: treasury,
  };
}

function createPlayerZoneMap(
  game: ToBGame,
  deckMaterials: Material<ToBGameIds>[],
  players: Player[]
): ToBZones["byPlayer"] {
  const byPlayer = new Map<Player, Record<PlayerZoneType, Zone<ToBGameIds>>>();

  let teamRed = true;
  for (const player of players) {
    byPlayer.set(
      player,
      createPlayerZones(game, player, deckMaterials, (teamRed = !teamRed))
    );
  }

  return byPlayer;
}

function createPlayerZones(
  game: ToBGame,
  player: Player,
  deckMaterials: Material<ToBGameIds>[],
  teamRed: boolean
): Record<PlayerZoneType, Zone<ToBGameIds>> {
  const idf = game.idFactories[IdFactoryType.ZONE];
  const team: Zone<ToBGameIds> = {
    id: idf(`${player.id}:team`),
    typeId: ToBZoneType.PLAYER_TEAM,
    reveal: {},
    materials: [teamMaterialFactory(game, teamRed)],
  };
  const credits: Zone<ToBGameIds> = {
    id: idf(`${player.id}:credits`),
    typeId: ToBZoneType.PLAYER_CREDITS,
    reveal: {},
    materials: times(2, () => creditMaterialFactory(game)),
  };
  const roles: Zone<ToBGameIds> = {
    id: idf(`${player.id}:roles`),
    typeId: ToBZoneType.PLAYER_ROLES,
    reveal: { secret: { [player.id]: 1 } },
    materials: times(2, () => deckMaterials.pop()!),
  };
  return {
    [ToBZoneType.PLAYER_TEAM]: team,
    [ToBZoneType.PLAYER_CREDITS]: credits,
    [ToBZoneType.PLAYER_ROLES]: roles,
    [ToBZoneType.PLAYER_DOMAIN]: {
      id: idf(`${player.id}:domain`),
      typeId: ToBZoneType.PLAYER_DOMAIN,
      reveal: {},
      children: [team, credits, roles],
      materials: [],
      customMeta: { relatedTo: [player] },
    },
  };
}

function getOtherPlayerZonesMap(
  players: Player[],
  byPlayer: ToBZones["byPlayer"]
): ToBZones["ofOtherPlayers"] {
  const ofOtherPlayers = new Map<
    Player,
    Record<
      Exclude<PlayerZoneType, ToBZoneType.PLAYER_DOMAIN>,
      Zone<ToBGameIds>[]
    >
  >();

  for (const player of players) {
    const otherPlayers = withoutUnique(players, player);
    ofOtherPlayers.set(player, {
      [ToBZoneType.PLAYER_TEAM]: otherPlayers.map(
        (p) => byPlayer.get(p)![ToBZoneType.PLAYER_TEAM]
      ),
      [ToBZoneType.PLAYER_CREDITS]: otherPlayers.map(
        (p) => byPlayer.get(p)![ToBZoneType.PLAYER_CREDITS]
      ),
      [ToBZoneType.PLAYER_ROLES]: otherPlayers.map(
        (p) => byPlayer.get(p)![ToBZoneType.PLAYER_ROLES]
      ),
    });
  }

  return ofOtherPlayers;
}
