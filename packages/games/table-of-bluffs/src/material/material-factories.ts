import { Material } from "@raining.cards/server";
import { ToBGameIds } from "@/ToB.types";
import { ToBMaterialFaceType } from "@/material/face-types";
import { ToBGame } from "@/ToB.game";
import { IdFactoryType } from "@/id-factory";

export function creditMaterialFactory(game: ToBGame): Material<ToBGameIds> {
  const idf = game.idFactories[IdFactoryType.MATERIAL_CREDIT];
  return {
    id: idf(),
    reveal: {},
    faces: [{ typeId: ToBMaterialFaceType.CREDIT }],
  };
}

export function roleMaterialFactory(
  game: ToBGame,
  role: ToBMaterialFaceType
): Material<ToBGameIds> {
  const idf = game.idFactories[IdFactoryType.MATERIAL_ROLE];
  return {
    id: idf(),
    reveal: {},
    faces: [{ typeId: ToBMaterialFaceType.ROLE_BACKDROP }, { typeId: role }],
  };
}

export function teamMaterialFactory(
  game: ToBGame,
  red: boolean
): Material<ToBGameIds> {
  const idf = game.idFactories[IdFactoryType.MATERIAL_TEAM];
  return {
    id: idf(),
    reveal: { global: red ? 1 : 0 },
    faces: [
      { typeId: ToBMaterialFaceType.TEAM_BLUE },
      { typeId: ToBMaterialFaceType.TEAM_RED },
    ],
  };
}
