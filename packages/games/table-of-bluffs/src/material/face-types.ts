import { MaterialFaceType, MaterialFaceTypeGroup } from "@raining.cards/server";
import { ToBGameIds } from "@/ToB.types";
import { UIMeta } from "@raining.cards/common";

export const CREDIT_SYMBOL = "₿";

export enum ToBMaterialFaceType {
  CREDIT = "credit",

  ROLE_BACKDROP = "role:backdrop",
  DUKE = "role:duke",
  ASSASSIN = "role:assassin",
  CAPTAIN = "role:captain",
  AMBASSADOR = "role:ambassador",
  CONTESSA = "role:contessa",

  TEAM_RED = "team:red",
  TEAM_BLUE = "team:blue",
}

export enum ToBMaterialFaceTypeGroup {
  ROLE = "role",
  TEAM = "team",
}

const rolesUIMeta: UIMeta = {
  styles: {
    backgroundColor: "white",
    color: "unset",
  },
};

export const MATERIAL_FACE_TYPES: MaterialFaceType<ToBGameIds>[] = [
  {
    id: ToBMaterialFaceType.CREDIT,
    label: CREDIT_SYMBOL,
    uiMeta: {
      styles: {
        borderRadius: "50%",
        color: "#1f5306",
        backgroundColor: "#abfb87",
        border: "2px solid #71ce46",
        fontWeight: "700",
      },
    },
  },

  {
    id: ToBMaterialFaceType.ROLE_BACKDROP,
    label: "[Role]",
    uiMeta: {
      styles: {
        backgroundColor: "#4d4949",
        color: "white",
        fontWeight: "600",
      },
    },
  },

  {
    id: ToBMaterialFaceType.DUKE,
    label: "Duke",
    description: `A duke may collect tax (${CREDIT_SYMBOL}3). They also can block foreign aid for other players. A duke may not collect the treasure.`,
    uiMeta: rolesUIMeta,
  },

  {
    id: ToBMaterialFaceType.ASSASSIN,
    label: "Assassin",
    description: `An assassin may perform assassinations for ${CREDIT_SYMBOL}3 to force another player to forfeit a life. A contessa can block assassinations against themselves.`,
    uiMeta: rolesUIMeta,
  },

  {
    id: ToBMaterialFaceType.CAPTAIN,
    label: "Captain",
    description: `A captain may steal ${CREDIT_SYMBOL}2 from any other player. Any captain or ambassador can block theft from themselves.`,
    uiMeta: rolesUIMeta,
  },

  {
    id: ToBMaterialFaceType.AMBASSADOR,
    label: "Ambassador",
    description: `An ambassador may exchange roles with the deck (draw two, discard two). They can block thefts from themselves as well.`,
    uiMeta: rolesUIMeta,
  },

  {
    id: ToBMaterialFaceType.CONTESSA,
    label: "Contessa",
    description: `A contessa may block assassinations against themselves.`,
    uiMeta: rolesUIMeta,
  },

  {
    id: ToBMaterialFaceType.TEAM_RED,
    description: `Red team. Unless all players are in the same team (or just two players are remaining), this player cannot perform actions against team members.`,
    uiMeta: { styles: { backgroundColor: "#ee555c" } },
  },

  {
    id: ToBMaterialFaceType.TEAM_BLUE,
    description: `Blue team. Unless all players are in the same team (or just two players are remaining), this player cannot perform actions against team members.`,
    uiMeta: { styles: { backgroundColor: "#2197ff" } },
  },
];

export const MATERIAL_FACE_TYPE_GROUPS: MaterialFaceTypeGroup<ToBGameIds>[] = [
  {
    id: ToBMaterialFaceTypeGroup.ROLE,
    faceIds: [
      ToBMaterialFaceType.ROLE_BACKDROP,
      ToBMaterialFaceType.DUKE,
      ToBMaterialFaceType.ASSASSIN,
      ToBMaterialFaceType.CAPTAIN,
      ToBMaterialFaceType.AMBASSADOR,
      ToBMaterialFaceType.CONTESSA,
    ],
  },
  {
    id: ToBMaterialFaceTypeGroup.TEAM,
    faceIds: [ToBMaterialFaceType.TEAM_RED, ToBMaterialFaceType.TEAM_BLUE],
  },
];
