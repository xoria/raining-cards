import { ToBGameIds, ToBPlayerUIMeta } from "@/ToB.types";
import { ACTIONS } from "@/action";
import { idFactories } from "@/id-factory";
import { createZones, ToBZones } from "@/zone/zones";
import { TOB_PLAYER_ZONE_TYPES } from "@/zone/zone-types";
import { useToBTransaction } from "@/ToBTransaction";
import { ToBActionSchema, ToBActionSchemaGroup } from "@/action/action.types";
import { assert } from "ts-essentials";
import {
  Client,
  Game,
  GameResultGroup,
  Participants,
  Player,
  pushMultiUpdate,
  pushParticipants,
  useExecutorStack,
  useGameLogs,
  Zone,
} from "@raining.cards/server";
import {
  fromEntries,
  onceBy,
  RingPointer,
  StringifySet,
} from "@raining.cards/util";
import { createClientMeta } from "@/ToB.meta";

const LATEST_TURN_ANNOUNCE = new WeakMap<ToBGame, Player>();

export class ToBGame implements Game<ToBGameIds> {
  idFactories = idFactories();
  zones = createZones(this, this.participants.players);
  playerByZone = getPlayerByZoneMap(this.zones, this.participants.players);
  actionSchemaMapping: Record<
    ToBGameIds["actionSchema"],
    ToBActionSchema
  > = fromEntries(Object.values(ToBActionSchema).map((it) => [it, it]));

  board = this.zones.board;
  meta = onceBy((client: Client, isPlayer: boolean) =>
    createClientMeta(
      this,
      client,
      isPlayer,
      this.participants.players,
      this.zones
    )
  );

  turn = new RingPointer([...this.participants.players]);
  actionStack = useExecutorStack();

  get currentPlayer(): Player {
    return this.turn.value;
  }

  logs = useGameLogs(this.participants);
  transaction = useToBTransaction(this);

  constructor(readonly participants: Participants) {
    for (const player of participants.players) {
      // explicit variable for strict type definition
      // noinspection UnnecessaryLocalVariableJS
      const uiMeta: ToBPlayerUIMeta = {
        classNames: new StringifySet<string>(),
      };
      player.uiMeta = uiMeta;
    }
  }

  isAlive(player: Player): boolean {
    return this.turn.items.includes(player);
  }

  // todo move logic aside of initial actionStack push into framework/concepts
  async run(): Promise<GameResultGroup[]> {
    const actionStackItem = this.actionStack.push({
      executors: [
        () => {
          const player = this.currentPlayer;
          if (LATEST_TURN_ANNOUNCE.get(this) !== player) {
            this.logs.add(`Next up: ${player.name}`);
            LATEST_TURN_ANNOUNCE.set(this, player);
          }
          this.transaction.addPlayerAction(player, ToBActionSchemaGroup.TURN);
        },
      ],
      condition: () => this.turn.size > 1,
    });
    // game loop
    while (this.turn.size > 1) {
      await this.performTask();
    }
    // do not expect any more client actions, the game is resolved
    this.actionStack.popAssert(actionStackItem);
    // push latest deltas (don't wait for resulting client action)
    pushMultiUpdate(this.participants, this.transaction.close()).catch(
      console.error
    );
    for (const player of this.participants.players) {
      player.uiMeta = {};
    }
    pushParticipants(this);
    // return game stats
    return Promise.resolve(this.collectStats());
  }

  // todo move into framework/concepts
  async performTask(): Promise<void> {
    if (!this.actionStack.peekAssert().exec()) {
      // action stack item condition is not fulfilled
      return;
    }

    const action = await pushMultiUpdate(
      this.participants,
      this.transaction.close()
    );
    const actionStackSizePre = this.actionStack.size;
    const nextTransaction = await ACTIONS[
      this.actionSchemaMapping[action.schema.id]
    ](this, action);
    const actionStackSizePost = this.actionStack.size;
    assert(
      actionStackSizePost === actionStackSizePre,
      `Action handlers must not modify the action stack size. ${action.schema.id}: ${actionStackSizePre} -> ${actionStackSizePost}`
    );

    return nextTransaction;
  }

  private collectStats(): GameResultGroup[] {
    const winnerGroup: GameResultGroup = {
      name: "Victory!",
      stats: [{ player: this.currentPlayer }],
    };
    const loserGroup: GameResultGroup = {
      name: "Maybe next time.",
      stats: this.participants.players
        .filter((player) => player !== this.currentPlayer)
        .map((player) => ({ player })),
    };

    return [winnerGroup, loserGroup];
  }
}

function getPlayerByZoneMap(
  zones: ToBZones,
  players: Player[]
): Map<Zone<ToBGameIds>, Player> {
  const result = new Map<Zone<ToBGameIds>, Player>();
  for (const player of players) {
    const playerZones = zones.byPlayer.get(player)!;
    for (const type of TOB_PLAYER_ZONE_TYPES) {
      result.set(playerZones[type], player);
    }
  }
  return result;
}
