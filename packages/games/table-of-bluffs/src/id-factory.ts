import { Alphabet, idFactory, IdFactory } from "@raining.cards/server";
import { fromEntries } from "@raining.cards/util";

export enum IdFactoryType {
  MATERIAL_CREDIT = "material:credit",
  MATERIAL_ROLE = "material:role",
  MATERIAL_TEAM = "material:team",
  ZONE = "zone",
}

export type IdFactories = Record<IdFactoryType, IdFactory>;

export function idFactories(): IdFactories {
  return fromEntries(
    Object.values(IdFactoryType).map((it) => [
      it,
      idFactory(it, Alphabet.INTERNAL),
    ])
  );
}
