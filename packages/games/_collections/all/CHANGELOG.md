# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.0] - 2021-04-22

### Added

- Game: Scared yet?

## [0.2.2] - 2021-04-15

### Added

- Companion-App: Who Knows Whom

## [0.2.1] - 2021-04-04

### Fixed

- Version range of gardeners
- Missing table-of-bluffs dependency

## [0.2.0] - 2021-04-01

### Changed

- Server compatibility changed to `@raining.cards/server@0.2.0`

## [0.1.0] - 2021-03-11

### Added

- Game: Table of Bluffs
- Game: Gardeners
