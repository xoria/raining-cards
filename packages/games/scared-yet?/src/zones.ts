import {
  defineBoard,
  DefinedZone,
  defineZone,
  defineZoneSingleton,
  idFactory,
  Player,
} from "@raining.cards/server";
import { fromEntries } from "@raining.cards/util";
import { GameIds } from "@raining.cards/common";

export type Zones = ReturnType<typeof useZones>;

export function useZones(players: Player[]) {
  const idfType = idFactory<GameIds["zoneType"]>("zone-type");
  const idf = idFactory<GameIds["zone"]>("zone");

  const monsters = defineZoneSingleton(idf("global"), undefined, {
    uiMeta: { styles: { gridColumn: "1 / -1", margin: "0 15% 22px" } },
  }).instance;

  const deck = defineZoneSingleton(idf("deck"), undefined, {
    label: "Deck",
    description:
      "Initially contained 2 minions (1), 2 skeletons (2), 2 wolves (3), 2 vampires (4), 2 golems (5), 1 ghost (6), 1 demon (7) and 1 dragon (9).",
  }).instance;
  const dungeon = defineZoneSingleton(idf("dungeon"), undefined, {
    label: "Dungeon",
    description: "The ordered set of monsters placed by the players.",
  }).instance;
  const dungeonDiscard = defineZoneSingleton(idf("dungeon:discard")).instance;
  const drawn = defineZoneSingleton(idf("drawn"), undefined, {
    label: "Drawn card",
    description: "The drawn card. Only visible to the active player.",
  }).instance;
  monsters.children = [deck, dungeon, dungeonDiscard, drawn];

  const hero = defineZoneSingleton(idf("hero"), undefined, {
    label: "Hero",
    description: "The hero and its equipment.",
  }).instance;
  const equipment = defineZoneSingleton(idf("hero:equipment")).instance;
  hero.children = [equipment];

  const playersZone = defineZoneSingleton(idf("players"), undefined, {
    label: "Discarded equipment",
  }).instance;
  const playerZoneType = defineZone(idfType("player"));
  const byPlayer = fromEntries(
    players.map((it) => [
      it.id,
      playerZoneType.factory(idf(it.id), undefined, {
        relatedTo: [it],
        description: `The equipment that ${it.name} has discarded.`,
      }),
    ])
  );
  playersZone.children = Object.values(byPlayer);

  const asList: DefinedZone[] = [
    monsters,
    ...(monsters.children as DefinedZone[]),
    hero,
    equipment,
    playersZone,
    ...(playersZone.children as DefinedZone[]),
  ];

  return {
    deck,
    dungeon,
    dungeonDiscard,
    drawn,
    hero,
    equipment,
    byPlayer,

    board: defineBoard([monsters, hero, playersZone]),
    asList,
  };
}
