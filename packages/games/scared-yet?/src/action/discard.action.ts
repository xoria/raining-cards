import {
  defineAction,
  DefinedMaterial,
  GameLogs,
  IdFactory,
  last,
  Transaction,
} from "@raining.cards/server";
import { ElementType, GameIds } from "@raining.cards/common";
import { Materials } from "@/material/materials";
import { Zones } from "@/zones";

export function useDiscardAction(
  idf: IdFactory<GameIds["actionSchema"]>,
  transaction: Transaction,
  gameLogs: GameLogs,
  materials: Materials,
  zones: Zones
) {
  return defineAction(
    {
      id: idf("discard"),
      name: "Discard",
      description:
        "Discard this equipment. The hero won't be able to use it within the dungeon.",
      origin: {
        type: ElementType.MATERIAL,
        position: [{ zone: zones.equipment }],
        materialView: { publicGroups: [materials.equipmentFaceGroup] },
      },
      target: { type: ElementType.GLOBAL },
    },
    ({ origin, player }) => {
      const equipment = origin.zone.materials[origin.index] as DefinedMaterial;
      transaction.move(origin, last(zones.byPlayer[player.id]));
      const pos = last(zones.drawn);
      transaction.clearSecret(pos);
      transaction.destroy(pos);
      gameLogs.add(
        `${player.name} discarded ${equipment.faces[0].type.label}.`
      );
    }
  );
}
