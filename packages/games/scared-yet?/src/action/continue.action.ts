import {
  ClientAction,
  ClientActorMaterial,
  defineAction,
  IdFactory,
  last,
  Transaction,
} from "@raining.cards/server";
import { ElementType, GameIds } from "@raining.cards/common";
import { Zones } from "@/zones";

export function useContinueAction(
  idf: IdFactory<GameIds["actionSchema"]>,
  transaction: Transaction,
  zones: Zones
) {
  const continueAction = defineAction(
    {
      id: idf("continue"),
      name: "Continue",
      origin: {
        type: ElementType.MATERIAL,
        position: [{ zone: zones.dungeon, index: [-1] }],
      },
      target: { type: ElementType.GLOBAL },
    },
    handler
  );
  const enterAction = defineAction(
    {
      id: idf("enter"),
      name: "Enter",
      origin: {
        type: ElementType.MATERIAL,
        position: [{ zone: zones.dungeon, index: [-1] }],
      },
      target: { type: ElementType.GLOBAL },
    },
    handler
  );

  return { continueAction, enterAction };

  function handler(action: ClientAction<GameIds, ClientActorMaterial>) {
    transaction.flipGlobal(action.origin);
    transaction.move(action.origin, last(zones.dungeonDiscard));
  }
}
