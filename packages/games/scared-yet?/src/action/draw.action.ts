import {
  defineAction,
  DefinedActionSchemaGroup,
  IdFactory,
  last,
  Transaction,
} from "@raining.cards/server";
import { ElementType, GameIds } from "@raining.cards/common";
import { Zones } from "@/zones";

export function useDrawAction(
  idf: IdFactory<GameIds["actionSchema"]>,
  transaction: Transaction,
  zones: Zones,
  placeGroup: DefinedActionSchemaGroup,
  next: () => Promise<unknown>
) {
  return defineAction(
    {
      id: idf("draw"),
      name: "Draw",
      description:
        "Draw a monster. If you do so and all other players fold afterwards, you have to send the hero into the dungeon.",
      origin: {
        type: ElementType.MATERIAL,
        position: [{ zone: zones.deck, index: [-1] }],
      },
      target: { type: ElementType.GLOBAL },
    },
    async ({ player, origin }) => {
      transaction.move(origin, last(zones.drawn));
      transaction.revealSecret(last(zones.drawn), [player]);
      transaction.addPlayerAction(player, placeGroup);
      await next();
    }
  );
}
