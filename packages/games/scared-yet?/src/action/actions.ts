import {
  defineActionSchemaGroup,
  DefinedAction,
  GameLogs,
  idFactory,
  Transaction,
  TurnOrder,
  useActions as useCoreActions,
} from "@raining.cards/server";
import { Zones } from "@/zones";
import { GameIds } from "@raining.cards/common";
import { Materials } from "@/material/materials";
import { useDiscardAction } from "@/action/discard.action";
import { useDrawAction } from "@/action/draw.action";
import { useFoldAction } from "@/action/fold.action";
import { usePlaceAction } from "@/action/place.action";
import { useContinueAction } from "@/action/continue.action";
import { useChooseMonsterAction } from "@/action/choose-monster.action";
import { useChooseHeroAction } from "@/action/choose-hero.action";

export type Actions = ReturnType<typeof useActions>;

export function useActions(
  transaction: Transaction,
  localTurnOrder: TurnOrder,
  gameLogs: GameLogs,
  materials: Materials,
  zones: Zones
) {
  const idf = idFactory<GameIds["actionSchema"]>("actions");
  const idfGroups = idFactory<GameIds["actionSchemaGroup"]>(
    "action-schema-groups"
  );

  const discard = useDiscardAction(
    idf,
    transaction,
    gameLogs,
    materials,
    zones
  );
  const place = usePlaceAction(idf, transaction, gameLogs, zones);
  const placeActions: DefinedAction[] = [discard, place];
  const placeGroup = defineActionSchemaGroup(idfGroups("place"), placeActions);

  const draw = useDrawAction(idf, transaction, zones, placeGroup, next);
  const fold = useFoldAction(idf, localTurnOrder, gameLogs);
  const drawActions: DefinedAction[] = [draw, fold];
  const drawGroup = defineActionSchemaGroup(idfGroups("draw"), drawActions);

  const { continueAction, enterAction } = useContinueAction(
    idf,
    transaction,
    zones
  );
  const continueGroup = defineActionSchemaGroup(idfGroups("continue"), [
    continueAction,
  ]);
  const enterGroup = defineActionSchemaGroup(idfGroups("enter"), [enterAction]);

  const chooseMonster = useChooseMonsterAction(idf, materials);
  const chooseMonsterGroup = defineActionSchemaGroup(
    idfGroups("choose-monster"),
    chooseMonster
  );

  const chooseHero = useChooseHeroAction(idf, materials, zones, transaction);
  const chooseHeroGroup = defineActionSchemaGroup(
    idfGroups("choose-hero"),
    chooseHero
  );

  const asList = [
    ...drawActions,
    ...placeActions,
    continueAction,
    enterAction,
    ...chooseMonster,
    ...chooseHero,
  ];
  const coreActions = useCoreActions(asList);

  return {
    asList,
    groups: [
      drawGroup,
      placeGroup,
      continueGroup,
      enterGroup,
      chooseMonsterGroup,
      chooseHeroGroup,
    ],
    drawGroup,
    placeGroup,
    continueGroup,
    enterGroup,
    chooseMonsterGroup,
    chooseHeroGroup,

    next,
  };

  async function next(): Promise<unknown> {
    const action = await transaction.pushUpdate();
    return await coreActions.perform(action);
  }
}
