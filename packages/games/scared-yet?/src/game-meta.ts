import { defineGameMeta, Game } from "@raining.cards/server";
import { Zones } from "@/zones";
import { Actions } from "@/action/actions";
import { Materials } from "@/material/materials";

export type GameMeta = Game["meta"];

export function useGameMeta(
  materials: Materials,
  zones: Zones,
  actions: Actions
): GameMeta {
  return defineGameMeta({
    materialFaceTypes: materials.faceTypes,
    materialFaceTypeGroups: materials.faceTypeGroups,
    zoneTypes: zones.asList,

    actionSchemas: actions.asList,
    actionSchemaGroups: actions.groups,
  });
}
