import { DefinedMaterial, Material } from "@raining.cards/server";
import { Zones } from "@/zones";
import { GameContext } from "@/game";
import { HeroData } from "@/material/hero-materials";
import { Materials } from "@/material/materials";

export type Dungeon = ReturnType<typeof useDungeon>;

export function useDungeon(materials: Materials, zones: Zones) {
  return { preDungeon, fight };

  async function preDungeon(heroData: HeroData, ctx: GameContext) {
    const {
      gameLogs,
      dealer,
      localTurnOrder: { current: player },
    } = ctx;
    gameLogs.add(`${player.name} is preparing the hero for the dungeon.`);
    dealer.addHP(heroData.baseHP);
    for (const material of [...zones.equipment.materials]) {
      await equipmentDefByMaterial(ctx, material)?.preDungeon?.(ctx, material);
    }
  }

  async function fight(heroData: HeroData, ctx: GameContext) {
    const {
      gameLogs,
      dealer,
      zones: { dungeonDiscard },
    } = ctx;

    const monster = dungeonDiscard.materials[
      dungeonDiscard.materials.length - 1
    ] as DefinedMaterial;
    const monsterName = monster.faces[1].type.label!;
    const strength = materials.getMonsterStrength(monster);
    await equipmentDefByMaterial(ctx, monster);

    let defeated = false;
    for (const material of [...zones.equipment.materials]) {
      defeated = !!(await equipmentDefByMaterial(ctx, material)?.fight?.(
        monster,
        ctx,
        material as DefinedMaterial
      ));
      if (defeated) {
        break;
      }
    }

    if (!defeated) {
      dealer.removeHP(strength);
      gameLogs.add(`Lost ${strength}HP against ${monsterName}.`);
    }

    for (const material of [...zones.equipment.materials]) {
      equipmentDefByMaterial(ctx, material)?.postFight?.(ctx, material);
    }
  }

  function equipmentDefByMaterial(
    { materials }: GameContext,
    material: Material
  ) {
    return materials.equipment.all.asLookup[material.faces[0].typeId];
  }
}
