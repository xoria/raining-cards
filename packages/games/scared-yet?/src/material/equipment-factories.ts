import {
  DefinedMaterial,
  defineMaterialFaceSingleton,
  defineMaterialSingleton,
  IdFactory,
  Material,
  MaterialDefinition,
  MaterialFaceSingletonDefinition,
  MaterialFaceType,
  MaterialSingletonDefinition,
} from "@raining.cards/server";
import { GameIds } from "@raining.cards/common";
import { GameContext } from "@/game";
import { MonsterMaterials } from "@/material/monster-materials";

export interface EquipmentDefinition<Data = NoData> {
  face: MaterialFaceSingletonDefinition;
  material: MaterialSingletonDefinition;
  data: Data;

  preDungeon?: (ctx: GameContext, equipment: Material) => Promise<void> | void;
  fight?: (
    monster: DefinedMaterial,
    ctx: GameContext,
    equipment: DefinedMaterial
  ) => Promise<boolean> | boolean;
  postFight?: (ctx: GameContext, equipment: Material) => Promise<void> | void;
}

export type EquipmentFactories = ReturnType<typeof useEquipmentFactories>;

type NoData = Omit<MaterialFaceType, "id"> &
  Required<Pick<MaterialFaceType, "label">>;
type HPData = { hp: number } & NoData;
type KillData = { defeats: MaterialDefinition[] } & NoData;

export function useEquipmentFactories(
  idfFace: IdFactory<GameIds["materialFaceType"]>,
  idfMaterial: IdFactory<GameIds["material"]>,
  materialsMonster: MonsterMaterials
) {
  return {
    useEquipmentHP,
    useEquipmentKill,
    useEquipmentLifeSteal,
    useEquipmentBan,
    useEquipmentPotion,
  };

  function useEquipmentHP(
    id: string,
    data: HPData
  ): EquipmentDefinition<HPData> {
    const face = defineMaterialFaceSingleton(idfFace(id), data);
    const material = defineMaterialSingleton(idfMaterial(id), face);
    return {
      face,
      material,
      data,

      preDungeon({ dealer, transaction, zones }: GameContext, equipment) {
        dealer.addHP(data.hp);
        transaction.destroy({ zone: zones.equipment, material: equipment });
      },
    };
  }

  function useEquipmentKill(
    id: string,
    data: KillData
  ): EquipmentDefinition<KillData> {
    const face = defineMaterialFaceSingleton(idfFace(id), data);
    const material = defineMaterialSingleton(idfMaterial(id), face);
    return {
      face,
      material,
      data,

      fight(monster, { gameLogs }) {
        if (data.defeats.some((it) => it.is(monster))) {
          gameLogs.add(
            `The hero defeats ${monster.faces[1].type.label} (${face.type.label}).`
          );
          return true;
        }
        return false;
      },
    };
  }

  function useEquipmentLifeSteal(
    id: string,
    data: KillData
  ): EquipmentDefinition<KillData> {
    const face = defineMaterialFaceSingleton(idfFace(id), data);
    const material = defineMaterialSingleton(idfMaterial(id), face);
    return {
      face,
      material,
      data,

      fight(monster, { dealer, gameLogs }) {
        if (data.defeats.some((it) => it.is(monster))) {
          const hp = materialsMonster.getMonsterStrength(monster);
          gameLogs.add(
            `The hero recovered ${hp}HP facing a ${monster.faces[1].type.label} (${face.type.label}).`
          );
          dealer.addHP(hp);
          return true;
        }
        return false;
      },
    };
  }

  function useEquipmentBan(id: string, data: NoData): EquipmentDefinition {
    const face = defineMaterialFaceSingleton(idfFace(id), data);
    const material = defineMaterialSingleton(idfMaterial(id), face);
    let target: MaterialFaceSingletonDefinition | null = null;
    return {
      face,
      material,
      data,

      async preDungeon({ localTurnOrder, transaction, actions, gameLogs }) {
        const player = localTurnOrder.current;
        transaction.addPlayerAction(player, actions.chooseMonsterGroup);
        gameLogs.add(
          `${player.name} needs to choose some monster type (${data.label}).`
        );
        target = (await actions.next()) as MaterialFaceSingletonDefinition;
        gameLogs.add(`They chose ${target.type.label}.`);
      },

      fight(monster, { gameLogs }) {
        if (target!.type.id === monster.faces[1].typeId) {
          gameLogs.add(
            `The hero defeats ${monster.faces[1].type.label} (${face.type.label}).`
          );
          return true;
        }
        return false;
      },
    };
  }

  function useEquipmentPotion(
    id: string,
    data: HPData
  ): EquipmentDefinition<HPData> {
    const face = defineMaterialFaceSingleton(idfFace(id), data);
    const material = defineMaterialSingleton(idfMaterial(id), face);
    return {
      face,
      material,
      data,

      postFight({ dealer, transaction, gameLogs, zones }, equipment) {
        if (dealer.countHP() === 0) {
          dealer.addHP(data.hp);
          transaction.destroy({ zone: zones.equipment, material: equipment });
          gameLogs.add(`The hero recovered to ${data.hp}HP (${data.label}).`);
        }
      },
    };
  }
}
