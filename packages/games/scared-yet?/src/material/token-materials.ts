import {
  defineMaterial,
  defineMaterialFaceSingleton,
  IdFactory,
} from "@raining.cards/server";
import { GameIds } from "@raining.cards/common";

export function useTokenMaterials(
  idfFace: IdFactory<GameIds["materialFaceType"]>,
  idfMaterial: IdFactory<GameIds["material"]>
) {
  const hpTokenFace = defineMaterialFaceSingleton(idfFace("hp"), {
    label: "HP",
    description: "Current health points of the hero",
    uiMeta: { style: { borderRadius: "50%" } },
  });
  const hpToken = defineMaterial(idfMaterial("hp"), hpTokenFace);

  return { hpToken, tokenFaceTypes: [hpTokenFace] };
}
