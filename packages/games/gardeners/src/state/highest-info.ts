import { Player } from "@raining.cards/server";
import { GMaterialColor } from "@/material/material-faces";
import { assert } from "ts-essentials";

export interface HighestInfoRef {
  player: Player;
  color?: GMaterialColor;
  value: number;
}

export type HighestInfo = ReturnType<typeof useHighestInfo>;

export function useHighestInfo() {
  let ref: HighestInfoRef | undefined;

  return { access, set };

  function access(): HighestInfoRef {
    assert(ref !== undefined, "No highest info defined.");
    return ref;
  }

  function set(player: Player, color?: GMaterialColor, value = -1) {
    ref = { player, value, color };
  }
}
