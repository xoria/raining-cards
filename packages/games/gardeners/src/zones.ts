import {
  defineBoard,
  DefinedZone,
  defineZone,
  defineZoneSingleton,
  idFactory,
  Player,
} from "@raining.cards/server";
import { withLookup } from "@raining.cards/util";
import { GameIds } from "@raining.cards/common";

export type Zones = ReturnType<typeof useZones>;

export function useZones(players: Player[]) {
  const idfType = idFactory<GameIds["zoneType"]>("zone-type");
  const idf = idFactory<GameIds["zone"]>("zone");

  const global = defineZoneSingleton(idf("global"), undefined, {
    uiMeta: { styles: { gridColumn: "1 / -1", margin: "0 15% 22px" } },
  }).instance;

  const token = defineZoneSingleton(idf("token")).instance;
  const heap = defineZoneSingleton(idf("heap"), { global: 1 }).instance;
  global.children = [token, heap];

  const globalZones = { domain: global, token, heap };

  const domainDef = defineZone(idfType("domain"));
  const handDef = defineZone(idfType("hand"), { label: "Hand" });
  const scoreDef = defineZone(idfType("score"), { label: "Score" });

  const playerZones = withLookup(
    players.map((player) => {
      const domain = domainDef.factory(idf(`domain:${player.id}`), undefined, {
        relatedTo: [player],
      });
      const hand = handDef.factory(idf(`hand:${player.id}`), {
        secret: { [player.id]: 1 },
      });
      const score = scoreDef.factory(idf(`score:${player.id}`), { global: 1 });

      domain.children = [hand, score];
      return { id: player.id, domain, hand, score };
    })
  );

  const asList: DefinedZone[] = [
    global,
    token,
    heap,
    ...playerZones.asList.flatMap((it) => [it.domain, it.hand, it.score]),
  ];

  return {
    global: globalZones,
    player: playerZones,

    board: defineBoard([global, ...playerZones.asList.map((it) => it.domain)]),

    asList,
  };
}
