import {
  defineMaterialFaceSingleton,
  idFactory,
  MaterialFaceDefinition,
} from "@raining.cards/server";
import { fromEntries, times } from "@raining.cards/util";
import { GameIds } from "@raining.cards/common";

export enum GMaterialColor {
  RED = "red",
  GREEN = "green",
  BLUE = "blue",
  ORANGE = "orange",
  MAGENTA = "magenta",
}

export const G_MATERIAL_COLORS = Object.values(GMaterialColor);

const BG_COLOR: Record<GMaterialColor, string> = {
  [GMaterialColor.RED]: "#ffcbcb",
  [GMaterialColor.GREEN]: "#c0f9ac",
  [GMaterialColor.BLUE]: "#90dbff",
  [GMaterialColor.ORANGE]: "#ffeab9",
  [GMaterialColor.MAGENTA]: "#ffd9fe",
};

export type MaterialFaces = ReturnType<typeof useMaterialFaces>;

export function useMaterialFaces() {
  const idf = idFactory<GameIds["materialFaceType"]>("material-face-type");

  const sun = defineMaterialFaceSingleton(idf, {
    description: "No color has yet been played.",
    uiMeta: {
      styles: {
        backgroundColor: "#fbfa63",
        borderRadius: "50%",
        border: "2px dashed #fbd04e",
      },
    },
  });
  const tokens = fromEntries(
    G_MATERIAL_COLORS.map((color) => [
      color,
      defineMaterialFaceSingleton(idf, {
        description: `${color} has to be served this turn.`,
        uiMeta: {
          styles: { backgroundColor: BG_COLOR[color], borderRadius: "50%" },
        },
      }),
    ])
  );

  const backdrop = defineMaterialFaceSingleton(idf, {
    label: "[Card]",
    description: "Some hand card; unknown to you.",
    uiMeta: { styles: { backgroundColor: "#c6b8b8" } },
  });
  const rain = defineMaterialFaceSingleton(idf, {
    label: "Rain",
    description: "Just a rainy day. Does have no effect.",
    uiMeta: { styles: { backgroundColor: "white" } },
  });
  const clippers = defineMaterialFaceSingleton(idf, {
    label: "Hedge clippers",
    description: "The player who wins the trick loses his highest score card.",
    uiMeta: { styles: { backgroundColor: "white" } },
  });
  const byColor = fromEntries(
    G_MATERIAL_COLORS.map((color) => [
      color,
      times(12, (idx) => {
        const value = idx + 1;
        const faceDef = defineMaterialFaceSingleton(idf, {
          label: value.toString(),
          description: `The ${color} ${value}.`,
          uiMeta: { styles: { backgroundColor: BG_COLOR[color] } },
        });
        return { faceDef, value };
      }),
    ])
  );

  const asList: MaterialFaceDefinition[] = [
    sun,
    ...Object.values(tokens),
    backdrop,
    rain,
    clippers,
    ...Object.values(byColor).flatMap((list) => list.map((it) => it.faceDef)),
  ];

  return {
    sun,
    tokens,

    rain,
    clippers,
    backdrop,
    byColor,

    asList,
  };
}
