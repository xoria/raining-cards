import { mapValues, times } from "@raining.cards/util";
import {
  defineMaterial,
  defineMaterialSingleton,
  idFactory,
  Material,
} from "@raining.cards/server";
import {
  G_MATERIAL_COLORS,
  GMaterialColor,
  MaterialFaces,
} from "@/material/material-faces";
import { GameIds } from "@raining.cards/common";

export interface ColorAndValue {
  color: GMaterialColor;
  value: number;
}

export type Materials = ReturnType<typeof useMaterials>;

export function useMaterials(materialFaces: MaterialFaces) {
  const colorAndValue = new Map<Material, ColorAndValue>();
  const idf = idFactory<GameIds["material"]>("material");
  const backdropFace = materialFaces.backdrop;

  const sun = defineMaterialSingleton(idf("sun"), materialFaces.sun);
  const tokens = mapValues(materialFaces.tokens, (face, color) =>
    defineMaterialSingleton(idf(`token:${color}`), face)
  );

  const rainDef = defineMaterial(idf("rain"), [
    backdropFace,
    materialFaces.rain,
  ]);
  const clippersDef = defineMaterial(idf("clippers"), [
    backdropFace,
    materialFaces.clippers,
  ]);

  const byColor = mapValues(materialFaces.byColor, (faces, color) =>
    faces.map(({ faceDef, value }) => {
      const { instance } = defineMaterialSingleton(idf(`${color}:${value}`), [
        backdropFace,
        faceDef,
      ]);
      colorAndValue.set(instance, { color, value });
      return instance;
    })
  );

  const deck = [
    ...times(2, () => rainDef.factory()),
    ...times(2, () => clippersDef.factory()),
    ...G_MATERIAL_COLORS.flatMap((color) => byColor[color]),
  ];

  return {
    sun,
    getToken,

    rain: rainDef,
    clippers: clippersDef,

    deck,
    getColorAndValue,
    sortByColorAndValue,
  };

  function getToken(color: GMaterialColor) {
    return tokens[color];
  }

  function getColorAndValue(it: Material): ColorAndValue | undefined {
    return colorAndValue.get(it);
  }

  function sortByColorAndValue(materials: Material[]): Material[] {
    return materials.sort(materialCompare);
  }

  function materialCompare(a: Material, b: Material): number {
    const vA = getColorAndValue(a);
    const vB = getColorAndValue(b);
    if (vA === undefined) {
      if (vB !== undefined) {
        return -1;
      }
      // rain < clippers
      return rainDef.is(a) ? (rainDef.is(b) ? 0 : -1) : 1;
    }
    if (vB === undefined) {
      return 1;
    }
    return vA.color.localeCompare(vB.color) || vA.value - vB.value;
  }
}
