import {
  defineActionSchemaGroup,
  idFactory,
  Player,
} from "@raining.cards/server";
import { Actions } from "@/action/actions";
import { onceBy } from "@raining.cards/util";
import { GameIds } from "@raining.cards/common";

export type ActionSchemaGroups = ReturnType<typeof useActionSchemaGroups>;

export function useActionSchemaGroups(actions: Actions) {
  const idf = idFactory<GameIds["actionSchemaGroup"]>("action-schema-groups");

  const choose = defineActionSchemaGroup(idf("choose"), actions.choose);

  return {
    choose,
    asList: [choose],
    of: onceBy(of),
  };

  function of(player: Player) {
    const playCard = defineActionSchemaGroup(
      idf("play-card"),
      actions.of(player).playCard
    );

    return {
      playCard,
      asList: [playCard],
    };
  }
}
