import {
  defineActionCondition,
  defineActionConditionItem,
  idFactory,
  Player,
} from "@raining.cards/server";
import { fromEntries, matchAll, matchSome, onceBy } from "@raining.cards/util";
import { Zones } from "@/zones";
import { MaterialFaceGroups } from "@/material/material-face-groups";
import { G_MATERIAL_COLORS, MaterialFaces } from "@/material/material-faces";
import { GameIds } from "@raining.cards/common";

export type ActionConditions = ReturnType<typeof useActionConditions>;

export function useActionConditions(
  materialFaces: MaterialFaces,
  materialFaceGroups: MaterialFaceGroups,
  zones: Zones
) {
  const idf = idFactory<GameIds["actionCondition"]>("action-condition");

  // no color has been fixed yet
  const noColor = defineActionCondition(
    idf("token:sun"),
    defineActionConditionItem({
      zones: [zones.global.token],
      materialFaceTypeGroups: [materialFaceGroups.tokens],
      count: { max: 0 },
    })
  );

  const tokenCondition = fromEntries(
    G_MATERIAL_COLORS.map((color) => [
      color,
      defineActionConditionItem({
        zones: [zones.global.token],
        materialFaceTypes: [materialFaces.tokens[color].type],
        count: { min: 1 },
      }),
    ])
  );

  // token color condition
  const tokenColor = fromEntries(
    G_MATERIAL_COLORS.map((color) => [
      color,
      defineActionCondition(idf(`token:${color}`), tokenCondition[color]),
    ])
  );

  return {
    tokenColor,
    noColor,

    asList: Object.values(tokenColor).concat([noColor]),
    of: onceBy(of),
  };

  function of(player: Player) {
    const playerZonesLookup = zones.player.asLookup[player.id];

    // player has no card of the token color
    const cannotServe = defineActionCondition(
      idf("serve:empty"),
      matchSome(
        G_MATERIAL_COLORS.map((color) =>
          matchAll([
            // TODO allow string reference of already defined conditions
            tokenCondition[color],
            defineActionConditionItem({
              zones: [playerZonesLookup.hand],
              materialFaceTypeGroups: [materialFaceGroups.byColor[color]],
              count: { max: 0 },
            }),
          ])
        )
      )
    );

    return {
      cannotServe,
      asList: [cannotServe],
    };
  }
}
