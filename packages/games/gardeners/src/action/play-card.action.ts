import {
  ClientAction,
  ClientActorGlobal,
  ClientActorMaterial,
  defineAction,
  DefineActorSchemaMaterial,
  DefinedMaterial,
  GameLogs,
  IdFactory,
  Player,
} from "@raining.cards/server";
import { Materials } from "@/material/material";
import { Zones } from "@/zones";
import { G_MATERIAL_COLORS } from "@/material/material-faces";
import { matchSome } from "@raining.cards/util";
import { ElementType, GameIds } from "@raining.cards/common";
import { MaterialFaceGroups } from "@/material/material-face-groups";
import { ActionConditions } from "@/action/action-conditions";
import { HighestInfo } from "@/state/highest-info";
import { Dealer } from "@/dealer";

export function useActionPlayCard(
  player: Player,
  idf: IdFactory<GameIds["actionSchema"]>,
  actionConditions: ActionConditions,
  dealer: Dealer,
  materials: Materials,
  materialFaceGroups: MaterialFaceGroups,
  zones: Zones,
  gameLogs: GameLogs,
  highestInfo: HighestInfo
) {
  const position: DefineActorSchemaMaterial["position"] = [
    { zone: zones.player.asLookup[player.id].hand },
  ];

  const colors = G_MATERIAL_COLORS.map((color) =>
    defineAction(
      {
        id: idf(`play:${color}`),
        name: "Play",
        origin: {
          type: ElementType.MATERIAL,
          position,
          materialView: { secretGroups: [materialFaceGroups.byColor[color]] },
        },
        target: { type: ElementType.GLOBAL },
        condition: matchSome([
          actionConditions.noColor,
          actionConditions.of(player).cannotServe,
          actionConditions.tokenColor[color],
        ]),
      },
      playCard
    )
  );

  const white = defineAction(
    {
      id: idf(`choose:white`),
      name: "Play",
      origin: {
        type: ElementType.MATERIAL,
        position,
        materialView: { secretGroups: [materialFaceGroups.white] },
      },
      target: { type: ElementType.GLOBAL },
    },
    playCard
  );

  return colors.concat([white]);

  function playCard(
    action: ClientAction<GameIds, ClientActorMaterial, ClientActorGlobal>
  ) {
    const { token } = zones.global;
    const player = action.player;
    const { zone, index } = action.origin;
    const material = zone.materials[index] as DefinedMaterial;
    const cav = materials.getColorAndValue(material);
    if (cav === undefined) {
      gameLogs.add(`${player.name} plays ${material.faces[1].type.label}`);
      dealer.playCard(action.origin);
      return;
    }
    if (materials.sun.is(token.materials[0])) {
      // first colored card => initialize trick
      dealer.setToken(cav.color);
      gameLogs.add(
        `${player.name} sets the trick with ${cav.color}:${cav.value}`
      );
      // first colored trick => also highest by default
      highestInfo.set(player, cav.color, cav.value);
      dealer.playCard(action.origin);
    } else {
      // check if higher than current highest
      const highest = highestInfo.access();
      if (
        highest.color === undefined ||
        (cav.color === highest.color && cav.value > highest.value)
      ) {
        gameLogs.add(
          `${player.name} plays ${cav.color}:${cav.value} which is a new high`
        );
        highestInfo.set(player, cav.color, cav.value);
        dealer.playCard(action.origin);
      } else {
        gameLogs.add(
          `${player.name} adds ${cav.color}:${cav.value} to the heap`
        );
        dealer.playCard(action.origin);
      }
    }
  }
}
