import {
  last,
  Material,
  MaterialPositionDescriptor,
  Transaction,
  TurnOrder,
  Zone,
} from "@raining.cards/server";
import { pickOne, shuffle } from "@raining.cards/util";
import { Materials } from "@/material/material";
import { Zones } from "@/zones";
import { GMaterialColor } from "@/material/material-faces";

export type Dealer = ReturnType<typeof useDealer>;

export function useDealer(
  transaction: Transaction,
  materials: Materials,
  zones: Zones,
  turnOrder: TurnOrder
) {
  const handSize = 15 - (turnOrder.size - 3);

  return { deal, playCard, takeAndCountClippers, setToken, dropHighestCard };

  function deal() {
    const byPlayer = zones.player.asLookup;
    // remove remaining materials
    for (const player of turnOrder.players) {
      const { hand, score } = byPlayer[player.id];
      transaction.destroyAll(hand);
      transaction.destroyAll(score);
    }
    transaction.flush();
    // distribute player cards
    shuffle(materials.deck);
    let offset = 0;
    const currentPlayer = turnOrder.current;
    for (const player of turnOrder.players) {
      const handZone = byPlayer[player.id].hand;
      const size = player === currentPlayer ? handSize - 1 : handSize;
      const handMaterials = materials.sortByColorAndValue(
        materials.deck.slice(offset, offset + size)
      );
      for (const material of handMaterials) {
        transaction.create(material, last(handZone));
      }
      offset += size;
    }
  }

  function playCard(pos: MaterialPositionDescriptor) {
    transaction.move(pos, last(zones.global.heap));
  }

  function takeAndCountClippers(score: Zone) {
    const { heap } = zones.global;
    let clippers = 0;
    for (const material of [...heap.materials]) {
      if (materials.clippers.is(material)) {
        clippers++;
        transaction.destroy({ material, zone: heap });
      } else {
        if (!takeScoreCard(score, material)) {
          transaction.destroy({ material, zone: heap });
        }
      }
    }
    return clippers;
  }

  function setToken(color?: GMaterialColor) {
    const { token } = zones.global;
    transaction.destroyAll(token);
    transaction.create(
      (color ? materials.getToken(color) : materials.sun).instance,
      last(token)
    );
  }

  function takeScoreCard(score: Zone, material: Material): boolean {
    if (materials.rain.is(material)) {
      return false;
    }

    const { heap } = zones.global;
    const { color, value } = materials.getColorAndValue(material)!;
    const scoreColorMaterial = score.materials.find(
      (it) => materials.getColorAndValue(it)!.color === color
    );

    if (scoreColorMaterial === undefined) {
      transaction.move({ material, zone: heap }, last(score));
      return true;
    }

    if (materials.getColorAndValue(scoreColorMaterial)!.value >= value) {
      return false;
    }

    const pos = {
      zone: score,
      index: score.materials.indexOf(scoreColorMaterial),
    };
    transaction.destroy(pos);
    transaction.move({ material, zone: heap }, pos);
    return true;
  }

  async function dropHighestCard(score: Zone, count: number) {
    do {
      const list = score.materials.map((material) => ({
        material,
        value: materials.getColorAndValue(material)!.value,
      }));
      if (list.length === 0) {
        return;
      }
      let highest = [list[0]];
      for (let i = 1; i < list.length; i++) {
        const item = list[i];
        if (item.value > highest[0].value) {
          highest = [item];
        } else if (item.value === highest[0].value) {
          highest.push(item);
        }
      }
      // TODO if highest.length > count, ask player
      transaction.destroy({
        material: pickOne(highest).material,
        zone: score,
      });
    } while (--count);
  }
}
