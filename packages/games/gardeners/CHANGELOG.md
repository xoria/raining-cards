# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.1] - 2021-04-04

### Changed

- Shuffle player list

## [0.2.0] - 2021-04-01

### Changed

- Server compatibility changed to `@raining.cards/server@0.2.0`
- Use new `define*` API (major rewrite of internals)
- Sun is drawn after hand cards to better reflect real-life order of actions
- Improved material descriptions

## [0.1.0] - 2021-03-11

### Added

- Initial Release
