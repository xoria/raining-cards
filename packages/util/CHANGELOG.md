# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.2] - 2021-04-22

### Added

- `RingPointer#reset`
- `withPartialLookup` (and respective return type)

### Fixed

- Improve return type declaration of `withLookup`.
- `deepEqual` check always assumed objects to be equal.

## [0.2.1] - 2021-04-04

### Added

- `bakeCountRange`

## [0.2.0] - 2021-04-01

### Changed

- Renamed `RingBuffer#to` to `RingBuffer#pointTo`

### Added

- `StringifySet#delete`
- Cache for `StringifySet#toJSON`
- Optional constructor items to `StringifySet`
- `RingBuffer#toJSON`
- Injection utils
- `withLookup` list util with related type
- String case utility: `titleCase`
- A mapped singleton helper `onceBy`
- `deepEqual*` functions
- `Obj` type for any record
- `omit`/`pick` utility functions

## [0.1.0] - 2021-03-11

### Added

- Initial Release
