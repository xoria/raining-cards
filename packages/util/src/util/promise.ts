import { noop } from "ts-essentials";
import { Subscription } from "@/util/subscription";

export interface CancellablePromise<T> extends Promise<T> {
  cancel: Subscription;
}

export const NEVER = new Promise<never>(noop);
export const RESOLVED = Promise.resolve();

export function delay(ms: number): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
