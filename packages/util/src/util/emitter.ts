import { once } from "@/util/function";
import { Subscription, unsubscribeAll } from "@/util/subscription";
import { CancellablePromise } from "@/util/promise";
import { ValueOf } from "ts-essentials";

type Handler<
  EventMap extends Record<string | symbol, unknown>,
  Key extends keyof EventMap
> = (value: EventMap[Key]) => unknown;

type Handlers<EventMap extends Record<string | symbol, unknown>> = {
  [Key in keyof EventMap]?: Handler<EventMap, Key>[];
};

// eslint-disable-next-line @typescript-eslint/ban-types
type Fn = Function;

export interface Emitter<EventMap extends Record<string | symbol, unknown>> {
  on: <Key extends keyof EventMap>(
    key: Key,
    handler: Handler<EventMap, Key>
  ) => Subscription;

  when: <Key extends keyof EventMap>(
    key: Key
  ) => CancellablePromise<EventMap[Key]>;

  once: <Key extends keyof EventMap>(
    key: Key,
    handler: Handler<EventMap, Key>
  ) => Subscription;

  off: <Key extends keyof EventMap>(
    key: Key,
    handler?: Handler<EventMap, Key>
  ) => void;

  emit: <Key extends keyof EventMap>(
    ...args: EventMap[Key] extends void
      ? [Key] | [Key, EventMap[Key]]
      : [Key, EventMap[Key]]
  ) => /* someListenerCalled */ boolean;

  /**
   * Similar to emit but always waits for promise, if some event handler returns
   * one.
   */
  trigger: <Key extends keyof EventMap>(
    ...args: EventMap[Key] extends void
      ? [Key] | [Key, EventMap[Key]]
      : [Key, EventMap[Key]]
  ) => Promise</* someListenerCalled */ boolean>;
}

export async function eventRace<
  EventMap extends Record<string | symbol, unknown>
>(
  emitter: Emitter<EventMap>,
  keys: (keyof EventMap)[]
): Promise<[ValueOf<EventMap>, keyof EventMap]> {
  return await new Promise<[ValueOf<EventMap>, keyof EventMap]>((resolve) => {
    const subscriptions = keys.map((key) =>
      emitter.once(key, next.bind(null, key))
    );

    function next(key: keyof EventMap, value: ValueOf<EventMap>) {
      unsubscribeAll(subscriptions);
      resolve([value, key]);
    }
  });
}

export function emitter<
  EventMap extends Record<string | symbol, unknown>
>(): Emitter<EventMap> {
  const listeners = {} as Handlers<EventMap>;

  return {
    on: (_on as Fn).bind(null, listeners) as Emitter<EventMap>["on"],
    once: (_once as Fn).bind(null, listeners) as Emitter<EventMap>["once"],
    when: (_when as Fn).bind(null, listeners) as Emitter<EventMap>["when"],
    off: (_off as Fn).bind(null, listeners) as Emitter<EventMap>["off"],
    emit: (_emit as Fn).bind(null, listeners) as Emitter<EventMap>["emit"],
    trigger: (_trigger as Fn).bind(
      null,
      listeners
    ) as Emitter<EventMap>["trigger"],
  };
}

export function withEmitter<
  T,
  EventMap extends Record<string | symbol, unknown>
>(
  obj: Omit<T, keyof Emitter<EventMap>>
): Omit<T, keyof Emitter<EventMap>> & Emitter<EventMap> {
  return { ...obj, ...emitter<EventMap>() };
}

function _on<
  EventMap extends Record<string | symbol, unknown>,
  Key extends keyof EventMap
>(
  handlers: Handlers<EventMap>,
  key: Key,
  handler: Handler<EventMap, Key>
): Subscription {
  if (handlers.hasOwnProperty(key)) {
    handlers[key]!.push(handler);
  } else {
    handlers[key] = [handler];
  }
  return once(() => _off(handlers, key, handler));
}

function _once<
  EventMap extends Record<string | symbol, unknown>,
  Key extends keyof EventMap
>(
  handlers: Handlers<EventMap>,
  key: Key,
  handler: Handler<EventMap, Key>
): Subscription {
  const unsubscribe = _on(handlers, key, (value: EventMap[Key]) => {
    handler(value);
    unsubscribe();
  });
  return unsubscribe;
}

function _when<
  EventMap extends Record<string | symbol, unknown>,
  Key extends keyof EventMap
>(
  handlers: Handlers<EventMap>,
  ...keys: Key[]
): CancellablePromise<EventMap[Key]> {
  let _reject: (reason?: unknown) => void;
  const promise = new Promise<EventMap[Key]>((resolve, reject) => {
    _reject = reject;
    const unsubscribe = keys.map((key) => _on(handlers, key, next));

    function next(value: EventMap[Key]) {
      for (const cb of unsubscribe) {
        cb();
      }
      resolve(value);
    }
  });
  const result = promise as CancellablePromise<EventMap[Key]>;
  result.cancel = once(_reject!);
  return result;
}

function _off<
  EventMap extends Record<string | symbol, unknown>,
  Key extends keyof EventMap
>(handlers: Handlers<EventMap>, key: Key, handler?: Handler<EventMap, Key>) {
  if (handlers.hasOwnProperty(key)) {
    const list = handlers[key]!;
    if (handler !== undefined) {
      const idx = list.indexOf(handler);
      if (idx >= 0) {
        if (list.length > 1) {
          list.splice(idx, 1);
        } else {
          handlers[key] = [];
        }
      }
    } else {
      handlers[key] = [];
    }
  }
}

function _emit<
  EventMap extends Record<string | symbol, unknown>,
  Key extends keyof EventMap
>(handlers: Handlers<EventMap>, key: Key, payload: EventMap[Key]): boolean {
  if (!handlers.hasOwnProperty(key)) {
    return false;
  }
  const list = handlers[key]!;
  if (list.length === 0) {
    return false;
  }
  for (const handler of [...list]) {
    handler(payload);
  }
  return true;
}

async function _trigger<
  EventMap extends Record<string | symbol, unknown>,
  Key extends keyof EventMap
>(
  handlers: Handlers<EventMap>,
  key: Key,
  payload: EventMap[Key]
): Promise<boolean> {
  if (!handlers.hasOwnProperty(key)) {
    return false;
  }
  const list = handlers[key]!;
  if (list.length === 0) {
    return false;
  }
  for (const handler of [...list]) {
    const result = handler(payload);
    if (result instanceof Promise) {
      await result;
    }
  }
  return true;
}
