export enum ConditionType {
  STATIC = "static",
  CONJUNCTION = "conjunction",
  DISJUNCTION = "disjunction",
  DATA = "data",
}

export const CONDITION_TYPE_SET = new Set(Object.values(ConditionType));

export type Condition<T> =
  | ConditionStatic
  | ConditionNested<T>
  | ConditionData<T>;

export interface ConditionStatic {
  type: ConditionType.STATIC;
  value: boolean;
}

export interface ConditionNested<T> {
  type: ConditionType.CONJUNCTION | ConditionType.DISJUNCTION;
  children: Condition<T>[];
}

export interface ConditionData<T> {
  type: ConditionType.DATA;
  data: T;
}

export const CONDITION_TRUE: ConditionStatic = {
  type: ConditionType.STATIC,
  value: true,
};

export const CONDITION_FALSE: ConditionStatic = {
  type: ConditionType.STATIC,
  value: false,
};

export function matchAll<T>(conditions: Condition<T>[]): Condition<T> {
  if (conditions.length === 0) {
    return CONDITION_TRUE;
  }
  if (conditions.length === 1) {
    return conditions[0];
  }
  return { type: ConditionType.CONJUNCTION, children: conditions };
}

export function matchSome<T>(conditions: Condition<T>[]): Condition<T> {
  if (conditions.length === 0) {
    return CONDITION_FALSE;
  }
  if (conditions.length === 1) {
    return conditions[0];
  }
  return { type: ConditionType.DISJUNCTION, children: conditions };
}

export function matchIt<T>(data: T): ConditionData<T> {
  return { type: ConditionType.DATA, data };
}

export function evaluate<T>(
  evaluateData: (it: T) => boolean,
  condition: Condition<T>,
  _eval = (it: Condition<T>): boolean => evaluate(evaluateData, it, _eval)
): boolean {
  if (condition.type === ConditionType.STATIC) {
    return condition.value;
  }
  let result: boolean;
  switch (condition.type) {
    case ConditionType.CONJUNCTION:
      result = condition.children.every(_eval);
      break;
    case ConditionType.DISJUNCTION:
      result = condition.children.some(_eval);
      break;
    case ConditionType.DATA:
      result = evaluateData(condition.data);
      break;
  }
  return result;
}
export function transformCondition<T, U>(
  it: Condition<T>,
  iteratee: (item: T) => U,
  _exec = (it: Condition<T>) => transformCondition(it, iteratee)
): Condition<U> {
  switch (it.type) {
    case ConditionType.STATIC:
      return it;
    case ConditionType.DATA:
      return { type: ConditionType.DATA, data: iteratee(it.data) };
  }
  return { type: it.type, children: it.children.map(_exec) };
}

export function transformConditionUnwrap<T, U>(
  it: Condition<T>,
  iteratee: (item: T) => Condition<U>,
  _exec = (it: Condition<T>) => transformConditionUnwrap(it, iteratee)
): Condition<U> {
  switch (it.type) {
    case ConditionType.STATIC:
      return it;
    case ConditionType.DATA:
      return iteratee(it.data);
  }
  return { type: it.type, children: it.children.map(_exec) };
}
