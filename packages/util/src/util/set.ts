export const EMPTY_SET = new Set();

export function mergeSets<T>(target: Set<T>, ...sets: Set<T>[]): Set<T> {
  for (const set of sets) {
    for (const value of set.values()) {
      target.add(value);
    }
  }
  return target;
}

/**
 * A set wrapper that stringifies to a list of its values.
 */
export class StringifySet<T> {
  private readonly set: Set<T>;
  private cache?: T[];

  constructor(items?: Iterable<T>) {
    this.set = new Set<T>(items);
  }

  add(value: T) {
    this.cache = undefined;
    this.set.add(value);
  }

  has(value: T): boolean {
    return this.set.has(value);
  }

  delete(value: T): boolean {
    this.cache = undefined;
    return this.set.delete(value);
  }

  [Symbol.iterator]() {
    return this.set[Symbol.iterator]();
  }

  toJSON(): T[] {
    if (this.cache === undefined) {
      this.cache = Array.from(this.set);
    }
    return this.cache;
  }
}
