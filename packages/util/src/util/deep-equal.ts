import { Obj } from "@/util/types";

export function deepEqualList(a: unknown[], b: unknown[]): boolean {
  if (a.length !== b.length) {
    return false;
  }
  for (let i = 0; i < a.length; i++) {
    if (!deepEqual(a[i], b[i])) {
      return false;
    }
  }
  return true;
}

export function deepEqualObject(a: Obj, b: Obj): boolean {
  const aKeys = Object.keys(a as any);
  const bKeys = Object.keys(b as any);
  if (aKeys.length !== bKeys.length) {
    return false;
  }
  for (const key of aKeys) {
    if (!Reflect.has(b, key) || !deepEqual(a[key], b[key])) {
      return false;
    }
  }
  return true;
}

export function deepEqual(a: unknown, b: unknown): boolean {
  if (a === b) {
    return true;
  }
  if (a == null || typeof a !== typeof b || typeof a !== "object") {
    return false;
  }
  const aIsArray = Array.isArray(a);
  const bIsArray = Array.isArray(b);
  if (aIsArray !== bIsArray) {
    return false;
  }
  if (aIsArray) {
    return deepEqualList(a as unknown[], b as unknown[]);
  }
  return deepEqualObject(a as Obj, b as Obj);
}
