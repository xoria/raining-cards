[Base Repository](https://gitlab.com/xoria/raining-cards/)

# Raining Cards: Common

A collection of shared types and enums between the _raining.cards_ server-side
and client-side core.
