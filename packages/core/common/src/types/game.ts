export interface GameIds {
  actionSchema: "__ActionSchema__";
  actionSchemaGroup: "__ActionSchemaGroup__";
  actionCondition: "__ActionCondition__";

  material: "__Material__";
  materialFaceType: "__MaterialFaceType__";
  materialFaceTypeGroup: "__MaterialFaceTypeGroup__";

  zone: "__Zone__";
  zoneType: "__ZoneType__";
}

export type AnyGameIds = Record<keyof GameIds, string>;
