import { Condition, CountRange } from "@raining.cards/util";
import { AnyGameIds } from "@/types/game";
import { ElementType } from "@/types/element";

export interface ActionSchemaDTO<
  GI extends AnyGameIds,
  Origin extends ActorSchemaDTO<GI> = ActorSchemaDTO<GI>,
  Target extends ActorSchemaSingleDTO<GI> = ActorSchemaSingleDTO<GI>
> {
  id: GI["actionSchema"];
  name: string;
  description?: string;
  origin: Origin;
  originCondition?: Condition<GI["actionCondition"]>;
  target: Target;
  targetCondition?: Condition<GI["actionCondition"]>;
  condition?: Condition<GI["actionCondition"]>;
}

export type ActorSchemaSingleDTO<GI extends AnyGameIds> =
  | ActorSchemaGlobalDTO
  | ActorSchemaZoneDTO<GI>
  | ActorSchemaMaterialDTO<GI>;

export type ActorSchemaDTO<GI extends AnyGameIds> =
  | ActorSchemaSingleDTO<GI>
  | ActorSchemaMaterialsDTO<GI>;

export interface ActorSchemaDTOByType<GI extends AnyGameIds> {
  [ElementType.GLOBAL]: ActorSchemaGlobalDTO;
  [ElementType.ZONE]: ActorSchemaZoneDTO<GI>;
  [ElementType.MATERIAL]: ActorSchemaMaterialDTO<GI>;
  [ElementType.MATERIALS]: ActorSchemaMaterialsDTO<GI>;
}

export interface ActorSchemaGlobalDTO {
  type: ElementType.GLOBAL;
}

export interface ActorSchemaZoneDTO<GI extends AnyGameIds> {
  type: ElementType.ZONE;
  // restrict zone
  zone?: GI["zone"][];
  zoneType?: GI["zoneType"][];
}

export interface MaterialViewRestrictionDTO<GI extends AnyGameIds> {
  public?: GI["materialFaceType"][];
  publicGroups?: GI["materialFaceTypeGroup"][];
  secret?: GI["materialFaceType"][];
  secretGroups?: GI["materialFaceTypeGroup"][];
}

export type PositionRestrictionDTO<GI extends AnyGameIds> =
  | { zone?: never; zoneType: GI["zoneType"]; index?: number[] }
  | { zone: GI["zone"]; zoneType?: never; index?: number[] };

export interface ActorSchemaMaterialDTO<GI extends AnyGameIds> {
  type: ElementType.MATERIAL;
  // restrict position; negative indices count from top of pile
  position?: PositionRestrictionDTO<GI>[];
  // restrict material to any of the listed types
  materialView?: MaterialViewRestrictionDTO<GI>;
}

export interface ActorSchemaMaterialsDTO<GI extends AnyGameIds> {
  type: ElementType.MATERIALS;
  // restrict count
  count?: CountRange;
  // restrict position (e.g. [[A], [B, C]] means either materials within
  // position A or materials within positions B and C.
  position?: PositionRestrictionDTO<GI>[][];
  // restrict material to any of the listed types
  materialView?: MaterialViewRestrictionDTO<GI>;
}
