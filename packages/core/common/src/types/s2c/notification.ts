// Designed to be compatible with vue-toastification#TYPE
export enum S2CNotificationType {
  SUCCESS = "success",
  INFO = "info",
  WARNING = "warning",
  ERROR = "error",
}

export const S2C_NOTIFICATION_TYPES = Object.values(S2CNotificationType);
export const S2C_NOTIFICATION_TYPE_SET = new Set(S2C_NOTIFICATION_TYPES);

export type S2CMessageDTONotification = [
  /* timestamp */ number | null,
  S2CNotificationType,
  /* timeout */ number | false | null,
  /* message */ string,
  /* title */ string | null
];
