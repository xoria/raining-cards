export enum ServerCommand {
  LOBBY_CREATED = "lc",
  LOBBY_JOINED = "lj",
  LOBBY_RECONNECTED = "rc",
  LOBBY_TEAMS = "lt",

  GAME_OPTIONS = "go",
  GAME_META = "ga",
  GAME_LOG = "gl",

  UPDATE_KEYFRAME = "uk",
  UPDATE_DELTA = "ud",
  UPDATE_CLOSURE = "uc",

  ACTION_REJECT = "ar",

  ERROR = "ex",
  NOTIFICATION = "nx",
}
