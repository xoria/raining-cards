import { AnyGameIds } from "@/types";

export interface ActionRevisionDTO<GI extends AnyGameIds> {
  revision: number;
  groups: GI["actionSchemaGroup"][];
}

export interface ActionSchemaGroupDTO<GI extends AnyGameIds> {
  id: GI["actionSchemaGroup"];
  schemaIds: GI["actionSchema"][];
}
