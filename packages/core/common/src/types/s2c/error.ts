export enum S2CMessageErrorCode {
  LOBBY_NOT_FOUND = "l-",
  LOBBY_INVALID_SECRET = "lis",

  GENERIC_CLIENT = "gc",
  GENERIC_SERVER = "gs",
}

export const S2C_MESSAGE_ERROR_CODE_VALUES = Object.values(S2CMessageErrorCode);
export const S2C_MESSAGE_ERROR_CODE_SET = new Set(
  S2C_MESSAGE_ERROR_CODE_VALUES
);

export type S2CMessageDTOError =
  | [S2CMessageErrorCode]
  | [S2CMessageErrorCode, /* message */ string];
