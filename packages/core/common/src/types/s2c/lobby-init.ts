import { ClientDTO, ParticipantsDTO } from "@/types";

export type S2CMessageDTOLobbyInit = [LobbyDTO, ClientDTO];

export interface LobbyDTO {
  id: string;
  gameBoxId: string;
  options: unknown;
  creatorId: string;
  participants: ParticipantsDTO;
}
