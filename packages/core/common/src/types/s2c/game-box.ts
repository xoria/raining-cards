import { CountRange } from "@raining.cards/util";

export interface GameBoxDTO {
  id: string;
  name: string;
  description: string;
  lobby: GameSchemaDTO;
}

export interface GameSchemaDTO {
  props?: Record<string, GameSchemaDTOProp>;
  players: CountRange;
  teams?: TeamSchemaDTO[];
}

export interface GameSchemaDTOProp {
  type: "text" | "number" | "checkbox";
  default: string | number | boolean;
  label: string;
  description?: string;
  min?: number;
  max?: number;
}

export interface TeamSchemaDTO {
  id: string;
  name: string;
  color?: string;
  players: CountRange;
}
