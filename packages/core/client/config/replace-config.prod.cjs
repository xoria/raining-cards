/// Replaces   /* IF DEBUG */ [...] /* ELSE: [0] */              with [0]
/// Replaces   /* IF DEBUG */ [...] /* ELSE */ [0] /* ENDIF */   with [0]
/// Removes    /* IF DEBUG */ [...] /* ENDIF */

/// "IF DEBUG" can also be "region IF DEBUG"
/// "ENDIF" can also be "ENDIF endregion"

const ENDIF = `\\s*ENDIF(?: endregion)?\\s*\\*/`;
const ELSE_MATCH_INLINE = `:((?:.(?!\\*/))+.)\\*/`;
const ELSE_MATCH = `\\s*\\*/((?:.(?!/\\*${ENDIF}))*.)/\\*${ENDIF}`;
const ELSE = `(?:ELSE(?:${ELSE_MATCH}|${ELSE_MATCH_INLINE}))`;

module.exports = {
  from: new RegExp(
    `/\\*\\s*(?:region )?IF DEBUG(?:.(?!ELSE|ENDIF))*.(?:${ENDIF}|${ELSE})`,
    "gs"
  ),
  to: (_, ...rest) => rest.find((it) => it !== undefined),
};
