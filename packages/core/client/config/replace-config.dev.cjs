/// Replaces   /* IF DEBUG */ [0] /* ELSE */ [...] /* ENDIF */   with [0]

const ENDIF = `\\s*ENDIF\\s*\\*/`;
const ELSE = `ELSE(?:.(?!/\\*${ENDIF}))*./\\*${ENDIF}`;

module.exports = {
  from: new RegExp(
    `/\\*\\s*IF DEBUG\\s*\\*/((?:.(?!/\\*\\s*ELSE\\s*))*.)/\\*\\s*${ELSE}`,
    "gs"
  ),
  to: (_, ...rest) => rest.find((it) => it !== undefined),
};
