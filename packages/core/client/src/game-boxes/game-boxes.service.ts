import { UIGameBox } from "@/game/game.types";
import {
  uiCreateLobby,
  uiJoinLobby,
  UILobbyCreateOptions,
  UILobbyJoinOptions,
  UILobbyReconnectOptions,
  UILobbyState,
  uiReconnectToLobby,
} from "@/lobby/lobby.service";
import { assert } from "ts-essentials";
import { uiConnectWebSocket } from "@/lobby/socket.service";
import { fetchJSON } from "@/util/fetch";
import { AnyGameIds, GameIds } from "@raining.cards/common";

export interface UIGameBoxesState<GI extends AnyGameIds = GameIds> {
  gameBoxes: UIGameBox[];
  gameBoxById: (gameBoxId: UIGameBox["id"]) => UIGameBox;

  createLobby: (
    gameBox: UIGameBox,
    options: Omit<UILobbyCreateOptions, "gameBox">
  ) => Promise<UILobbyState<GI>>;
  joinLobby: (
    lobbyId: string,
    options: Omit<UILobbyJoinOptions, "lobbyId">
  ) => Promise<UILobbyState<GI>>;
  reconnectToLobby: (
    lobbyId: string,
    options: Omit<UILobbyReconnectOptions, "lobbyId">
  ) => Promise<UILobbyState<GI>>;
}

/**
 * Entry-point to start the raining.cards client state management.
 *
 * @param apiUrl The URL of the raining.cards server API. Must include a
 *        trailing slash.
 * @param wsUrl The URL of the raining.cards WS server.
 */
export async function uiFetchGameBoxes<GI extends AnyGameIds>(
  apiUrl: string,
  wsUrl = apiUrl.replace(/^http/, "ws")
): Promise<UIGameBoxesState<GI>> {
  const gameBoxes = await fetchJSON<UIGameBox[]>(`${apiUrl}games.json`);
  const gameBoxesMap = Object.fromEntries(gameBoxes.map((it) => [it.id, it]));

  const state: UIGameBoxesState<GI> = {
    gameBoxes,
    gameBoxById,

    createLobby,
    joinLobby,
    reconnectToLobby,
  };

  return state;

  function gameBoxById(id: UIGameBox["id"]): UIGameBox {
    assert(gameBoxesMap.hasOwnProperty(id), "Cannot find game box.");
    return gameBoxesMap[id];
  }

  async function createLobby(
    gameBox: UIGameBox,
    options: Omit<UILobbyCreateOptions, "gameBox">
  ): Promise<UILobbyState<GI>> {
    const socketState = await uiConnectWebSocket(state, wsUrl);
    return await uiCreateLobby(state, socketState, { ...options, gameBox });
  }

  async function joinLobby(
    lobbyId: string,
    options: Omit<UILobbyJoinOptions, "lobbyId">
  ): Promise<UILobbyState<GI>> {
    const socketState = await uiConnectWebSocket(state, wsUrl);
    return await uiJoinLobby(state, socketState, { ...options, lobbyId });
  }

  async function reconnectToLobby(
    lobbyId: string,
    options: Omit<UILobbyReconnectOptions, "lobbyId">
  ): Promise<UILobbyState<GI>> {
    const socketState = await uiConnectWebSocket(state, wsUrl);
    return await uiReconnectToLobby(state, socketState, {
      ...options,
      lobbyId,
    });
  }
}
