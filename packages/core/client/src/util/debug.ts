export const DEBUG = /* IF DEBUG */ true; /* ELSE: false; */

/* IF DEBUG */
export function debugClone<T>(it: T): T {
  return JSON.parse(JSON.stringify(it));
}
/* ELSE: export { identity as debugClone } from "@raining.cards/util" */
