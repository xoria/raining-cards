import { UIClient, UIGameBox, UILobby } from "@/game/game.types";
import {
  SocketMessageLowLevel,
  UISocketEventMap,
  UISocketState,
} from "@/lobby/socket.service";
import {
  Emitter,
  eventRace,
  Subscription,
  unsubscribeAll,
  withEmitter,
} from "@raining.cards/util";
import { uiCreateGame, UIGameState, UIGameStateRef } from "@/game/game.service";
import {
  AnyGameIds,
  ClientCommand,
  GameIds,
  S2CMessageDTOError,
  S2CMessageDTOGameMeta,
  S2CMessageDTOLobbyInit,
  S2CMessageDTOLobbyUpdate,
  S2CMessageDTONotification,
  ServerCommand,
} from "@raining.cards/common";
import { UIGameBoxesState } from "@/game-boxes/game-boxes.service";
import { c2sLobbyJoinToDTO } from "@/lobby/dto/c2s/lobby-join";
import { s2cLobbyCreatedFromDTO } from "@/lobby/dto/s2c/lobby-created";
import { s2cLobbyUpdateFromDTO } from "@/lobby/dto/s2c/lobby-update";
import { s2cGameMetaFromDTO } from "@/game/dto/s2c/game-meta";
import { c2sLobbyReconnectToDTO } from "@/lobby/dto/c2s/lobby-reconnect";
import { c2sLobbyOptionsToDTO } from "@/lobby/dto/c2s/lobby-options";
import { c2sLobbyCreateToDTO } from "@/lobby/dto/c2s/lobby-create";
import { c2sLobbyChangeTeamToDTO } from "@/lobby/dto/c2s/lobby-change-team";
import { debugClone } from "@/util/debug";
import { s2cErrorFromDTO, S2CMessageClientError } from "@/lobby/dto/s2c/error";
import {
  S2CMessageClientNotification,
  s2cNotificationFromDTO,
} from "@/lobby/dto/s2c/notification";

type UILobbyEventMap<GI extends AnyGameIds = GameIds> = {
  updated: UILobby;
  game: UIGameState<GI>;
  error: S2CMessageClientError;
  notification: S2CMessageClientNotification;
};

export interface UILobbyState<GI extends AnyGameIds = GameIds>
  extends Emitter<UILobbyEventMap<GI>> {
  self: UIClient;
  lobby: UILobby;

  changeTeam: (teamIndex: number | null) => void;
  setGameOptions: (data: Record<string, unknown>) => void;
  startGame: () => void;
  destroy: () => void;
}

export interface UILobbyCreateOptions {
  gameBox: UIGameBox;
  clientName: string;
}

export interface UILobbyJoinOptions {
  lobbyId: string;
  clientName: string;
}

export interface UILobbyReconnectOptions {
  lobbyId: string;
  secret: string;
}

export async function uiCreateLobby<GI extends AnyGameIds>(
  gameBoxesState: UIGameBoxesState<GI>,
  socketState: UISocketState<GI>,
  { clientName, gameBox }: UILobbyCreateOptions
): Promise<UILobbyState<GI>> {
  socketState.send(
    ClientCommand.LOBBY_CREATE,
    c2sLobbyCreateToDTO({ clientName, gameBoxId: gameBox.id })
  );
  const { payload } = await commandOrError(
    socketState,
    ServerCommand.LOBBY_CREATED
  );
  return initLobby(
    gameBoxesState,
    socketState,
    payload as S2CMessageDTOLobbyInit
  );
}

export async function uiJoinLobby<GI extends AnyGameIds>(
  gameBoxesState: UIGameBoxesState<GI>,
  socketState: UISocketState<GI>,
  options: UILobbyJoinOptions
): Promise<UILobbyState<GI>> {
  socketState.send(ClientCommand.LOBBY_JOIN, c2sLobbyJoinToDTO(options));
  const { payload } = await commandOrError(
    socketState,
    ServerCommand.LOBBY_JOINED
  );
  return initLobby(
    gameBoxesState,
    socketState,
    payload as S2CMessageDTOLobbyInit
  );
}

export async function uiReconnectToLobby<GI extends AnyGameIds>(
  gameBoxesState: UIGameBoxesState<GI>,
  socketState: UISocketState<GI>,
  options: UILobbyReconnectOptions
) {
  socketState.send(
    ClientCommand.LOBBY_RECONNECT,
    c2sLobbyReconnectToDTO(options)
  );
  const { payload } = await commandOrError(
    socketState,
    ServerCommand.LOBBY_RECONNECTED
  );
  return initLobby(
    gameBoxesState,
    socketState,
    payload as S2CMessageDTOLobbyInit
  );
}

async function commandOrError<GI extends AnyGameIds, Cmd extends ServerCommand>(
  socketState: UISocketState<GI>,
  cmd: Cmd
): Promise<UISocketEventMap[Cmd]> {
  const [result, event] = await eventRace(socketState, [
    cmd,
    ServerCommand.ERROR,
  ]);
  if (event === ServerCommand.ERROR) {
    throw s2cErrorFromDTO(result.payload as S2CMessageDTOError);
  }
  return result;
}

function initLobby<GI extends AnyGameIds>(
  gameBoxesState: UIGameBoxesState<GI>,
  socketState: UISocketState<GI>,
  payload: S2CMessageDTOLobbyInit
): UILobbyState<GI> {
  const { client: self, lobby } = s2cLobbyCreatedFromDTO(
    gameBoxesState,
    payload
  );

  const state = withEmitter<UILobbyState<GI>, UILobbyEventMap<GI>>({
    self,
    lobby,

    changeTeam,
    setGameOptions,
    startGame,
    destroy,
  });

  let subscriptions: Subscription[] = [
    socketState.on(ServerCommand.ERROR, (it) =>
      state.emit("error", s2cErrorFromDTO(it.payload as S2CMessageDTOError))
    ),
    socketState.on(ServerCommand.NOTIFICATION, (it) =>
      state.emit(
        "notification",
        s2cNotificationFromDTO(it.payload as S2CMessageDTONotification)
      )
    ),
    socketState.on(ServerCommand.GAME_OPTIONS, onLobbyUpdate),
    socketState.on(ServerCommand.LOBBY_TEAMS, onLobbyUpdate),
    socketState.on(ServerCommand.GAME_META, (it) =>
      onGameMeta(it).catch(console.error)
    ),
  ];
  const gameStateRef: UIGameStateRef<GI> = { gameState: undefined };

  return state;

  function onLobbyUpdate({ payload }: SocketMessageLowLevel) {
    const { lobby } = s2cLobbyUpdateFromDTO(
      gameBoxesState,
      payload as S2CMessageDTOLobbyUpdate
    );
    state.lobby = lobby;
    console.info("lobby:updated", debugClone(lobby));
    state.emit("updated", lobby);
  }

  async function onGameMeta({ payload }: SocketMessageLowLevel) {
    // clear listeners of potential previous games
    gameStateRef.gameState?.destroy();
    // create new game
    const gameState = (gameStateRef.gameState = await uiCreateGame(
      socketState,
      state,
      s2cGameMetaFromDTO(
        gameStateRef,
        state,
        payload as S2CMessageDTOGameMeta<GI>
      )
    ));
    console.info("lobby:game", debugClone(gameState));
    state.emit("game", gameState);
  }

  function changeTeam(teamIndex: number | null) {
    socketState.send(
      ClientCommand.LOBBY_CHANGE_TEAM,
      c2sLobbyChangeTeamToDTO({ teamIndex })
    );
  }

  function setGameOptions(data: Record<string, unknown>) {
    socketState.send(
      ClientCommand.GAME_OPTIONS,
      c2sLobbyOptionsToDTO({ data })
    );
  }

  function startGame() {
    socketState.send(ClientCommand.GAME_START);
  }

  function destroy() {
    gameStateRef.gameState?.destroy();
    gameStateRef.gameState = undefined;
    unsubscribeAll(subscriptions);
    subscriptions = [];
  }
}
