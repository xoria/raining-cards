import {
  S2C_MESSAGE_ERROR_CODE_SET,
  S2CMessageDTOError,
  S2CMessageErrorCode,
} from "@raining.cards/common";

export interface S2CMessageClientError {
  code: S2CMessageErrorCode;
  title: string;
  message?: string;
}

const ERROR_TITLE: Record<S2CMessageErrorCode, string> = {
  [S2CMessageErrorCode.GENERIC_SERVER]: "Internal server error",
  [S2CMessageErrorCode.GENERIC_CLIENT]: "Request failed",
  [S2CMessageErrorCode.LOBBY_NOT_FOUND]: "Lobby not found",
  [S2CMessageErrorCode.LOBBY_INVALID_SECRET]: "Invalid secret",
};

export function s2cErrorFromDTO([
  code,
  message,
]: S2CMessageDTOError): S2CMessageClientError {
  return { code, title: ERROR_TITLE[code], message };
}

export function isS2CMessageClientError(
  err: unknown
): err is S2CMessageClientError {
  if (err === null || typeof err !== "object") {
    return false;
  }
  if (!S2C_MESSAGE_ERROR_CODE_SET.has((err as any).code)) {
    return false;
  }
  if (typeof (err as any).title !== "string") {
    return false;
  }
  // noinspection RedundantIfStatementJS
  if (
    (err as any).message !== undefined &&
    typeof (err as any).message !== "string"
  ) {
    return false;
  }
  return true;
}
