import { UIClient, UILobby, UIParticipants } from "@/game/game.types";
import {
  AnyGameIds,
  ClientDTO,
  LobbyDTO,
  ParticipantsDTO,
  S2CMessageDTOLobbyInit,
  ServerCommand,
} from "@raining.cards/common";
import { UIGameBoxesState } from "@/game-boxes/game-boxes.service";
import { withLookup } from "@raining.cards/util";

export interface S2CMessageClientLobbyInit {
  cmd:
    | ServerCommand.LOBBY_CREATED
    | ServerCommand.LOBBY_JOINED
    | ServerCommand.LOBBY_RECONNECTED;
  lobby: UILobby;
  client: UIClient;
}

export function s2cLobbyInitFromDTO<GI extends AnyGameIds>(
  gameBoxesState: UIGameBoxesState<GI>,
  [lobby, client]: S2CMessageDTOLobbyInit
): Omit<S2CMessageClientLobbyInit, "cmd"> {
  return { lobby: lobbyFromDTO(gameBoxesState, lobby), client };
}

export function lobbyFromDTO<GI extends AnyGameIds>(
  gameBoxesState: UIGameBoxesState<GI>,
  dto: LobbyDTO
): UILobby {
  return {
    id: dto.id,
    creator: findClient(dto.participants, dto.creatorId),
    options: dto.options,
    participants: participantsFromDTO(dto.participants),
    gameBox: gameBoxesState.gameBoxById(dto.gameBoxId),
  };
}

function findClient(
  { players, spectators }: ParticipantsDTO,
  id: ClientDTO["id"]
): ClientDTO {
  return (players.find((it) => it.id === id) ??
    spectators.find((it) => it.id === id))!;
}

function participantsFromDTO(dto: ParticipantsDTO): UIParticipants {
  const players = withLookup(dto.players);
  return {
    players,
    spectators: dto.spectators,
    teams: dto.teams?.map((ids) => ids.map((id) => players.asLookup[id])),
  };
}
