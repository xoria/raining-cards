import { C2SMessageDTOLobbyReconnect } from "@raining.cards/common";

export interface C2SMessageClientLobbyReconnect {
  lobbyId: string;
  secret: string;
}

export function c2sLobbyReconnectToDTO({
  lobbyId,
  secret,
}: C2SMessageClientLobbyReconnect): string {
  const payload: C2SMessageDTOLobbyReconnect = [lobbyId, secret];
  return JSON.stringify(payload);
}
