import { C2SMessageDTOLobbyCreate, GameBoxDTO } from "@raining.cards/common";

export interface C2SMessageClientLobbyCreate {
  gameBoxId: GameBoxDTO["id"];
  clientName: string;
}

export function c2sLobbyCreateToDTO({
  gameBoxId,
  clientName,
}: C2SMessageClientLobbyCreate): string {
  const dto: C2SMessageDTOLobbyCreate = [gameBoxId, clientName];
  return JSON.stringify(dto);
}
