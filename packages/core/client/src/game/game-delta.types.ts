import { AnyGameIds, DeltaType, GameIds } from "@raining.cards/common";
import { UIZone } from "@/area/zone.types";
import { UIMaterialView } from "@/game/material";

export type UIGameDeltaStep<GI extends AnyGameIds = GameIds> =
  | UIGameDeltaCreate<GI>
  | UIGameDeltaDestroy<GI>
  | UIGameDeltaUpdate<GI>
  | UIGameDeltaFlush;

export interface UIGameDeltaCreate<GI extends AnyGameIds = GameIds> {
  type: DeltaType.CREATE;
  zone: UIZone<GI>;
  index: number;
  materialView: UIMaterialView<GI>;
}

export interface UIGameDeltaDestroy<GI extends AnyGameIds = GameIds> {
  type: DeltaType.DESTROY;
  zone: UIZone<GI>;
  index: number;
}

export interface UIGameDeltaUpdate<GI extends AnyGameIds = GameIds> {
  type: DeltaType.UPDATE;
  origin: { zone: UIZone<GI>; index: number };
  target?: { zone?: UIZone<GI>; index: number };
  newMaterialView?: UIMaterialView<GI>;
}

export interface UIGameDeltaFlush {
  type: DeltaType.FLUSH;
  delay?: number;
}
