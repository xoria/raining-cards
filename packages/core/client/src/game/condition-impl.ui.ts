import { UIMaterialPositionCount } from "@/game/game.types";
import { AnyGameIds } from "@raining.cards/common";
import { UIZone } from "@/area/zone.types";
import { UIGameStateRef } from "@/game/game.service";

// todo add proper type definition for actor conditions
export function uiEvaluateMaterialsOnZone<GI extends AnyGameIds>(
  zone: UIZone<GI>,
  { count: { min, max }, materialFaceTypes }: UIMaterialPositionCount<GI>
) {
  let count = 0;
  const noCap = max === Number.POSITIVE_INFINITY;
  for (const material of materialFaceTypes) {
    const value = zone.materialFaceTypeCount.get(material) ?? 0;
    if (value > 0) {
      count += value;
      if (noCap) {
        if (count >= min) {
          return true;
        }
      } else if (count > max) {
        return false;
      }
    }
  }
  return count >= min && count <= max;
}

export function uiEvaluateMaterials<GI extends AnyGameIds>(
  { gameState }: UIGameStateRef<GI>,
  {
    count: { min, max },
    materialFaceTypes,
    zoneTypes,
    zones: zoneIds,
  }: UIMaterialPositionCount<GI>
): boolean {
  const zonesByType = gameState!.game.zones.byType;
  let count = 0;
  const visited = new WeakSet<UIZone<GI>>();
  const zones = zoneIds
    .map((it) => gameState!.game.zones.asMap[it])
    .concat(zoneTypes.flatMap((it) => zonesByType[it.id]));
  const noCap = max === Number.POSITIVE_INFINITY;
  for (const zone of zones) {
    if (visited.has(zone)) {
      continue;
    }
    visited.add(zone);
    for (const material of materialFaceTypes) {
      const value = zone.materialFaceTypeCount.get(material) ?? 0;
      if (value > 0) {
        count += value;
        if (noCap) {
          if (count >= min) {
            return true;
          }
        } else if (count > max) {
          return false;
        }
      }
    }
  }
  return count >= min && count <= max;
}
