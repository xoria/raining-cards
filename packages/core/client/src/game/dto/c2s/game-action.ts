import {
  AnyGameIds,
  C2SMessageDTOGameAction,
  ClientActionDTO,
  GameIds,
} from "@raining.cards/common";

export interface C2SMessageClientGameAction<GI extends AnyGameIds = GameIds> {
  actionsRevision: number;
  action: ClientActionDTO<GI>;
}

export function c2sGameActionToDTO<GI extends AnyGameIds>({
  actionsRevision,
  action,
}: C2SMessageClientGameAction<GI>): string {
  const payload: C2SMessageDTOGameAction<GI> = [actionsRevision, action];
  return JSON.stringify(payload);
}
