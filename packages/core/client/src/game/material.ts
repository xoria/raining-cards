import { UIGameMeta, UIMaterialFaceType } from "@/game/game.types";
import { AnyGameIds, GameIds, MaterialViewDTO } from "@raining.cards/common";

export interface UIMaterialPositionInChunk<GI extends AnyGameIds = GameIds> {
  chunkIndex: number;
  chunk: UIMaterialChunk<GI>;
  index: number;
}

export interface UIMaterialFace<GI extends AnyGameIds = GameIds> {
  type: UIMaterialFaceType<GI>;
  customMeta?: Partial<Omit<UIMaterialFaceType<GI>, "id">>;
}

export interface UIMaterialView<GI extends AnyGameIds = GameIds> {
  global: UIMaterialFace<GI>;
  secret?: UIMaterialFace<GI>;
}

export interface UIMaterialChunk<GI extends AnyGameIds = GameIds>
  extends UIMaterialView<GI> {
  count: number;
  offset: number;
}

export function materialViewFromDTO<GI extends AnyGameIds = GameIds>(
  meta: UIGameMeta<GI>,
  { global, secret }: MaterialViewDTO<GI>
): UIMaterialView<GI> {
  return {
    global: {
      type: meta.materialFaceTypes[global.typeId],
      customMeta: global.customMeta,
    },
    secret: secret
      ? {
          type: meta.materialFaceTypes[secret.typeId],
          customMeta: secret.customMeta,
        }
      : undefined,
  };
}
