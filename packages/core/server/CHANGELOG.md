# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.3] - 2021-04-22

### Added

- Transaction allows you to use void action revisions (hence the old revision
  stays valid).
- `Transaction#setPlayerActions`, `Transaction#addBaseAction`
- Pass-through action handler return value within `Actions#perform` comfort API.

### Deprecated

- `Transaction#addAction` -- use ``Transaction#addBaseAction` instead (drop-in)

## [0.2.2] - 2021-04-15

### Fixed

- Transaction deltas in case of multiple operations on the same material within
  a single transaction.

### Added

- GameAbort client command
- Export `Lobby`
- Export `sendMetas`, `sendKeyframes` as well as `getMetaMessage` and
  `getKeyframeMessage` respectively.

## [0.2.1] - 2021-04-04

### Fixed

- Allow change-team message to join players if no teams exist.

### Added

- Enforce lobby player restrictions (within non-dev environments).
- `useActions` comfort API to handle ClientAction type casting for game devs.
- Enforce unique names for lobby clients (within non-dev environments).
- Do not close websockets on message parser/handler error. This has not been
  great behavior as it would open up the lobby for cleaning.

## [0.2.0] - 2021-04-01

### Changed

- Allow deeply nested zones instead; drop domains as a consequence.
- Use any number of material faces instead of just `back` and `front`. The index
  is used for reveal specification. Still, at most one global and one secret
  face can be available to any one client at the once.
- Streamline type definition (major rework, sorry for missing out on a migration
  guide).
- Use long environment names (development, production) instead of (dev, prod).
- Comfort: Replaced `Transaction` class with `useTransaction` factory.
- Comfort: Replaced `ActionStack` with more general `ExecutorStack`. The new
  implementation is not coupled to predefined factories anymore. It also uses
  the `use*` factory function instead of less flexible classes.

### Added

- Removed zonesMap from Game type: It was unnecessary to put this into game dev
  responsibility.
- Add meta override on a per material/zone basis.
- Comfort: `define*` -- This should allow for better game development
  experience. It a) provides a great overview of which elements are available
  and b) introduces devs more easily to the type definitions that they need.
- Comfort: `useGameLogs`, `useTurnOrder`
- Alias types to shadow any `DTO` types

## [0.1.0] - 2021-03-11

### Added

- Initial Release
