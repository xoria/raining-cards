import { once } from "@raining.cards/util";
import { GameBox } from "@/game/game.types";
import { GameIds } from "@raining.cards/common";
import { ServerOptions } from "@/server/server";
import { readPkgVersion } from "@/util";

export const STATIC_INFO = {
  version: {
    "@raining.cards/util": readPkgVersion("@raining.cards/util"),
    "@raining.cards/common": readPkgVersion("@raining.cards/common"),
    "@raining.cards/server": readPkgVersion("@raining.cards/server"),
  },
};

export const getInfo = once(({ gameBoxes, meta }: ServerOptions) => ({
  ...STATIC_INFO,
  meta,
  games: gameBoxes.map(gameBoxInfo),
}));

function gameBoxInfo({ id, meta }: GameBox<GameIds>) {
  if (typeof meta === "object" && meta !== null) {
    return { id, ...meta };
  }
  if (meta != null) {
    return { id, meta };
  }
  return { id };
}
