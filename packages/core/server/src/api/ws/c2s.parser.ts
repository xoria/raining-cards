import { assert } from "ts-essentials";
import * as WebSocket from "ws";
import {
  c2sLobbyCreateFromDTO,
  C2SMessageServerLobbyCreate,
} from "@/lobby/web-api/lobby-create.c2s";
import {
  c2sLobbyOptionsFromDTO,
  C2SMessageServerLobbyOptions,
} from "@/lobby/web-api/lobby-options.c2s";
import {
  c2sGameStartFromDTO,
  C2SMessageServerGameStart,
} from "@/game/web-api/game-start.c2s";
import {
  c2sGameActionFromDTO,
  C2SMessageServerGameAction,
} from "@/game/web-api/game-action.c2s";
import {
  c2sLobbyReconnectFromDTO,
  C2SMessageServerLobbyReconnect,
} from "@/lobby/web-api/lobby-reconnect.c2s";
import {
  c2sLobbyJoinFromDTO,
  C2SMessageServerLobbyJoin,
} from "@/lobby/web-api/lobby-join.c2s";
import {
  c2sLobbyChangeTeamFromDTO,
  C2SMessageServerLobbyChangeTeam,
} from "@/lobby/web-api/lobby-change-team.c2s";
import {
  AnyGameIds,
  CLIENT_COMMAND_SET,
  ClientCommand,
  GameIds,
} from "@raining.cards/common";
import { GameBoxMap } from "@/game/game.types";
import {
  c2sGameAbortFromDTO,
  C2SMessageServerGameAbort,
} from "@/game/web-api/game-abort.c2s";

export type C2SMessageServer<GI extends AnyGameIds, GameProps> =
  | C2SMessageServerLobbyCreate<GI>
  | C2SMessageServerLobbyJoin<GI, GameProps>
  | C2SMessageServerLobbyReconnect<GI, GameProps>
  | C2SMessageServerLobbyChangeTeam<GI>
  | C2SMessageServerLobbyOptions<GI>
  | C2SMessageServerGameStart<GI, GameProps>
  | C2SMessageServerGameAbort<GI>
  | C2SMessageServerGameAction<GI>;

const C2S_FROM_DTO: {
  [Key in ClientCommand]: (
    socket: WebSocket,
    data: unknown,
    gameBoxMap: GameBoxMap<GameIds>
  ) => C2SMessageServer<GameIds, unknown>;
} = {
  [ClientCommand.LOBBY_CREATE]: c2sLobbyCreateFromDTO,
  [ClientCommand.LOBBY_JOIN]: c2sLobbyJoinFromDTO,
  [ClientCommand.LOBBY_RECONNECT]: c2sLobbyReconnectFromDTO,
  [ClientCommand.LOBBY_CHANGE_TEAM]: c2sLobbyChangeTeamFromDTO,
  [ClientCommand.GAME_OPTIONS]: c2sLobbyOptionsFromDTO,

  [ClientCommand.GAME_START]: c2sGameStartFromDTO,
  [ClientCommand.GAME_ABORT]: c2sGameAbortFromDTO,
  [ClientCommand.GAME_ACTION]: c2sGameActionFromDTO,
};

export function parseClientCommand(
  msg: WebSocket.Data
): [ClientCommand, string | undefined] {
  assert(typeof msg === "string", "Message must be a string.");
  const cmd = msg.substr(0, 2);
  assert(CLIENT_COMMAND_SET.has(cmd as any), "Invalid command.");
  return [cmd as ClientCommand, msg.length > 2 ? msg.substr(3) : undefined];
}

export function c2sFromDTO<GI extends AnyGameIds, GameProps>(
  socket: WebSocket,
  cmd: ClientCommand | string,
  payload: string | undefined,
  gameBoxMap: GameBoxMap<GameIds>
): C2SMessageServer<GI, GameProps> {
  let data: unknown;
  try {
    data = payload !== undefined ? JSON.parse(payload) : undefined;
  } catch (_) {
    throw new Error("Failed to parse payload.");
  }
  assert(Reflect.has(C2S_FROM_DTO, cmd), "Invalid command.");
  return (C2S_FROM_DTO[cmd as ClientCommand](
    socket,
    data,
    gameBoxMap
  ) as unknown) as C2SMessageServer<GI, GameProps>;
}
