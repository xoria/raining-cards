// API

// Entry
export { startServer } from "./server/server";
export {
  Notification,
  MulticastOptions,
  assertNotification,
} from "./server/notification";
export { S2CNotificationType as NotificationType } from "@raining.cards/common";

// Utilities
export * from "./util";

// Game-Framework
export {
  sendMetas,
  getMetaMessage,
  sendKeyframes,
  getKeyframeMessage,
} from "./game/game";
export * from "./lobby/communication";
export * from "./comfort";

// Game-Framework utils
export { accessOrThrow } from "./game/material/material";
export { last, first } from "./game/material/material.position";

// Types

export * from "./game/action/client-action.types";
export * from "./game/action/action-schema.types";
export {
  ActionCondition,
  ActionConditionItem,
  matchCondition,
} from "./game/action/action-condition";
export * from "./game/game.types";
export * from "./game/zone/zone.types";
export { Lobby } from "./lobby/Lobby";
export * from "./lobby/participant.types";
export * from "./game/material/material.types";
