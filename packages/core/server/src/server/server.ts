import { createServer, Server } from "http";
import { ListenOptions } from "net";
import { useWebsocketServer } from "@/server/server.ws";
import { registerAPI } from "@/api/http/router";
import { GameBox } from "@/game/game.types";
import { AnyGameIds, GameIds } from "@raining.cards/common";
import * as WebSocket from "ws";
import { Subscription } from "@raining.cards/util";
import { MulticastOptions, Notification } from "@/server/notification";
import { debug, Environment, setEnvironment } from "@/util";
import { useStats } from "@/meta/stats";

export interface StartServerResult {
  server: Server;
  wsServer: WebSocket.Server;
  multicast: (
    notification: Notification,
    options?: MulticastOptions
  ) => Subscription;
}

export interface ServerOptions<GI extends AnyGameIds = GameIds> {
  http: ListenOptions;
  gameBoxes: GameBox<GI>[];
  origin?: Set<string>;
  meta?: unknown;
  environment?: Environment;
}

export function startServer(
  options: ServerOptions<AnyGameIds>
): StartServerResult {
  const _options = options as ServerOptions;

  if (_options.environment !== undefined) {
    // todo use server context instead of this syntax, multiple servers (just
    //      hypothetically) should be able to use different environments
    setEnvironment(_options.environment);
  }

  const stats = useStats(_options);
  const server = createServer();
  const { wsServer, multicast } = useWebsocketServer(server, _options, stats);

  registerAPI(server, _options, stats);
  server.listen(_options.http);

  server.once("listening", () => {
    debug("Server is listening", _options.http);
  });

  return { server, wsServer, multicast };
}
