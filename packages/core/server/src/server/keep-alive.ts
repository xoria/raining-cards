import * as WebSocket from "ws";
import { noop } from "ts-essentials";

const OUTSTANDING_PONG = new WeakSet<WebSocket>();
const PING_INTERVAL = 10000;
const CLIENTS = new Set<WebSocket>();

export function keepConnectionsAlive(wss: WebSocket.Server) {
  const interval = setInterval(check, PING_INTERVAL);
  check();

  wss.on("connection", (socket) => {
    CLIENTS.add(socket);
    const markAlive = () => OUTSTANDING_PONG.delete(socket);

    socket.on("pong", markAlive);
    socket.on("message", markAlive);
    socket.on("close", () => CLIENTS.delete(socket));
  });

  wss.on("close", () => clearInterval(interval));
}

function check() {
  for (const socket of CLIENTS) {
    if (OUTSTANDING_PONG.has(socket)) {
      socket.terminate();
      continue;
    }

    OUTSTANDING_PONG.add(socket);
    socket.ping(noop);
  }
}
