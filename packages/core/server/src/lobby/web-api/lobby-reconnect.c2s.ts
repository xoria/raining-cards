import * as WebSocket from "ws";
import { assertLobby, Lobby } from "@/lobby/Lobby";
import { assertNoContext } from "@/lobby/participant-context";
import { assert } from "ts-essentials";
import { AnyGameIds, ClientCommand, GameIds } from "@raining.cards/common";
import { assertArray } from "@raining.cards/util";

export interface C2SMessageServerLobbyReconnect<
  GI extends AnyGameIds,
  GameProps
> {
  cmd: ClientCommand.LOBBY_RECONNECT;
  lobby: Lobby<GI, GameProps>;
  secret: string;
}

export function c2sLobbyReconnectFromDTO(
  socket: WebSocket,
  data: unknown
): C2SMessageServerLobbyReconnect<GameIds, unknown> {
  assertNoContext(socket);
  const [lobbyId, secret] = assertArray(data);
  const lobby = assertLobby(lobbyId);
  assert(typeof secret === "string");
  return { cmd: ClientCommand.LOBBY_RECONNECT, lobby, secret };
}
