import * as WebSocket from "ws";
import { assert } from "ts-essentials";
import { assertContext, ClientContext } from "@/lobby/participant-context";
import { AnyGameIds, ClientCommand, GameIds } from "@raining.cards/common";

export interface C2SMessageServerLobbyOptions<GI extends AnyGameIds> {
  cmd: ClientCommand.GAME_OPTIONS;
  ctx: ClientContext<GI>;
  data: Record<string, unknown>;
}

export function c2sLobbyOptionsFromDTO(
  socket: WebSocket,
  data: unknown
): C2SMessageServerLobbyOptions<GameIds> {
  assert(typeof data === "object" && data !== null);
  const ctx = assertContext(socket);
  return {
    cmd: ClientCommand.GAME_OPTIONS,
    ctx,
    data: data as Record<string, unknown>,
  };
}
