import { send, sendDTO } from "@/lobby/communication";
import { Player } from "@/lobby/participant.types";
import { getKeyframeMessage, getMetaMessage } from "@/game/game";
import { C2SMessageServerLobbyReconnect } from "@/lobby/web-api/lobby-reconnect.c2s";
import { abortLobbyCleanup } from "@/lobby/Lobby";
import { findClient } from "@/lobby/participant";
import { createClientContext } from "@/lobby/participant-context";
import * as WebSocket from "ws";
import { assert } from "ts-essentials";
import { AnyGameIds, ServerCommand } from "@raining.cards/common";

export function c2sLobbyReconnectHandler<GI extends AnyGameIds, GameProps>(
  socket: WebSocket,
  { lobby, secret }: C2SMessageServerLobbyReconnect<GI, GameProps>
) {
  // TODO tidy-up spectators not having a secret anymore
  const client = findClient(
    lobby.participants,
    (it) => (it as { secret?: Player["secret"] }).secret === secret
  );
  assert(client, "Invalid secret.");
  client.socket = socket;
  abortLobbyCleanup(lobby);
  send(client, { cmd: ServerCommand.LOBBY_RECONNECTED, client, lobby });
  createClientContext(socket, lobby, client);
  if (lobby.game) {
    const isPlayer = lobby.participants.players.includes(client);
    send(client, getMetaMessage(lobby.game, client, isPlayer));
    send(client, getKeyframeMessage(lobby.game, client, isPlayer));
    const logs =
      typeof lobby.game.logs === "function"
        ? lobby.game.logs(client, isPlayer)
        : lobby.game.logs ?? [];
    for (const log of logs) {
      sendDTO(client, log);
    }
  }
}
