import * as WebSocket from "ws";
import { assert } from "ts-essentials";
import { assertContext, ClientContext } from "@/lobby/participant-context";
import { AnyGameIds, ClientCommand, GameIds } from "@raining.cards/common";

export interface C2SMessageServerLobbyChangeTeam<GI extends AnyGameIds> {
  cmd: ClientCommand.LOBBY_CHANGE_TEAM;
  ctx: ClientContext<GI>;
  teamIndex: number | null;
}

export function c2sLobbyChangeTeamFromDTO(
  socket: WebSocket,
  data: unknown
): C2SMessageServerLobbyChangeTeam<GameIds> {
  assert(typeof data === "number" || data == null);
  const ctx = assertContext(socket);
  assert(
    ctx.lobby.game === undefined,
    "Team change is not possible during gameplay."
  );
  return {
    cmd: ClientCommand.LOBBY_CHANGE_TEAM,
    ctx,
    teamIndex: typeof data === "number" ? data | 0 : null,
  };
}
