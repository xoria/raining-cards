import * as WebSocket from "ws";
import { assertLobby, Lobby } from "@/lobby/Lobby";
import { assertNoContext } from "@/lobby/participant-context";
import { assertClientName } from "@/lobby/participant";
import { AnyGameIds, ClientCommand, GameIds } from "@raining.cards/common";
import { assertArray } from "@raining.cards/util";
import { assert } from "ts-essentials";
import { isDev } from "@/util";

export interface C2SMessageServerLobbyJoin<GI extends AnyGameIds, GameProps> {
  cmd: ClientCommand.LOBBY_JOIN;
  lobby: Lobby<GI, GameProps>;
  clientName: string;
}

export function c2sLobbyJoinFromDTO(
  socket: WebSocket,
  data: unknown
): C2SMessageServerLobbyJoin<GameIds, unknown> {
  assertNoContext(socket);
  const [lobbyId, clientName] = assertArray(data);
  assertClientName(clientName);
  const lobby = assertLobby(lobbyId);
  if (!isDev()) {
    assertNoNameConflict(lobby, clientName);
  }
  return {
    cmd: ClientCommand.LOBBY_JOIN,
    lobby,
    clientName,
  };
}

function assertNoNameConflict<GI extends AnyGameIds>(
  { participants: { all } }: Lobby<GI, unknown>,
  name: string
) {
  assert(!all.some((it) => it.name === name), "Name is already in use.");
}
