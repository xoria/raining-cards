import { C2SMessageServerLobbyOptions } from "@/lobby/web-api/lobby-options.c2s";
import * as WebSocket from "ws";
import { assert } from "ts-essentials";
import { debug } from "@/util";
import { AnyGameIds } from "@raining.cards/common";

export function c2sLobbyOptionsHandler<GI extends AnyGameIds>(
  socket: WebSocket,
  { ctx: { lobby, client }, data }: C2SMessageServerLobbyOptions<GI>
) {
  assert(client === lobby.creator, "Not permitted.");
  debug("Received lobby options", data);
  throw new Error("Not yet implemented."); // todo implement
}
