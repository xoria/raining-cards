import * as WebSocket from "ws";
import { assertNoContext } from "@/lobby/participant-context";
import { assertClientName } from "@/lobby/participant";
import { AnyGameIds, ClientCommand, GameIds } from "@raining.cards/common";
import { assertArray, assertLookup } from "@raining.cards/util";
import { GameBox, GameBoxMap } from "@/game/game.types";

export interface C2SMessageServerLobbyCreate<GI extends AnyGameIds> {
  cmd: ClientCommand.LOBBY_CREATE;
  gameBox: GameBox<GI>;
  clientName: string;
}

export function c2sLobbyCreateFromDTO(
  socket: WebSocket,
  data: unknown,
  gameBoxMap: GameBoxMap<GameIds>
): C2SMessageServerLobbyCreate<GameIds> {
  assertNoContext(socket);
  const [gameBoxId, clientName] = assertArray(data);
  assertClientName(clientName);
  const gameBox = assertLookup(gameBoxMap, gameBoxId, "GameBox not found.");
  return { cmd: ClientCommand.LOBBY_CREATE, gameBox, clientName };
}
