import * as WebSocket from "ws";
import { Alphabet, idFactory, Environment } from "@/util";
import {
  Client,
  Participants,
  Player,
  Spectator,
} from "@/lobby/participant.types";
import { assert } from "ts-essentials";
import type { Lobby } from "@/lobby/Lobby";
import {
  AnyGameIds,
  ParticipantsDTO,
  PlayerDTO,
  SpectatorDTO,
} from "@raining.cards/common";
import { pluckId, Predicate } from "@raining.cards/util";

export function isActive(
  client: Client
): client is Client & { socket: WebSocket } {
  if (client.socket === null) {
    return false;
  }
  return client.socket.readyState === WebSocket.OPEN;
}

export function findClient(
  { players, spectators }: Participants,
  iteratee: Predicate<[Client]>
): Client | undefined {
  return players.find(iteratee) ?? spectators.find(iteratee);
}

// TODO use game-specific incrementing Id generator for player Ids and drop
//      Client#id; drop Id of spectator all together
const clientIdFactory = idFactory(
  "client",
  { alphabet: Alphabet.INTERNAL, size: 6 },
  // don't debug client Ids in test environment to avoid player name collision
  [Environment.DEVELOPMENT]
);

const secretFactory = idFactory(null, { alphabet: Alphabet.URL, size: 8 });

export function createClient(socket: WebSocket, name: string): Client {
  return {
    // todo color generator
    id: clientIdFactory(name),
    secret: secretFactory(),
    socket,
    name,
  };
}

export function assertClientName(name: unknown): asserts name is string {
  assert(typeof name === "string");
  assert(name.length > 0, "Alias cannot be empty.");
}

export function participantsToDTO<GI extends AnyGameIds, GameProps>({
  participants: { players, spectators, teams },
}: Lobby<GI, GameProps>): ParticipantsDTO {
  return {
    players: players.map(otherPlayerToDTO),
    spectators: spectators.map(otherSpectatorToDTO),
    teams: teamsToDTO(teams),
  };
}

function teamsToDTO(teams?: Player[][]): undefined | Player["id"][][] {
  return teams && teams.map((team) => team.map(pluckId));
}

function otherPlayerToDTO({ id, name, uiMeta }: Player): PlayerDTO {
  return { id, name, uiMeta };
}

function otherSpectatorToDTO({ id, name, uiMeta }: Spectator): SpectatorDTO {
  return { id, name, uiMeta };
}
