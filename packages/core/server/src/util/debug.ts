import { noop } from "ts-essentials";
import { Environment } from "./environment.types";

export let debug = noop as (...args: unknown[]) => void;
export let debugLazy = noop as (cb: () => unknown[]) => void;

export function onEnvUpdate(env: Environment) {
  if (env === Environment.PRODUCTION) {
    debug = noop;
    debugLazy = noop;
  } else {
    debug = (...args: unknown[]) => console.debug("[DEBUG]", ...args);
    debugLazy = (cb: () => unknown[]) => console.debug("[DEBUG]", ...cb());
  }
}
