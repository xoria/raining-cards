import { Environment } from "./environment.types";
import { onEnvUpdate } from "./debug";

let env = Environment.PRODUCTION;

onEnvUpdate(env);

export function getEnvironment(): Environment {
  return env;
}

export function isDebug(): boolean {
  return env !== Environment.PRODUCTION;
}

export function isDev(): boolean {
  return env === Environment.DEVELOPMENT;
}

export function setEnvironment(environment: Environment) {
  env = environment;
  onEnvUpdate(env);
}
