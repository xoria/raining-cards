import { Zone, ZoneType } from "@/game/zone/zone.types";
import { IdFactory } from "@/util";
import { AnyGameIds, GameIds } from "@raining.cards/common";
import { Reveal } from "@/game/material/reveal.types";

export interface DefinedZone<GI extends AnyGameIds = GameIds> extends Zone<GI> {
  type: ZoneType<GI>;
  toJSON: () => Zone<GI>;
}

export interface ZoneDefinition<GI extends AnyGameIds = GameIds> {
  id: GI["zoneType"];
  type: ZoneType<GI>;
  factory: (
    idf: GI["zone"] | IdFactory<GI["zone"]>,
    reveal?: Reveal,
    customMeta?: Zone<GI>["customMeta"],
    materials?: Zone<GI>["materials"]
  ) => DefinedZone<GI>;
}

export interface ZoneSingletonDefinition<GI extends AnyGameIds = GameIds>
  extends ZoneDefinition<GI> {
  instance: DefinedZone<GI>;
}

export function defineZone<GI extends AnyGameIds = GameIds>(
  idf: GI["zoneType"] | IdFactory<GI["zoneType"]>,
  typeData: Omit<Partial<ZoneType<GI>>, "id"> = {}
): ZoneDefinition<GI> {
  const id = typeof idf === "function" ? idf() : idf;
  const type: ZoneType<GI> = { id, ...typeData };
  return {
    id,
    type,
    factory: (idf, reveal = {}, customMeta, materials = []) => {
      const id = typeof idf === "function" ? idf() : idf;
      return {
        id,
        type,
        typeId: type.id,
        reveal,
        children: [],
        materials,
        customMeta,
        toJSON: zoneToJSON,
      };
    },
  };
}

export function defineZoneSingleton<GI extends AnyGameIds = GameIds>(
  idf: GI["zone"] | IdFactory<GI["zone"]>,
  reveal: Reveal = {},
  typeData: Omit<Partial<ZoneType<GI>>, "id"> = {},
  materials?: Zone<GI>["materials"]
): ZoneSingletonDefinition<GI> {
  const def = defineZone(
    (idf as unknown) as GI["zoneType"] | IdFactory<GI["zoneType"]>,
    typeData
  );
  const instance = def.factory(idf, reveal, undefined, materials);
  return {
    ...def,
    factory: () => instance,
    instance,
  };
}

function zoneToJSON<GI extends AnyGameIds>(this: DefinedZone<GI>): Zone<GI> {
  return {
    id: this.id,
    typeId: this.typeId,
    reveal: this.reveal,
    children: this.children,
    materials: this.materials,
    customMeta: this.customMeta,
  };
}
