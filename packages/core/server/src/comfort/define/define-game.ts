import { Game, GameResultGroup } from "@/game/game.types";
import { AnyGameIds, GameIds } from "@raining.cards/common";

export type DefinedGame<GI extends AnyGameIds = GameIds> = Game<GI>;

interface DefineGameOptions<GI extends AnyGameIds>
  extends Omit<Game<GI>, "run"> {
  main: () => Promise<void>;
  closure?: () => GameResultGroup | GameResultGroup[];
}

export function defineGame<GI extends AnyGameIds>(
  it: DefineGameOptions<GI>
): DefinedGame<GI> {
  return {
    ...it,

    async run() {
      await it.main();

      const result = it.closure?.() ?? [];
      return Array.isArray(result) ? result : [result];
    },
  };
}
