import { InjectionKey, RingBuffer } from "@raining.cards/util";
import { broadcast } from "@/lobby/communication";
import { ServerCommand } from "@raining.cards/common";
import { Participants } from "@/lobby/participant.types";

export type GameLogs = ReturnType<typeof useGameLogs>;

export const GAME_LOGS: InjectionKey<GameLogs> = { name: "logs" };

export function useGameLogs(participants: Participants, count = 5) {
  const buffer = new RingBuffer<ReturnType<typeof broadcast>>(count);

  return {
    add,
    toJSON: () => buffer.toJSON(),
    [Symbol.iterator]: buffer[Symbol.iterator].bind(buffer),
  };

  function add(message: string) {
    buffer.push(
      broadcast(participants, {
        cmd: ServerCommand.GAME_LOG,
        time: new Date(),
        message,
      })
    );
  }
}
