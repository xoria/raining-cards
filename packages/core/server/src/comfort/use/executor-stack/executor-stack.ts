import { assert } from "ts-essentials";
import { Predicate } from "@raining.cards/util";
import {
  Executor,
  ExecutorStackItem,
} from "@/comfort/use/executor-stack/ExecutorStackItem";

export type ExecutorStack = ReturnType<typeof useExecutorStack>;

export function useExecutorStack() {
  const stack: ExecutorStackItem[] = [];

  return {
    get size() {
      return stack.length;
    },

    peek,
    peekAssert,
    pop,
    popAssert,
    push,
    dropUntilExec,
  };

  function peek(): ExecutorStackItem | undefined {
    return stack[stack.length - 1];
  }

  function peekAssert(assertItem?: ExecutorStackItem): ExecutorStackItem {
    assert(stack.length > 0, "No stack item present.");
    const item = stack[stack.length - 1];
    if (assertItem !== undefined) {
      assert(assertItem === item, "Unexpected item on top of stack.");
    }
    return item;
  }

  function pop(): ExecutorStackItem | undefined {
    const item = peek();
    stack.pop();
    return item;
  }

  function popAssert(assertItem?: ExecutorStackItem): ExecutorStackItem {
    const item = peekAssert(assertItem);
    stack.pop();
    return item;
  }

  function push(
    it:
      | Executor
      | Executor[]
      | { executors: Executor[]; condition?: Predicate<[ExecutorStackItem]> }
      | ExecutorStackItem
  ): ExecutorStackItem {
    let item: ExecutorStackItem;
    if (typeof it === "function") {
      item = new ExecutorStackItem([it]);
    } else if (Array.isArray(it)) {
      item = new ExecutorStackItem(it);
    } else if (it instanceof ExecutorStackItem) {
      item = it;
    } else {
      item = new ExecutorStackItem(it.executors, it.condition);
    }
    stack.push(item);
    return item;
  }

  /**
   * Drops all items from the top of the stack of which the condition does not
   * hold true. The first item that holds true is executed and returned (not
   * dropped), if any.
   */
  function dropUntilExec(): ExecutorStackItem | undefined {
    while (stack.length > 0) {
      const item = peek()!;
      if (item.exec()) {
        return item;
      }
      pop();
    }
  }
}
