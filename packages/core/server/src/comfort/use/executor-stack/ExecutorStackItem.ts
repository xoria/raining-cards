import { Predicate } from "@raining.cards/util";

export type Executor = () => false | unknown;

export class ExecutorStackItem {
  constructor(
    private executors: Executor[],
    private readonly condition?: Predicate<[ExecutorStackItem]>
  ) {}

  exec(): boolean {
    if (this.condition?.(this) === false) {
      return false;
    }
    this.executors = this.executors.filter((it) => it() !== false);
    return true;
  }

  add(executors: Executor | Executor[]) {
    this.executors = this.executors.concat(
      Array.isArray(executors) ? executors : [executors]
    );
  }
}
