import { AnyGameIds, GameIds } from "@raining.cards/common";
import { Participants } from "@/lobby/participant.types";
import { useTransactionDirtyState } from "@/comfort/use/transaction/transaction-dirty-state";
import {
  TransactionClosure,
  useTransactionClosure,
} from "@/comfort/use/transaction/transaction-closure";
import {
  TransactionActions,
  useTransactionActions,
} from "@/comfort/use/transaction/transaction-actions";
import {
  TransactionCreate,
  useTransactionCreate,
} from "@/comfort/use/transaction/transaction-create";
import {
  TransactionDestroy,
  useTransactionDestroy,
} from "@/comfort/use/transaction/transaction-destroy";
import {
  TransactionMove,
  useTransactionMove,
} from "@/comfort/use/transaction/transaction-move";
import {
  TransactionReveal,
  useTransactionReveal,
} from "@/comfort/use/transaction/transaction-reveal";
import { InjectionKey } from "@raining.cards/util";
import { ActionSchemaGroup } from "@/game/action/action-schema.types";

export type Transaction<GI extends AnyGameIds = GameIds> = Omit<
  TransactionActions<GI>,
  "close"
> &
  TransactionClosure<GI> &
  TransactionCreate<GI> &
  TransactionDestroy<GI> &
  TransactionMove<GI> &
  TransactionReveal<GI>;

export const TRANSACTION: InjectionKey<Transaction> = { name: "transaction" };

export function useTransaction<GI extends AnyGameIds = GameIds>(
  participants: Participants,
  defaultBaseActions:
    | GI["actionSchemaGroup"][]
    | ActionSchemaGroup<GI>[]
    | null = []
): Transaction<GI> {
  const dirtyState = useTransactionDirtyState<GI>(participants);

  // actions
  const actions = useTransactionActions<GI>(participants, defaultBaseActions);
  const {
    setBaseActions,
    addBaseAction,
    addAction,
    setPlayerActions,
    addPlayerAction,
  } = actions;

  // modifications
  const { create } = useTransactionCreate(dirtyState);
  const { destroy, destroyAll } = useTransactionDestroy(dirtyState);
  const { shuffle, move, swap } = useTransactionMove(dirtyState);
  const {
    flipGlobal,
    revealGlobal,
    flipSecret,
    revealSecret,
    clearSecret,
  } = useTransactionReveal(dirtyState);

  // closure
  const { flush, close, pushUpdate } = useTransactionClosure<GI>(
    participants,
    dirtyState,
    actions
  );

  return {
    // actions
    setBaseActions,
    addBaseAction,
    addAction,
    setPlayerActions,
    addPlayerAction,
    // modifications: create / destroy
    create,
    destroy,
    destroyAll,
    // modifications: move
    shuffle,
    move,
    swap,
    // modifications: reveal
    flipGlobal,
    revealGlobal,
    flipSecret,
    revealSecret,
    clearSecret,
    // closure
    flush,
    close,
    pushUpdate,
  };
}
