import { ActionSchemaGroup } from "@/game/action/action-schema.types";
import { debug } from "@/util";
import { Participants, Player } from "@/lobby/participant.types";
import { AnyGameIds } from "@raining.cards/common";
import { StringifySet } from "@raining.cards/util";

export interface TransactionActions<GI extends AnyGameIds> {
  setBaseActions: (
    groups: GI["actionSchemaGroup"][] | ActionSchemaGroup<GI>[] | null
  ) => void;
  addBaseAction: (
    group: GI["actionSchemaGroup"] | ActionSchemaGroup<GI>
  ) => void;
  /**
   * @deprecated Use addBaseAction instead.
   */
  addAction: (group: GI["actionSchemaGroup"] | ActionSchemaGroup<GI>) => void;

  setPlayerActions: (
    player: Player,
    groups: GI["actionSchemaGroup"][] | ActionSchemaGroup<GI>[]
  ) => void;
  addPlayerAction: (
    player: Player,
    group: GI["actionSchemaGroup"] | ActionSchemaGroup<GI>
  ) => void;

  close: () => Map<Player, GI["actionSchemaGroup"][]>;
}

export function useTransactionActions<GI extends AnyGameIds>(
  participants: Participants,
  defaultBaseActions: GI["actionSchemaGroup"][] | ActionSchemaGroup<GI>[] | null
): TransactionActions<GI> {
  let playerActionSchemaGroups!: Map<
    Player,
    StringifySet<GI["actionSchemaGroup"]> | null
  >;
  let baseActionSchemaGroups!: StringifySet<GI["actionSchemaGroup"]> | null;

  resetActions();

  return {
    setBaseActions,
    addBaseAction: addAction,
    /**
     * @deprecated Use addBaseAction instead.
     */
    addAction,

    setPlayerActions,
    addPlayerAction,

    close,
  };

  function setBaseActions(
    groups: GI["actionSchemaGroup"][] | ActionSchemaGroup<GI>[] | null
  ) {
    if (groups === null) {
      debug("Transaction: Void base actions");
      baseActionSchemaGroups = null;
      return;
    }
    const groupIds = new StringifySet(groups.map(actionSchemaGroupToId));
    debug("Transaction: Set base action", groupIds);
    baseActionSchemaGroups = groupIds;
  }

  function addAction(group: GI["actionSchemaGroup"] | ActionSchemaGroup<GI>) {
    if (baseActionSchemaGroups === null) {
      baseActionSchemaGroups = new StringifySet<GI["actionSchemaGroup"]>();
    }
    const id = actionSchemaGroupToId(group);
    debug("Transaction: Add base action", id);
    baseActionSchemaGroups.add(id);
  }

  function setPlayerActions(
    player: Player,
    groups: GI["actionSchemaGroup"][] | ActionSchemaGroup<GI>[] | null
  ) {
    if (groups === null) {
      debug("Transaction: Void player actions", { player: player.name });
      playerActionSchemaGroups.set(player, null);
      return;
    }
    const groupIds = new StringifySet(groups.map(actionSchemaGroupToId));
    debug("Transaction: Set player actions", groupIds);
    playerActionSchemaGroups.set(player, groupIds);
  }

  function addPlayerAction(
    player: Player,
    group: GI["actionSchemaGroup"] | ActionSchemaGroup<GI>
  ) {
    const id = actionSchemaGroupToId(group);
    debug("Transaction: Add player action", { player: player.name, group: id });
    if (playerActionSchemaGroups.has(player)) {
      playerActionSchemaGroups.get(player)!.add(id);
    } else {
      const set = new StringifySet<GI["actionSchemaGroup"]>();
      set.add(id);
      playerActionSchemaGroups.set(player, set);
    }
  }

  function close(): Map<Player, GI["actionSchemaGroup"][]> {
    const result = new Map<Player, GI["actionSchemaGroup"][]>();
    for (const player of participants.players) {
      let playerGroups = playerActionSchemaGroups.get(player);
      if (playerGroups != null) {
        playerGroups = new StringifySet<GI["actionSchemaGroup"]>(playerGroups);
        if (baseActionSchemaGroups !== null) {
          for (const group of baseActionSchemaGroups) {
            playerGroups.add(group);
          }
        }
      } else if (playerGroups === null) {
        playerGroups = undefined;
      } else {
        playerGroups = baseActionSchemaGroups ?? undefined;
      }
      if (playerGroups !== undefined) {
        result.set(player, playerGroups.toJSON());
      }
    }

    resetActions();
    return result;
  }

  function resetActions() {
    playerActionSchemaGroups = new Map();
    baseActionSchemaGroups = defaultBaseActions
      ? new StringifySet(defaultBaseActions.map(actionSchemaGroupToId))
      : null;
  }
}

function actionSchemaGroupToId<GI extends AnyGameIds>(
  group: GI["actionSchemaGroup"] | ActionSchemaGroup<GI>
): GI["actionSchemaGroup"] {
  return typeof group === "string" ? group : group.id;
}
