import { AnyGameIds } from "@raining.cards/common";
import { Zone } from "@/game/zone/zone.types";
import { debug } from "@/util";
import {
  MaterialLocator,
  MaterialPosition,
  MaterialPositionDescriptor,
  safeMaterialPosition,
} from "@/game/material/material.types";
import { accessOrThrow } from "@/game/material/material";
import { positionFromInsertDescriptor } from "@/game/material/material.position";
import { TransactionDirtyState } from "@/comfort/use/transaction/transaction-dirty-state";
import { shuffle as shuffleList } from "@raining.cards/util";
import {
  bakeDeltaReveal,
  dropMaterial,
  insertMaterial,
} from "@/comfort/use/transaction/material.util";

export interface TransactionMove<GI extends AnyGameIds> {
  shuffle: (zone: Zone<GI>) => void;
  swap: (pos0: MaterialLocator<GI>, pos1: MaterialLocator<GI>) => void;
  move: (
    pos: MaterialLocator<GI>,
    dest: MaterialPositionDescriptor<GI>
  ) => MaterialPosition<GI>;
}

export function useTransactionMove<GI extends AnyGameIds>(
  dirtyState: TransactionDirtyState<GI>
): TransactionMove<GI> {
  return {
    shuffle,
    swap,
    move,
  };

  function shuffle(zone: Zone<GI>) {
    debug("Transaction: Shuffle", zone);
    shuffleList(zone.materials);
    // TODO create delta
  }

  function swap(pos0: MaterialLocator<GI>, pos1: MaterialLocator<GI>) {
    const { material: m0, position: p0 } = accessOrThrow(pos0);
    const { material: m1, position: p1 } = accessOrThrow(pos1);
    debug(
      "Transaction: Swap materials",
      { position: p0, material: m0 },
      { position: p1, material: m1 }
    );
    if (m0 === m1) {
      return;
    }
    const r0 = bakeDeltaReveal(m0, p0);
    const r1 = bakeDeltaReveal(m1, p1);
    p0.zone.materials[p0.index] = m1;
    p1.zone.materials[p1.index] = m0;
    const target = safeMaterialPosition({ zone: p1.zone, index: p1.index + 1 });
    dirtyState.add(p0, {
      material: m0,
      location: { origin: p0, target },
      reveal: r0,
    });
    dirtyState.add(p1, {
      material: m1,
      location: { origin: p1, target: { ...p0 } },
      reveal: r1,
    });
  }

  function move(
    pos: MaterialLocator<GI>,
    dest: MaterialPositionDescriptor<GI>
  ): MaterialPosition<GI> {
    const { material, position } = accessOrThrow(pos);
    const reveal = bakeDeltaReveal(material, position);
    debug("Transaction: Move material", { from: position, to: dest, material });
    const target = positionFromInsertDescriptor(dest);
    if (position.zone === dest.zone) {
      if (position.index === target.index) {
        return target;
      }
      if (position.index < target.index) {
        target.index--;
      }
    }
    dropMaterial(position);
    insertMaterial(material, target);
    dirtyState.add(position, {
      material,
      location: { origin: position, target },
      reveal,
    });
    return { ...target };
  }
}
