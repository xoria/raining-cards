import {
  AnyGameIds,
  DeltaType,
  MaterialUpdateDelta,
  MaterialUpdateDeltaUpdate,
  MaterialViewDTO,
} from "@raining.cards/common";
import { Material, MaterialPosition } from "@/game/material/material.types";
import { BakedReveal, Reveal } from "@/game/material/reveal.types";
import { Participants, Player } from "@/lobby/participant.types";
import {
  compactHistory,
  TransactionHistoryItem,
} from "@/comfort/use/transaction/compact-history.util";
import {
  getBakedMaterialView,
  getMaterialView,
} from "@/game/material/material";
import { Zone } from "@/game/zone/zone.types";

export interface TransactionFlush<GI extends AnyGameIds> {
  byPlayer: Map<Player, MaterialUpdateDelta<GI>[]>;
  spectators: MaterialUpdateDelta<GI>[];
}

export interface MaterialDeltaLocation<GI extends AnyGameIds> {
  origin: MaterialPosition<GI> | null;
  target: MaterialPosition<GI> | null;
}

export interface MaterialDeltaReveal {
  origin: BakedReveal;
  target: Reveal;
}

export interface DirtyTransactionHistoryItem<GI extends AnyGameIds> {
  material: Material<GI>;
  location?: MaterialDeltaLocation<GI>;
  reveal: MaterialDeltaReveal;
}

export interface TransactionDirtyState<GI extends AnyGameIds> {
  add: (
    originPosition: MaterialPosition<GI> | null,
    item: DirtyTransactionHistoryItem<GI>
  ) => void;
  flush: () => TransactionFlush<GI>;
}

export function useTransactionDirtyState<GI extends AnyGameIds>(
  participants: Participants
): TransactionDirtyState<GI> {
  /**
   * Note for development: positions within dirtyHistory must be unique.
   * Multiple references of the same position would cause wrong compact history
   * calculation since a move-ahead is done in-place. For the same reason, the
   * {@link originPositionMap} of some material can only be used as said material
   * origin location within history items; not as target location for example.
   */
  let dirtyHistory: DirtyTransactionHistoryItem<GI>[] = [];
  let originPositionMap = new Map<Material<GI>, MaterialPosition<GI> | null>();

  return {
    add,
    flush,
  };

  function add(
    originPosition: MaterialPosition<GI> | null,
    item: DirtyTransactionHistoryItem<GI>
  ) {
    if (!originPositionMap.has(item.material)) {
      originPositionMap.set(item.material, originPosition);
    }
    dirtyHistory.push(item);
  }

  function flush(): TransactionFlush<GI> {
    const target: TransactionFlush<GI> = {
      spectators: [],
      byPlayer: new Map(),
    };

    for (const player of participants.players) {
      if (!target.byPlayer.has(player)) {
        target.byPlayer.set(player, []);
      }
    }

    const history = compactHistory(dirtyHistory, originPositionMap);
    for (const delta of history) {
      flushMaterial(target, participants.players, delta);
    }

    dirtyHistory = [];
    originPositionMap = new Map();

    return target;
  }
}

function flushMaterial<GI extends AnyGameIds>(
  target: TransactionFlush<GI>,
  players: Player[],
  historyDelta: TransactionHistoryItem<GI>
) {
  for (const player of players) {
    const playerDeltas = target.byPlayer.get(player)!;
    const delta = createUpdateDelta(player, historyDelta);
    if (delta !== undefined) {
      playerDeltas.push(delta);
    }
  }
  const delta = createUpdateDelta(null, historyDelta);
  if (delta !== undefined) {
    target.spectators.push(delta);
  }
}

function createUpdateDelta<GI extends AnyGameIds>(
  player: Player | null,
  { material, location: { origin, target }, reveal }: TransactionHistoryItem<GI>
): MaterialUpdateDelta<GI> | undefined {
  if (origin === null) {
    if (target === null) {
      return undefined;
    }
    return {
      type: DeltaType.CREATE,
      targetZoneId: target.zone.id,
      targetIndex: target.index,
      targetView: getMaterialView(player, material, target.zone),
    };
  }

  if (target === null) {
    return {
      type: DeltaType.DESTROY,
      originZoneId: origin.zone.id,
      originIndex: origin.index,
    };
  }

  const delta: MaterialUpdateDeltaUpdate<GI> = {
    type: DeltaType.UPDATE,
    originZoneId: origin.zone.id,
    originIndex: origin.index,
  };

  let revealChanged = false;
  // since target type could change due to zone change only, do not restrict
  // the check to `reveal !== undefined`.
  const targetView = getTargetMaterialView(
    player,
    material,
    reveal.origin,
    material,
    origin.zone,
    target.zone
  );
  if (targetView !== undefined) {
    delta.targetView = targetView;
    revealChanged = true;
  }

  let locationChanged = false;
  if (target.zone !== origin.zone || target.index !== origin.index) {
    locationChanged = true;
    delta.targetZoneId = target.zone.id;
    delta.targetIndex = target.index;
  }

  return revealChanged || locationChanged ? delta : undefined;
}

function getTargetMaterialView<GI extends AnyGameIds>(
  player: Player | null,
  originMaterial: Material<GI>,
  originReveal: BakedReveal,
  targetMaterial: Material<GI>,
  originZone: Zone<GI>,
  targetZone: Zone<GI>
): MaterialViewDTO<GI> | undefined {
  const originView = getBakedMaterialView(player, originMaterial, originReveal);
  const targetView = getMaterialView(player, targetMaterial, targetZone);
  return originView.secret !== targetView.secret ||
    originView.global !== targetView.global
    ? targetView
    : undefined;
}
