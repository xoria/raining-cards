import {
  TransactionFlush,
  TransactionDirtyState,
} from "@/comfort/use/transaction/transaction-dirty-state";
import {
  AnyGameIds,
  DeltaType,
  MaterialUpdateDeltaFlush,
} from "@raining.cards/common";
import { Participants, Player } from "@/lobby/participant.types";
import { debug } from "@/util";
import { TransactionActions } from "@/comfort/use/transaction/transaction-actions";
import { ClientAction } from "@/game/action/client-action.types";
import {
  pushMultiUpdate,
  PushUpdateDelta,
  PushUpdateOptions,
  PushUpdateType,
} from "@/comfort/push";

export interface TransactionUpdate<GI extends AnyGameIds> {
  playerUpdates: Map<Player, PushUpdateDelta<GI>>;
  spectatorUpdate: PushUpdateDelta<GI>;
}

export interface TransactionClosure<GI extends AnyGameIds> {
  flush: (delay?: number) => void;
  close: () => TransactionUpdate<GI>;
  pushUpdate: () => Promise<ClientAction<GI>>;
}

export function useTransactionClosure<GI extends AnyGameIds>(
  participants: Participants,
  dirtyState: TransactionDirtyState<GI>,
  actions: TransactionActions<GI>
): TransactionClosure<GI> {
  let history: TransactionFlush<GI>[] = [];

  return { flush, close, pushUpdate };

  function flush(delay?: number) {
    debug("Transaction: Flush", delay);
    const deltas = dirtyState.flush();

    if (delay !== 0) {
      addFlushDelta(deltas, participants.players, delay);
    }

    history.push(deltas);
  }

  function close(): TransactionUpdate<GI> {
    history.push(dirtyState.flush());
    const playerActions = actions.close();

    const playerUpdates = new Map<Player, PushUpdateDelta<GI>>();
    for (const player of participants.players) {
      const steps = history.flatMap((it) => it.byPlayer.get(player)!);
      playerUpdates.set(player, {
        type: PushUpdateType.DELTA,
        actionSchemaGroups: playerActions.get(player),
        steps,
      });
    }

    const spectatorUpdate: PushUpdateDelta<GI> = {
      type: PushUpdateType.DELTA,
      steps: history.flatMap((it) => it.spectators),
    };

    history = [];

    return { playerUpdates, spectatorUpdate };
  }

  function pushUpdate(options?: Partial<PushUpdateOptions>) {
    return pushMultiUpdate(participants, close(), options);
  }
}

function addFlushDelta<GI extends AnyGameIds>(
  target: TransactionFlush<GI>,
  players: Player[],
  delay?: number
) {
  const flushDelta: MaterialUpdateDeltaFlush = {
    type: DeltaType.FLUSH,
    delay,
  };
  for (const player of players) {
    target.byPlayer.get(player)!.push(flushDelta);
  }
  if (target.spectators.length > 0) {
    target.spectators.push(flushDelta);
  }
}
