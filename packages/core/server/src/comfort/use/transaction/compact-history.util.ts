import {
  DirtyTransactionHistoryItem,
  MaterialDeltaLocation,
} from "@/comfort/use/transaction/transaction-dirty-state";
import { Material, MaterialPosition } from "@/game/material/material.types";
import { AnyGameIds } from "@raining.cards/common";

export interface TransactionHistoryItem<GI extends AnyGameIds>
  extends DirtyTransactionHistoryItem<GI> {
  location: MaterialDeltaLocation<GI>;
}

/**
 * Given some history, return a new list of deltas with the same effect. The new
 * list contains only one delta per affected material.
 *
 * Side-effect: Some deltas are modified in-place, invalidating the passed list.
 *
 * @param deltas The original list of deltas.
 * @param originPosition A map of materials to their origin position.
 */
export function compactHistory<GI extends AnyGameIds>(
  deltas: DirtyTransactionHistoryItem<GI>[],
  originPosition: Map<Material<GI>, MaterialPosition<GI> | null>
): TransactionHistoryItem<GI>[] {
  const map = new Map<Material<GI>, TransactionHistoryItem<GI>>();
  const set: TransactionHistoryItem<GI>[] = [];
  for (let i = 0; i < deltas.length; i++) {
    const dirtyDelta = deltas[i];
    const { material } = dirtyDelta;
    if (map.has(material)) {
      const targetDelta = map.get(material)!;
      if (dirtyDelta.location !== undefined) {
        for (let j = set.length - 1; true; j--) {
          const item = set[j];
          if (item === targetDelta) {
            break;
          }
          if (item.location === undefined) {
            continue;
          }
          pullDeltaAhead(item.location, dirtyDelta.location);
        }
      }
      mergeDeltas(targetDelta, dirtyDelta);
    } else {
      const delta = tidyUpDelta(
        dirtyDelta,
        originPosition.get(material) as MaterialPosition<GI> | null
      );
      map.set(material, delta);
      set.push(delta);
    }
  }
  return set;
}

function tidyUpDelta<GI extends AnyGameIds>(
  dirtyDelta: DirtyTransactionHistoryItem<GI>,
  originPosition: MaterialPosition<GI> | null
): TransactionHistoryItem<GI> {
  const origin =
    dirtyDelta.location !== undefined
      ? dirtyDelta.location.origin
      : originPosition;
  const target =
    dirtyDelta.location !== undefined
      ? dirtyDelta.location.target
      : origin
      ? { ...origin }
      : null;
  return { ...dirtyDelta, location: { origin, target } };
}

/**
 * Given two in-order deltas A and B, this function modifies them in-place such
 * that B is supposed to happen before A.
 */
function pullDeltaAhead<GI extends AnyGameIds>(
  a: MaterialDeltaLocation<GI>,
  b: MaterialDeltaLocation<GI>
) {
  if (b.origin !== null) {
    if (a.origin !== null) {
      shiftIndicesIfAtop(a.origin, b.origin, -1);
    }
    if (a.target !== null) {
      shiftIndicesIfAtop(a.target, b.origin, -1);
    }
  }
  if (b.target !== null) {
    if (a.origin !== null) {
      shiftIndicesIfAtop(a.origin, b.target, 1);
    }
    if (a.target !== null) {
      shiftIndicesIfAtop(a.target, b.target, 1);
    }
  }
}

function shiftIndicesIfAtop<GI extends AnyGameIds>(
  a: MaterialPosition<GI>,
  b: MaterialPosition<GI>,
  step: number
) {
  if (a.zone === b.zone) {
    if (a.index >= b.index) {
      a.index += step;
    } else {
      b.index -= step;
    }
  }
}

/**
 * Merges source into target so that target reflects the two original deltas in
 * one.
 */
function mergeDeltas<GI extends AnyGameIds>(
  target: TransactionHistoryItem<GI>,
  source: DirtyTransactionHistoryItem<GI>
) {
  if (source.location !== undefined) {
    target.location.target = source.location.target;
  }
  if (source.reveal !== undefined) {
    if (target.reveal === undefined) {
      target.reveal = source.reveal;
    } else {
      target.reveal.target = source.reveal.target;
    }
  }
}
