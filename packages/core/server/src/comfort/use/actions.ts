import { AnyGameIds, GameIds } from "@raining.cards/common";
import { DefinedAction, LooseDefinedAction } from "@/comfort";
import { ClientAction } from "@/game/action/client-action.types";
import { fromEntries } from "@raining.cards/util";

export type Actions = ReturnType<typeof useActions>;

type SomeDefinedAction<GI extends AnyGameIds> =
  | LooseDefinedAction<GI, ClientAction<GI>>
  | DefinedAction<GI>;

type SomeHandler<GI extends AnyGameIds> =
  | SomeDefinedAction<GI>
  | ((action: ClientAction<GI>) => void | Promise<void>);

export function useActions<GI extends AnyGameIds = GameIds>(
  handlers:
    | Partial<Record<GI["actionSchema"], SomeHandler<GI>>>
    | SomeDefinedAction<GI>[]
) {
  const byId = Array.isArray(handlers)
    ? fromEntries(handlers.map((it) => [it.schema.id, it]))
    : handlers;

  return { perform, register };

  async function perform(action: ClientAction<GI>): Promise<unknown> {
    const it: SomeHandler<GI> = byId[action.schema.id]!;
    let result: unknown;
    if (isDefinedAction(it)) {
      result = it.handler(action);
    } else {
      result = it(action);
    }
    return result instanceof Promise ? await result : result;
  }

  function register(actionDefs: SomeDefinedAction<GI>[]) {
    Object.assign(
      byId,
      fromEntries(actionDefs.map((it) => [it.schema.id, it]))
    );
  }
}

function isDefinedAction<GI extends AnyGameIds>(
  it: SomeHandler<GI>
): it is LooseDefinedAction<GI> {
  return typeof it !== "function";
}
