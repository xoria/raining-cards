import { ClientAction } from "@/game/action/client-action.types";
import { nextClientAction, send } from "@/lobby/communication";
import { Participants, Player, Spectator } from "@/lobby/participant.types";
import { PushUpdateDelta } from "@/comfort/push/push-update.dto";
import {
  ActionRevisionDTO,
  AnyGameIds,
  ServerCommand,
} from "@raining.cards/common";
import { setNextActionRevision } from "@/game/action/action-revision";
import { TransactionUpdate } from "@/comfort/use/transaction/transaction-closure";

export interface PushUpdateOptions {
  /**
   * Amount of milliseconds before player may perform an action.
   */
  tilt: number;

  /**
   * Drop pending actions that are triggered before the client receives the
   * update.
   */
  dropInFlight: boolean;
}

const PUSH_UPDATE_OPTIONS: PushUpdateOptions = {
  tilt: 250,
  dropInFlight: false,
};

export async function pushUpdate<GI extends AnyGameIds>(
  client: Player | Spectator,
  update: PushUpdateDelta<GI>,
  options: Partial<PushUpdateOptions> = {}
): Promise<ClientAction<GI>> {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const opts = { ...PUSH_UPDATE_OPTIONS, ...options };
  // TODO use options
  let nextActionRevision: ActionRevisionDTO<GI> | undefined = undefined;
  if (update.actionSchemaGroups !== undefined) {
    nextActionRevision = setNextActionRevision(
      client,
      update.actionSchemaGroups
    );
  }
  // TODO skip, if update has no steps and actions have not changed
  send(client, {
    cmd: ServerCommand.UPDATE_DELTA,
    actionRevision: nextActionRevision,
    data: update,
  });
  return await nextClientAction<GI>(client);
}

// todo skip noop updates to single participants
export async function pushMultiUpdate<GI extends AnyGameIds>(
  participants: Participants,
  { playerUpdates, spectatorUpdate }: TransactionUpdate<GI>,
  options?: Partial<PushUpdateOptions>
): Promise<ClientAction<GI>> {
  const { players, spectators } = participants;
  const playerPromises = players
    .filter((player) => playerUpdates.has(player))
    .map(
      async (player) =>
        await pushUpdate(player, playerUpdates.get(player)!, options)
    );
  const spectatorPromises = spectators.map(
    async (spectator) => await pushUpdate(spectator, spectatorUpdate, options)
  );
  return await Promise.race(playerPromises.concat(spectatorPromises));
}
