import { GameResultGroup } from "@/game/game.types";
import { PushUpdateType } from "@/comfort/push/push-update.dto";

interface PushUpdateBase {
  type: PushUpdateType;
}

export interface PushUpdateClosure extends PushUpdateBase {
  type: PushUpdateType.CLOSURE;
  endTime: Date;
  gameResultGroups: GameResultGroup[];
}
