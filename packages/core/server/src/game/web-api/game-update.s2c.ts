import {
  ActionRevisionDTO,
  AnyGameIds,
  GameResultGroupDTO,
  S2CMessageDTOUpdateClosure,
  S2CMessageDTOUpdateDelta,
  S2CMessageDTOUpdateKeyframe,
  ServerCommand,
} from "@raining.cards/common";
import {
  PushUpdateDelta,
  PushUpdateKeyframe,
} from "@/comfort/push/push-update.dto";
import { PushUpdateClosure } from "@/comfort/push/push-update.types";
import { GameResultGroup } from "@/game/game.types";
import { zoneWithMaterialChunksToDTO } from "@/game/zone/zone.dto";

export interface S2CMessageServerUpdateKeyframe<GI extends AnyGameIds> {
  cmd: ServerCommand.UPDATE_KEYFRAME;
  actionRevision?: ActionRevisionDTO<GI>;
  data: PushUpdateKeyframe<GI>;
}

export function s2cGameUpdateKeyframeToDTO<GI extends AnyGameIds>({
  actionRevision,
  data: { state },
}: S2CMessageServerUpdateKeyframe<GI>): S2CMessageDTOUpdateKeyframe<GI> {
  return [actionRevision ?? null, state.map(zoneWithMaterialChunksToDTO)];
}

export interface S2CMessageServerUpdateDelta<GI extends AnyGameIds> {
  cmd: ServerCommand.UPDATE_DELTA;
  actionRevision?: ActionRevisionDTO<GI>;
  data: PushUpdateDelta<GI>;
}

export function s2cGameUpdateDeltaToDTO<GI extends AnyGameIds>({
  actionRevision,
  data: { steps },
}: S2CMessageServerUpdateDelta<GI>): S2CMessageDTOUpdateDelta<GI> {
  return [actionRevision ?? null, steps];
}

export interface S2CMessageServerUpdateClosure {
  cmd: ServerCommand.UPDATE_CLOSURE;
  data: PushUpdateClosure;
}

export function s2cGameUpdateClosureToDTO({
  data: { endTime, gameResultGroups },
}: S2CMessageServerUpdateClosure): S2CMessageDTOUpdateClosure {
  return [endTime.getTime(), gameResultGroups.map(gameResultGroupToDTO)];
}

function gameResultGroupToDTO(group: GameResultGroup): GameResultGroupDTO {
  return {
    ...group,
    stats: group.stats.map((it) => ({ text: it.text, playerId: it.player.id })),
  };
}
