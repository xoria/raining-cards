import { assertContext, ClientContext } from "@/lobby/participant-context";
import { ClientAction } from "@/game/action/client-action.types";
import * as WebSocket from "ws";
import { assert } from "ts-essentials";
import { assertClientAction } from "@/game/action/client-action.dto";
import { clientActionFromDTO } from "@/game/action/client-action.converter";
import { AnyGameIds, ClientCommand, GameIds } from "@raining.cards/common";
import { assertArray } from "@raining.cards/util";
import { getLatestActionRevision } from "@/game/action/action-revision";

export interface C2SMessageServerGameAction<GI extends AnyGameIds> {
  cmd: ClientCommand.GAME_ACTION;
  ctx: ClientContext<GI>;
  actionsRevision: number;
  action: ClientAction<GI>;
}

export function c2sGameActionFromDTO(
  socket: WebSocket,
  data: unknown
): C2SMessageServerGameAction<GameIds> {
  const ctx = assertContext(socket);
  const [actionsRevision, action] = assertArray(data);
  assert(typeof actionsRevision === "number");
  const latestPlayerActionGroups = getLatestActionRevision(ctx.client);
  assert(
    actionsRevision === latestPlayerActionGroups.revision,
    "Outdated action revision."
  );
  assertClientAction(action);
  return {
    cmd: ClientCommand.GAME_ACTION,
    ctx,
    actionsRevision,
    action: clientActionFromDTO(action, ctx),
  };
}
