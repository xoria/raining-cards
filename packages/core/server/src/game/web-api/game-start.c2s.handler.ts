import { sendClosure, sendKeyframes, sendMetas } from "@/game/game";
import { C2SMessageServerGameStart } from "@/game/web-api/game-start.c2s";
import * as WebSocket from "ws";
import { Lobby, LOBBY_BY_GAME } from "@/lobby/Lobby";
import { assert } from "ts-essentials";
import { STATS_CONTEXT } from "@/meta/stats";
import { AnyGameIds, GameIds } from "@raining.cards/common";
import { Game } from "@/game/game.types";

export function c2sGameStartHandler<GI extends AnyGameIds, GameProps>(
  socket: WebSocket,
  { ctx: { lobby } }: C2SMessageServerGameStart<GI, GameProps>
) {
  assert(socket === lobby.creator.socket, "Not permitted.");
  const stats = STATS_CONTEXT.get(socket)!;
  const { gameBox } = lobby;
  // todo use game props
  const game = (lobby.game = gameBox.factory(
    lobby,
    (undefined as unknown) as GameProps
  ));
  LOBBY_BY_GAME.set(
    game as Game,
    (lobby as unknown) as Lobby<GameIds, unknown>
  );
  sendMetas(game);
  sendKeyframes(game);
  game.run().then((results) => {
    sendClosure(game, results);
    LOBBY_BY_GAME.delete(game as Game);
    lobby.game = undefined;
    stats.gameResolved(gameBox);
  }, console.error);
  stats.gameCreated(gameBox);
}
