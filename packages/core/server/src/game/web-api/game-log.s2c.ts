import { S2CMessageDTOGameLog, ServerCommand } from "@raining.cards/common";

export interface S2CMessageServerGameLog {
  cmd: ServerCommand.GAME_LOG;
  time: Date;
  message: string;
}

export function s2cGameLogToDTO({
  time,
  message,
}: S2CMessageServerGameLog): S2CMessageDTOGameLog {
  return [time.getTime(), message];
}
