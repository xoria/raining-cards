import { ServerResponse } from "http";
import { HTTP_HEADER_JSON_CONTENT_TYPE } from "@/api/http/common";
import { getInfo } from "@/meta/info";
import { ServerOptions } from "@/server/server";

export function infoHandler(res: ServerResponse, options: ServerOptions) {
  res.writeHead(200, HTTP_HEADER_JSON_CONTENT_TYPE);
  res.write(JSON.stringify(getInfo(options)));
  res.end();
}
