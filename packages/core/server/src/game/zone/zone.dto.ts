import { Zone, ZoneType } from "@/game/zone/zone.types";
import { AnyGameIds, ZoneDTO, ZoneTypeDTO } from "@raining.cards/common";
import { pluckId } from "@raining.cards/util";
import { ZoneWithMaterialChunks } from "@/game/game.dto";

export function zoneTypeToDTO<GI extends AnyGameIds>(
  meta: ZoneType<GI>
): ZoneTypeDTO<GI> {
  return { id: meta.id, ...partialZoneTypeToDTO(meta) };
}

function partialZoneTypeToDTO<GI extends AnyGameIds>(
  zoneType: Zone<GI>["customMeta"]
): typeof zoneType extends undefined ? undefined : ZoneDTO<GI>["customMeta"] {
  if (zoneType === undefined) {
    return undefined;
  }
  return {
    label: zoneType.label,
    description: zoneType.description,
    uiMeta: zoneType.uiMeta,
    relatedTo: zoneType.relatedTo?.map(pluckId),
  };
}

export function zoneWithMaterialChunksToDTO<GI extends AnyGameIds>({
  zone,
  children,
  materialChunks,
}: ZoneWithMaterialChunks<GI>): ZoneDTO<GI> {
  return {
    id: zone.id,
    typeId: zone.typeId,
    materialChunks,
    children: children?.map(zoneWithMaterialChunksToDTO),
    customMeta: partialZoneTypeToDTO(zone.customMeta),
  };
}
