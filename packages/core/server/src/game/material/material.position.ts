import { Zone } from "@/game/zone/zone.types";
import {
  isMaterialWithZone,
  MaterialLocator,
  MaterialPosition,
  MaterialPositionDescriptor,
} from "@/game/material/material.types";
import { AnyGameIds } from "@raining.cards/common";
import { assert } from "ts-essentials";

export function positionFromLocator<GI extends AnyGameIds>(
  locator: MaterialLocator<GI>
): MaterialPosition<GI> {
  const zone = locator.zone;
  if (isMaterialWithZone(locator)) {
    const material = locator.material;
    return {
      zone,
      index: zone.materials.indexOf(material),
    } as MaterialPosition<GI>;
  }
  let idx: number;
  const _len = zone.materials.length;
  if (locator.index >= 0) {
    idx = locator.index;
    assert(idx <= _len - 1, "Locator evaluates to index >= length.");
  } else {
    idx = _len + locator.index;
    assert(idx >= 0, "Locator evaluates to index < 0.");
  }
  return { zone, index: idx } as MaterialPosition<GI>;
}

export function positionFromInsertDescriptor<GI extends AnyGameIds>({
  zone,
  index,
}: MaterialPositionDescriptor<GI>): MaterialPosition<GI> {
  const _len = zone.materials.length;
  let idx: number;
  if (index >= 0) {
    idx = index;
    assert(idx <= _len, "Insert locator evaluates to index > length.");
  } else {
    idx = _len + index + 1;
    assert(idx >= 0, "Insert locator evaluates to index < 0.");
  }
  return { zone, index: idx } as MaterialPosition<GI>;
}

export function last<GI extends AnyGameIds>(
  zone: Zone<GI>
): MaterialPositionDescriptor<GI> {
  return { zone, index: -1 };
}

export function first<GI extends AnyGameIds>(
  zone: Zone<GI>
): MaterialPositionDescriptor<GI> {
  return { zone, index: 0 };
}
