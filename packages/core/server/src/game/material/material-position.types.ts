import { assert } from "ts-essentials";
import { GameIds, MaterialPositionDescriptorDTO } from "@raining.cards/common";

export function assertMaterialPositionDescriptor(
  obj: any
): asserts obj is MaterialPositionDescriptorDTO<GameIds> {
  const { index, zone } = obj;
  assert(typeof zone === "string");
  assert(typeof index === "number");
}
