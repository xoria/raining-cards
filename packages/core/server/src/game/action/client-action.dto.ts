import { assertMaterialPositionDescriptor } from "@/game/material/material-position.types";
import { assert } from "ts-essentials";
import {
  ClientActionDTO,
  ClientActorDTO,
  ElementType,
  GameIds,
} from "@raining.cards/common";
import { assertArray } from "@raining.cards/util";

export function assertClientAction(
  action: unknown
): asserts action is ClientActionDTO<GameIds> {
  assert(typeof action === "object" && action != null);
  const { schema, origin, target } = action as any;
  assert(typeof schema === "string");
  assertClientActor(origin);
  assertClientActor(target);
  assert(target.type !== ElementType.MATERIALS);
}

function assertClientActor(
  actor: unknown
): asserts actor is ClientActorDTO<GameIds> {
  assert(typeof actor === "object" && actor != null);
  const { type } = actor as any;
  switch (type as ElementType | unknown) {
    case ElementType.GLOBAL:
      return;
    case ElementType.ZONE:
      assert(typeof (actor as any).zone === "string");
      return;
    case ElementType.MATERIAL:
      assertMaterialPositionDescriptor(actor);
      return;
    case ElementType.MATERIALS:
      const materials = assertArray((actor as any).materials);
      for (const material of materials) {
        assertMaterialPositionDescriptor(material);
      }
      return;
  }
  throw new Error("Invalid actor.");
}
