import {
  ActionSchemaDTO,
  ActionSchemaGroupDTO,
  ActorSchemaDTO,
  ActorSchemaDTOByType,
  ActorSchemaGlobalDTO,
  ActorSchemaMaterialDTO,
  ActorSchemaMaterialsDTO,
  ActorSchemaSingleDTO,
  ActorSchemaZoneDTO,
  AnyGameIds,
  MaterialViewRestrictionDTO,
  PositionRestrictionDTO,
} from "@raining.cards/common";

export type ActionSchemaGroup<GI extends AnyGameIds> = ActionSchemaGroupDTO<GI>;

export type ActorSchema<GI extends AnyGameIds> = ActorSchemaDTO<GI>;
export type ActorSchemaSingle<GI extends AnyGameIds> = ActorSchemaSingleDTO<GI>;
export type MaterialViewRestriction<
  GI extends AnyGameIds
> = MaterialViewRestrictionDTO<GI>;
export type PositionRestriction<
  GI extends AnyGameIds
> = PositionRestrictionDTO<GI>;
export type ActorSchemaGlobal = ActorSchemaGlobalDTO;
export type ActorSchemaZone<GI extends AnyGameIds> = ActorSchemaZoneDTO<GI>;
export type ActorSchemaMaterial<
  GI extends AnyGameIds
> = ActorSchemaMaterialDTO<GI>;
export type ActorSchemaMaterials<
  GI extends AnyGameIds
> = ActorSchemaMaterialsDTO<GI>;
export type ActorSchemaByType<GI extends AnyGameIds> = ActorSchemaDTOByType<GI>;

export type ActionSchema<
  GI extends AnyGameIds,
  Origin extends ActorSchema<GI> = ActorSchema<GI>,
  Target extends ActorSchemaSingle<GI> = ActorSchemaSingle<GI>
> = ActionSchemaDTO<GI, Origin, Target>;
