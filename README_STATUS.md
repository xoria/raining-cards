# Status Quo

**Work in progress!** This project is in early stage. Major improvements and
additions are planned that may change the game authoring API. The most invasive
ones planned being:

- Game scaffold
- Zone neighborship relations
- Runtime zone (and other game meta) updates
- Lifecycle hooks for game implementations (most importantly client join and
  leave)

In addition, some features are not implemented within the engine (or generic UI)
yet, despite their types already being exported to game definitions:

- Actions {global, material, zone} to {material, zone}
- Actions materials to {global, material, zone}

## Limitations

All the limitations below are due to early development. There are no
philosophical or known technical reasons that block fixing them, just the
available resources. Feel free to contribute! If you want to contribute,
consider creating an issue first in order to discuss a possible solution.

- Missing i18n support
- Lack of custom game GUI
- Lack of support/comfort for concurrent-turn games
