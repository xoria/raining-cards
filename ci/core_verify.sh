#!/usr/bin/env bash

set -e

CI_DIR="$(cd "$(dirname "$0")" && pwd)"

packages="$("$CI_DIR/core_packages.sh")"

for package in ${packages}; do
  cd "$package"
  npm run verify:ci
done
